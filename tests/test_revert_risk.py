import json
from collections.abc import Generator
from datetime import datetime
from pathlib import Path
from typing import Any, cast

import pandas as pd
import pytest
from knowledge_integrity.schema import Revision
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T
from research_datasets.revert_risk.base_features import enrich_with_base_features
from research_datasets.revert_risk.features import (
    Features,
    Featurizer,
    LanguageAgnosticFeaturizer,
    MultilingualFeaturizer,
    enrich_with_features,
    extract_features,
    with_revision_features,
)
from research_datasets.revert_risk.predict import with_predictions
from research_datasets.revert_risk.train import train_cross_validated_model, train_model
from research_datasets.time import Period
from syrupy.assertion import SnapshotAssertion
from xgboost import DMatrix, XGBClassifier
from xgboost.spark import SparkXGBClassifierModel


@pytest.fixture
def example_revision_data() -> dict[str, Any]:
    return {
        "id": 2002,
        "lang": "en",
        "text": """This is a lead.
== Section I ==
Section I body. {{and a|template}}

[[Category:bar]]
[[Category:baz]]
""",
        "comment": "c1",
        "timestamp": "2024-05-06 04:11:49.0",
        "bytes": 2700,
        "parent": {
            "id": 1001,
            "lang": "en",
            "text": """This is a lead.
== Section I ==
Section I body. {{and a|template}}

[[Category:bar]]
""",
            "comment": "c2",
            "timestamp": "2024-05-06 03:11:49.0",
            "bytes": 2800,
        },
        "page": {
            "id": 123,
            "title": "This is a title",
            "first_edit_timestamp": "2020-05-23 20:40:09.0",
        },
        "user": {
            "id": 4567,
            "name": "Athena",
            "groups": ["bot"],
            "registration_timestamp": "2016-07-11 08:10:02.0",
        },
    }


@pytest.fixture
def updated_revision_data(
    example_revision_data: dict[str, Any],
    request: pytest.FixtureRequest,
) -> dict[str, Any]:
    return example_revision_data | cast(dict[str, Any], request.param)


@pytest.fixture
def example_revisions_df(spark: SparkSession) -> DataFrame:
    revision_data = (
        {
            "wiki_db": "enwiki",
            "event_entity": "revision",
            "revision_id": 1001,
            "revision_parent_id": 0,
            "revision_is_identity_reverted": False,
            "revision_seconds_to_identity_revert": None,
            "revision_tags": ["test-tag"],
            "page_id": 123,
            "page_title": "123",
            "page_first_edit_timestamp": "2018-01-01T01:01:01Z",
            "event_timestamp": "2018-01-01T01:01:01Z",
            "event_comment": "test revision",
            "event_user_id": 456,
            "event_user_groups": ["test-group"],
            "event_user_is_permanent": False,
            "event_user_registration_timestamp": "2017-01-01T01:01:01Z",
            "event_user_revision_count": 5,
            "event_user_text": "test_user",
            "revision_text_bytes": 1,
            "revision_first_identity_reverting_revision_id": None,
            "page_namespace_is_content": True,
            "event_user_is_bot_by": ["name", "group"],
            "event_user_is_bot_by_historical": ["name", "group"],
        },
        {
            "wiki_db": "enwiki",
            "event_entity": "revision",
            "revision_id": 1002,
            "revision_parent_id": 1001,
            "revision_is_identity_reverted": True,
            "revision_seconds_to_identity_revert": 60,
            "revision_tags": ["test-tag"],
            "page_id": 123,
            "page_title": "123",
            "page_first_edit_timestamp": "2018-01-01T01:01:01Z",
            "event_timestamp": "2018-02-02T02:02:02Z",
            "event_comment": "test revision",
            "event_user_id": 456,
            "event_user_groups": ["test-group"],
            "event_user_is_permanent": False,
            "event_user_registration_timestamp": "2017-01-01T01:01:01Z",
            "event_user_revision_count": 5,
            "event_user_text": "test_user",
            "revision_text_bytes": 2,
            "revision_first_identity_reverting_revision_id": 1003,
            "page_namespace_is_content": True,
            "event_user_is_bot_by": [],
            "event_user_is_bot_by_historical": [],
        },
        {
            "wiki_db": "enwiki",
            "event_entity": "revision",
            "revision_id": 1003,
            "revision_parent_id": 1002,
            "revision_is_identity_reverted": True,
            "revision_seconds_to_identity_revert": 60,
            "revision_tags": ["test-tag"],
            "page_id": 123,
            "page_title": "123",
            "page_first_edit_timestamp": "2018-01-01T01:01:01Z",
            "event_timestamp": "2018-03-03T03:03:03Z",
            "event_comment": "test revision",
            "event_user_id": None,
            "event_user_groups": None,
            "event_user_is_permanent": True,
            "event_user_registration_timestamp": None,
            "event_user_revision_count": None,
            "event_user_text": "127.0.0.1",
            "revision_text_bytes": 3,
            "revision_first_identity_reverting_revision_id": 1004,
            "page_namespace_is_content": True,
            "event_user_is_bot_by": [],
            "event_user_is_bot_by_historical": [],
        },
    )

    df = spark.createDataFrame(data=(T.Row(**item) for item in revision_data))
    return df


@pytest.fixture
def example_wikidiff_df(spark: SparkSession) -> DataFrame:
    wikidiff_cols = (
        "revision_id",
        "wiki_db",
        "revision_timestamp",
        "revision_text",
        "parent_revision_diff",
    )
    wikidiff_data = (
        (
            1001,
            "enwiki",
            "2018-01-01T00:01:01Z",
            "Original text",
            None,
        ),
        (
            1002,
            "enwiki",
            "2018-02-02T02:02:02Z",
            "Changed text",
            "@@ -1,11 +1,12 @@\n-Changed\n+Original\n  tex\n",
        ),
        (
            1003,
            "enwiki",
            "2018-03-03T03:03:03Z",
            "Changed text",
            "",
        ),
    )
    return spark.createDataFrame(
        data=wikidiff_data,
        schema=wikidiff_cols,
    )


@pytest.mark.parametrize(
    ("period",),
    (
        (Period(start=datetime(2018, 1, 1), end=datetime(2018, 4, 4)),),
        (Period(start=datetime(2018, 1, 1), end=datetime(2018, 3, 2)),),
        (Period(start=datetime(2018, 1, 1), end=datetime(2018, 1, 1)),),
    ),
    ids=(
        "period includes all data",
        "period includes subset of data",
        "period includes no data",
    ),
)
def test_base_features(
    example_revisions_df: DataFrame,
    example_wikidiff_df: DataFrame,
    dataframe_snapshot: SnapshotAssertion,
    period: Period,
) -> None:
    actual = example_revisions_df.transform(
        enrich_with_base_features(
            wikidiff_df=example_wikidiff_df,
            wiki_dbs=("enwiki",),
            period=period,
        )
    )
    assert actual == dataframe_snapshot


@pytest.mark.parametrize(
    (
        "drop_disputed_reverts",
        "drop_non_permanent_user_edits",
        "drop_bot_edits",
        "max_rows_per_wiki",
    ),
    (
        (
            False,
            False,
            False,
            None,
        ),
        (
            True,
            False,
            False,
            None,
        ),
        (
            False,
            True,
            False,
            None,
        ),
        (
            False,
            False,
            True,
            None,
        ),
    ),
    ids=(
        "don't drop disputed or anonymous edits",
        "drop only disputed edits",
        "drop only anonymous edits",
        "drop only bot edits",
    ),
)
def test_generate_features_set(
    example_revisions_df: DataFrame,
    example_wikidiff_df: DataFrame,
    dataframe_snapshot: SnapshotAssertion,
    drop_disputed_reverts: bool,
    drop_non_permanent_user_edits: bool,
    drop_bot_edits: bool,
    max_rows_per_wiki: int | None,
) -> None:
    base_features_df = example_revisions_df.transform(
        enrich_with_base_features(
            wikidiff_df=example_wikidiff_df,
            wiki_dbs=("enwiki",),
            period=Period(start=datetime(2000, 1, 1), end=datetime(2100, 1, 1)),
        )
    )
    feature_set_transform = enrich_with_features(
        LanguageAgnosticFeaturizer,
        max_rows_per_wiki=max_rows_per_wiki,
        drop_disputed_reverts=drop_disputed_reverts,
        drop_non_permanent_user_edits=drop_non_permanent_user_edits,
        drop_bot_edits=drop_bot_edits,
    )
    actual = base_features_df.transform(feature_set_transform).select("features")

    assert actual == dataframe_snapshot


@pytest.mark.parametrize(
    "featurizer", (LanguageAgnosticFeaturizer, MultilingualFeaturizer)
)
@pytest.mark.parametrize(
    "updated_revision_data",
    ({}, {"page": {"id": None}}),
    ids=("valid data", "invalid data"),
    indirect=True,
)
def test_extract_features(
    featurizer: Featurizer,
    updated_revision_data: dict[str, Any],
    snapshot: SnapshotAssertion,
) -> None:
    revision_json = json.dumps(updated_revision_data)
    actual = extract_features(featurizer=featurizer, revision_json=revision_json)
    assert actual == snapshot


class CustomFeaturizer:
    @classmethod
    def extract(cls, revision: Revision) -> Features:
        return {"rev_text_len": len(revision.text)}


@pytest.mark.parametrize(
    "featurizer",
    (LanguageAgnosticFeaturizer, MultilingualFeaturizer, CustomFeaturizer),
)
def test_with_revision_features(
    example_revisions_df: DataFrame,
    example_wikidiff_df: DataFrame,
    featurizer: Featurizer,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    base_features_df = example_revisions_df.transform(
        enrich_with_base_features(
            wikidiff_df=example_wikidiff_df,
            wiki_dbs=("enwiki",),
            period=Period(start=datetime(2000, 1, 1), end=datetime(2100, 1, 1)),
        )
    )

    actual = base_features_df.transform(with_revision_features(featurizer))

    # Drop the base columns so that the snapshot is more readable but check
    # that they exist first so we know the function isn't accidentally dropping them.
    base_cols = ("revision", "parent")
    _ = actual.select(*base_cols)
    actual = actual.drop(*base_cols)

    assert actual == dataframe_snapshot


@pytest.fixture
def example_features_df(spark: SparkSession) -> DataFrame:
    feature_names = ["feature1", "feature2"]
    train_cols = ["label", *feature_names]
    train_data = (
        (True, 10, 20),
        (False, 10, -10),
        (True, 10, 21),
        (False, 10, -9),
    )
    df = spark.createDataFrame(
        data=train_data,
        schema=train_cols,
    )
    return df.withColumn("features", F.to_json(F.struct(*feature_names)))


def is_probability(prob: float) -> bool:
    zero = 0
    one = 1
    return zero <= prob <= one


def test_train_model(example_features_df: DataFrame) -> None:
    feature_names = ["feature1", "feature2"]

    trainer = train_model("label", 1, feature_names)
    model = trainer(example_features_df)

    assert model.get_booster().feature_names == feature_names

    features_json = {"feature1": 11, "feature2": 19}
    X = DMatrix(pd.DataFrame([features_json]))
    [prob_yes] = model.get_booster().predict(X, validate_features=True)
    assert is_probability(prob_yes)


def test_train_cross_validated_model(example_features_df: DataFrame) -> None:
    feature_names = ["feature1", "feature2"]
    grid_params = [
        ("max_depth", [2, 5]),
    ]
    trainer = train_cross_validated_model("label", 1, grid_params, feature_names)

    model = trainer(example_features_df)

    assert model.get_booster().feature_names == feature_names

    features_json = {"feature1": 11, "feature2": 19}
    X = DMatrix(pd.DataFrame([features_json]))
    [prob_yes] = model.get_booster().predict(X, validate_features=True)
    assert is_probability(prob_yes)


@pytest.fixture()
def example_model() -> Generator[SparkXGBClassifierModel, None, None]:
    test_model_file = Path(__file__).parent / "data" / "revert_risk" / "test_model.json"
    xgb_model = XGBClassifier()
    xgb_model.load_model(test_model_file)
    model = SparkXGBClassifierModel(xgb_model)
    model.set_device("cpu")
    model.set(model.use_gpu, False)
    model.set(model.getParam("tree_method"), "exact")
    yield model


def test_batch_predict(
    example_model: SparkXGBClassifierModel,
    example_features_df: DataFrame,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    feature_names = ["feature1", "feature2"]
    predictor = with_predictions(example_model, feature_names)

    df = example_features_df.transform(predictor)
    assert df == dataframe_snapshot
