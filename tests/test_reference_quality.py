import pytest
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from research_datasets.reference_quality.domain import (
    extract_cited_domains,
    parse_url_domain,
)
from research_datasets.reference_quality.pipeline import (
    enrich_with_domain_statistics,
)
from syrupy.assertion import SnapshotAssertion


@pytest.fixture
def example_wikitext_history(spark: SparkSession) -> DataFrame:
    wikitext = "Example wikitext."
    wikitext_with_ref = (
        wikitext
        + '<ref name="example">{{Cite web |url="https://www.example.com}}</ref>'
    )
    wikitext_history_cols = (
        "wiki_db",
        "page_id",
        "page_namespace",
        "page_redirect_title",
        "revision_id",
        "revision_parent_id",
        "revision_text",
        "revision_timestamp",
        "user_text",
    )
    wikitext_history = (
        (
            "enwiki",
            123,
            0,
            "",
            101,
            0,
            wikitext,
            "2018-01-01T01:01:01Z",
            "TestUser",
        ),
        (
            "enwiki",
            123,
            0,
            "",
            201,
            101,
            wikitext_with_ref,
            "2018-01-02T01:01:01Z",
            "1.0.0.1",
        ),
        (
            "enwiki",
            123,
            0,
            "",
            301,
            201,
            wikitext_with_ref,
            "2018-01-03T01:01:01Z",
            "NewUser",
        ),
        (
            "enwiki",
            123,
            0,
            "",
            401,
            301,
            wikitext,
            "2018-01-04T01:01:01Z",
            "TestUser",
        ),
    )
    df = spark.createDataFrame(
        wikitext_history, schema=wikitext_history_cols
    ).withColumn(
        colName="revision_timestamp",
        col=F.col("revision_timestamp").cast("timestamp"),
    )
    return df


@pytest.mark.parametrize(
    ("url", "expected"),
    (
        (
            "https://www.example.com",
            "www.example.com",
        ),
        (
            "http://www.example.com",
            "www.example.com",
        ),
        (
            "ftp://www.example.com",
            "www.example.com",
        ),
        (
            "https://www.example.com?param=value",
            "www.example.com",
        ),
        (
            "https://www.example.com#fragment",
            "www.example.com",
        ),
        (
            "www.example.com",
            None,
        ),
        (
            "https://web.archive.org/web/20200101010101/http://example.com",
            "example.com",
        ),
        (
            "https://web.archive.org/web/20200101010101/example.com",
            "web.archive.org",
        ),
        (
            "https://web.archive.org/details",
            "web.archive.org",
        ),
        (
            "http://web.archive.org/web/20200101010101/http://archive.org/web/20200101010101/http://example.com",
            "example.com",
        ),
        (
            "https://web.archive.org/web/20200101010101/https:///",
            "web.archive.org",
        ),
        (
            "https://www.example.com/web/http://archive.org",
            "www.example.com",
        ),
        (
            "Not-a-url",
            None,
        ),
        (
            "",
            None,
        ),
    ),
)
def test_parse_domain(url: str, expected: str) -> None:
    assert parse_url_domain(url) == expected


@pytest.mark.parametrize(
    ("wikitext", "expected"),
    (
        (
            '<ref name="example">{{Cite web |url="https://www.example.com#fragment" |archive-url=https://web.archive.org/web/20200101010101/http://example.com#fragment}}</ref>',
            ["example.com"],
        ),
        (
            '<ref name="example">{{Cite web |url="https://archive.today/404" |archive-url=https://web.archive.org/web/20200101010101/http://example.com#fragment}}</ref>',
            ["archive.today"],
        ),
        (
            '<dummy name="example">{{Cite web |url="https://archive.today/404" |archive-url=https://web.archive.org/web/20200101010101/http://example.com#fragment}}</dummy>',
            [],
        ),
    ),
)
def test_extract_cited_domains(wikitext: str, expected: list[str]) -> None:
    assert extract_cited_domains(wikitext) == expected


def test_enrich_with_domain_statistics(
    example_wikitext_history: DataFrame,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    actual = example_wikitext_history.transform(
        enrich_with_domain_statistics(
            wiki_dbs=("enwiki",),
            min_revisions_since=2,
        )
    )
    assert actual == dataframe_snapshot
