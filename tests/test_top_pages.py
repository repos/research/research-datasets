from datetime import datetime

import pytest
from pyspark import Row
from pyspark.sql import DataFrame, SparkSession
from research_datasets.top_pages.pipeline import enrich_with_top_pages
from syrupy.assertion import SnapshotAssertion


@pytest.fixture
def pageview_actor_df(spark: SparkSession) -> DataFrame:
    example_pageview = {
        "page_id": 123,
        "agent_type": "user",
        "normalized_host": {
            "project_class": "wikipedia",
            "project": "test",
            "tld": "org",
            "project_family": "wikipedia",
        },
        "year": 2024,
        "month": 4,
        "day": 2,
        "hour": 1,
        "pageview_info": {
            "language_variant": "default",
            "project": "test.wikipedia",
            "page_title": "Test_page",
        },
        "geocoded_data": {
            "continent": "Europe",
            "country_code": "AT",
            "country": "Austria",
            "subdivision": "Vienna",
            "city": "Vienna",
            "timezone": "Europe/Vienna",
        },
        "ip": "127.0.0.1",
        "user_agent": (
            "Mozilla/5.0 (Linux; Android 10; K) AppleWebKit/537.36 "
            "(KHTML, like Gecko) Chrome/95.0.0.0 Mobile Safari/537.36"
        ),
        "accept_language": "de-de,de;q=0.9",
    }

    return spark.createDataFrame(
        data=(Row(**example_pageview), Row(**example_pageview))
    )


@pytest.mark.parametrize(
    "hour_to_process",
    (datetime(2024, 4, 2, 0), datetime(2024, 4, 2, 1), datetime(2024, 4, 2, 2)),
    ids=(
        "hour-before",
        "hour-of",
        "hour-after",
    ),
)
@pytest.mark.parametrize(
    "pageviews_threshold",
    (0, 2),
    ids=(
        "pageviews-threshold-lt-cardinality",
        "pageviews-threshold-eq-cardinality",
    ),
)
@pytest.mark.parametrize(
    "fingerprint_threshold",
    (0, 1),
    ids=(
        "fingerprint-threshold-lt-cardinality",
        "fingerprint-threshold-eq-cardinality",
    ),
)
def test_enrich_with_top_pages(
    pageview_actor_df: DataFrame,
    hour_to_process: datetime,
    pageviews_threshold: int,
    fingerprint_threshold: int,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    actual_df = pageview_actor_df.transform(
        enrich_with_top_pages(
            hour_to_process=hour_to_process,
            pageviews_threshold=pageviews_threshold,
            fingerprint_threshold=fingerprint_threshold,
            top_n=5,
        )
    )
    assert actual_df == dataframe_snapshot
