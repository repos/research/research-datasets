from pyspark.sql import SparkSession
from research_transformations.transformation import (
    filter_disputed_reverts,
    stratified_sample,
    with_parent_revision_history,
    with_wikitext,
)
from syrupy.assertion import SnapshotAssertion


def test_statified_sample(
    spark: SparkSession, dataframe_snapshot: SnapshotAssertion
) -> None:
    cols = ("wiki_db", "rev_id")
    data = (
        ("abwiki", 1234),
        ("cdwiki", 4567),
        ("dewiki", 7891),
        ("abwiki", 1234),
    )
    df = spark.createDataFrame(data, cols)

    actual_df = df.transform(
        stratified_sample(cols=["wiki_db"], strata_size=1, seed=42)
    )

    assert actual_df == dataframe_snapshot


def test_statified_sample_repartition(
    spark: SparkSession, dataframe_snapshot: SnapshotAssertion
) -> None:
    wikis = ["abwiki", "cdwiki", "dewiki"]
    strata_size = 10

    cols = ("wiki_db", "rev_id")
    data = []
    for w in wikis:
        data.extend([(w, i) for i in range(100)])
    df = spark.createDataFrame(data, cols)

    actual_df = (
        df.transform(
            stratified_sample(
                cols=["wiki_db"], strata_size=strata_size, seed=42, repartition=True
            )
        )
        .groupBy("wiki_db")
        .count()
    )
    assert actual_df == dataframe_snapshot


def test_filter_disputed_reverts(
    spark: SparkSession, dataframe_snapshot: SnapshotAssertion
) -> None:
    cols = (
        "wiki_db",
        "revision_id",
        "page_id",
        "revision_is_identity_reverted",
        "revision_first_identity_reverting_revision_id",
    )
    data = (
        ("abwiki", 1234, 1, True, 1011),
        ("dewiki", 7891, 2, True, 1011),
        ("abwiki", 1011, 1, True, 1112),
        ("cdwiki", 4567, 1, False, None),
    )
    df = spark.createDataFrame(data, cols)

    actual_df = df.transform(filter_disputed_reverts(reverting_rev_df=df))

    assert actual_df == dataframe_snapshot


def test_with_parent_revision_history(
    spark: SparkSession, dataframe_snapshot: SnapshotAssertion
) -> None:
    cols = (
        "wiki_db",
        "revision_id",
        "revision_parent_id",
        "revision_is_identity_reverted",
    )
    data = (
        ("abwiki", 1234, 1011, True),
        ("dewiki", 7891, 1011, True),
        ("abwiki", 1011, 1112, True),
        ("cdwiki", 4567, None, False),
    )
    df = spark.createDataFrame(data, cols)

    actual_df = df.transform(with_parent_revision_history(parent_df=df))

    assert actual_df == dataframe_snapshot


def test_with_wikitext(
    spark: SparkSession, dataframe_snapshot: SnapshotAssertion
) -> None:
    cols = ("wiki_db", "revision_id")
    data = (
        ("abwiki", 1234),
        ("dewiki", 7891),
        ("cdwiki", 4567),
    )
    df = spark.createDataFrame(data, cols)
    wikitext_data = (
        ("abwiki", 1234, "ab wikitext"),
        ("dewiki", 1112, "de wikitext"),
        ("ghwiki", 4567, "gh wikitext"),
    )
    wikitext_df = spark.createDataFrame(
        wikitext_data, ("wiki_db", "revision_id", "revision_text")
    )

    actual_df = df.transform(with_wikitext(wikitext_df=wikitext_df))
    assert actual_df == dataframe_snapshot
