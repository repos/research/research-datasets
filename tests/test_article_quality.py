import pytest
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T
from research_datasets.article_quality.features import with_article_features
from research_datasets.article_quality.scores import with_quality_scores
from syrupy.assertion import SnapshotAssertion


@pytest.fixture
def example_wikitext_history(spark: SparkSession) -> DataFrame:
    wikitext = "Example wikitext."
    ref = '<ref name="example">{{Cite web |url="https://www.example.com}}</ref>'
    link = "[[ Page|anchor]]"
    category = "[[Category:Example cat]]"
    media = "[animage.jpg]"
    media1 = "[animage1.jpg]"
    heading = "==Heading=="

    wikitext_history_cols = (
        "wiki_id",
        "page_id",
        "page_namespace_id",
        "revision_id",
        "revision_text",
        "revision_dt",
    )

    wikitext_history = (
        (
            "enwiki",
            123,
            0,
            101,
            wikitext
            + " ".join(
                7 * [heading] + 2 * [category] + 3 * [link] + 4 * [ref] + [media]
            ),
            "2018-01-01T01:01:01Z",
        ),
        (
            "enwiki",
            123,
            0,
            201,
            wikitext + " ".join(5 * [ref] + [media, media1]),
            "2018-01-02T01:01:01Z",
        ),
        (
            "enwiki",
            124,
            0,
            301,
            wikitext + " ".join(5 * [link]),
            "2018-01-03T01:01:01Z",
        ),
        (
            "enwiki",
            124,
            0,
            401,
            wikitext + " ".join(5 * [category]),
            "2018-01-04T01:01:01Z",
        ),
    )
    df = (
        spark.createDataFrame(wikitext_history, schema=wikitext_history_cols)
        .withColumn(
            colName="revision_dt",
            col=F.col("revision_dt").cast("timestamp"),
        )
        .withColumn("page_redirect_target", F.lit(None).cast(T.StringType()))
    )
    return df


def test_enrich_with_features(
    example_wikitext_history: DataFrame,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    actual = example_wikitext_history.transform(with_article_features())

    assert actual == dataframe_snapshot


def test_with_quality_scores(
    spark: SparkSession,
    example_wikitext_history: DataFrame,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    wikidata_item_page_link_df = spark.createDataFrame(
        schema=("wiki_db", "item_id", "page_id"),
        data=(("enwiki", "Q1", 123), ("enwiki", "Q2", 124)),
    )
    actual = example_wikitext_history.transform(with_article_features()).transform(
        with_quality_scores(wikidata_item_page_link_df)
    )

    assert actual == dataframe_snapshot
