import chispa  # type: ignore[import-untyped]
import pytest
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T
from research_datasets.wikidiff.diff import apply_diff, unified_diff
from research_datasets.wikidiff.pipeline import enrich_with_wikitext_diff
from syrupy.assertion import SnapshotAssertion


@pytest.fixture
def original_wikitext() -> str:
    return """This is a lead.

== Section I ==
Section I body. {{and a|template}}
=== Section I.A ===
Section I.A [[body]].

== Section II ==
Section II body.
"""


@pytest.fixture
def revised_wikitext() -> str:
    return """This is not not a lead.

== Section I ==
Section I body. {{and a|template}}
=== Section I.A ===
Section I.A 'changed' [[body]].

== Section II ==
Section II body.
"""


@pytest.fixture(scope="function")
def example_wikitext_df(
    spark: SparkSession, original_wikitext: str, revised_wikitext: str
) -> DataFrame:
    cols = ("revision_id", "wiki_db", "page_id", "revision_text", "revision_parent_id")
    data = (
        (101, "testwiki", 123, original_wikitext, None),
        (201, "testwiki", 123, revised_wikitext, 101),
        (102, "testwiki", 456, original_wikitext, None),
        (202, "testwiki", 456, "", 102),
        (103, "testwiki", 789, "", None),
        (203, "testwiki", 789, revised_wikitext, 103),
        (104, "testwiki", 111, None, None),
        (204, "testwiki", 111, revised_wikitext, 104),
        (105, "testwiki", 131, original_wikitext, None),
        (205, "testwiki", 131, original_wikitext, 105),
    )
    return spark.createDataFrame(data=data, schema=cols)


@pytest.mark.parametrize(
    ("original", "revised"),
    (
        ("The past.", "The present."),
        ("The past.", ""),
        ("", "The present."),
        ("The past.", None),
        # (None, "The present"),
        ("The past.", "The past."),
    ),
    ids=(
        "original-to-revised",
        "revised-empty",
        "original-empty",
        "revised-null",
        # "original-null",
        "original-unchanged",
    ),
)
def test_diff_roundtrip(spark: SparkSession, original: str, revised: str) -> None:
    df = spark.createDataFrame(
        data=((original, revised),),
        schema=T.StructType(
            [
                T.StructField("original", T.StringType(), True),
                T.StructField("revised", T.StringType(), True),
            ]
        ),
    )
    result_df = df.withColumn(
        colName="roundtrip",
        col=apply_diff(
            original=F.col("original"),
            diff=unified_diff(F.col("original"), F.col("revised")),
        ),
    )

    chispa.assert_column_equality(result_df, "roundtrip", "revised")


def compare_original_and_reconstructed_text(wikidiff_df: DataFrame) -> None:
    compare_df = wikidiff_df.withColumn(
        colName="parent_reconstructed_text",
        col=apply_diff(
            F.col("revision_text"),
            F.col("parent_revision_diff"),
        ),
    )
    chispa.assert_column_equality(
        compare_df, "parent_reconstructed_text", "parent_original_text"
    )


def test_diff_wikitext_single_batch(
    example_wikitext_df: DataFrame,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    result_df = example_wikitext_df.transform(enrich_with_wikitext_diff())

    # Drop text column so that the snapshot is more readable but check
    # that it exists first so we know the function isn't accidentally dropping.
    text_col = "revision_text"
    assert text_col in result_df.columns
    assert result_df.drop(text_col).sort("revision_id") == dataframe_snapshot

    # rejoin original parent revision text for roundtrip
    rev_id_matches = F.col("result.revision_parent_id") == F.col("parent.revision_id")
    wiki_db_matches = F.col("result.wiki_db") == F.col("parent.wiki_db")

    result_df = result_df.alias("result").join(
        example_wikitext_df.alias("parent").select(
            "wiki_db",
            "revision_id",
            F.col("revision_text").alias("parent_original_text"),
        ),
        on=((rev_id_matches) & (wiki_db_matches)),
    )

    compare_original_and_reconstructed_text(result_df)


@pytest.mark.parametrize(
    "current_batch", (0, 1, 2, 3, 4, 5), ids=lambda n: f"current-batch-{n}-of-5"
)
def test_diff_wikitext_multiple_batches(
    example_wikitext_df: DataFrame,
    dataframe_snapshot: SnapshotAssertion,
    current_batch: int,
) -> None:
    result_df = example_wikitext_df.transform(
        enrich_with_wikitext_diff(
            current_batch=current_batch,
            num_batches=5,
        )
    )

    # Drop the revision text column so that the snapshot is more readable but
    # check that it exists first so we know the function isn't accidentally dropping it.
    assert "revision_text" in result_df.columns
    assert result_df.drop("revision_text").sort("revision_id") == dataframe_snapshot
