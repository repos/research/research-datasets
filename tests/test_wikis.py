import chispa  # type: ignore
from pyspark.sql import SparkSession
from research_datasets.wikis import get_wikis


def test_canonical_wikis(spark: SparkSession) -> None:
    cols = ["database_code", "database_group"]

    data = [
        ("enwiki", "wikipedia"),
        ("somewiki", "media"),
        ("elwiki", "wikipedia"),
        ("mywiki", "document"),
        ("lowwiki", "media"),
    ]

    df = spark.createDataFrame(data=data, schema=cols)

    df.createOrReplaceTempView("canonical_wikis")

    expected_df = spark.createDataFrame(
        data=[("enwiki",), ("elwiki",)], schema=["wiki_db"]
    )

    actual_df = get_wikis(spark=spark, is_canonical=True, table_name="canonical_wikis")

    chispa.assert_df_equality(expected_df, actual_df)


def test_all_wikis(spark: SparkSession) -> None:
    cols = ["database_code", "database_group"]

    data = [
        ("enwiki", "wikipedia"),
        ("somewiki", "media"),
        ("elwiki", "wikipedia"),
        ("mywiki", "document"),
        ("lowwiki", "media"),
    ]

    df = spark.createDataFrame(data=data, schema=cols)

    df.createOrReplaceTempView("canonical_wikis")

    expected_df = spark.createDataFrame(
        data=[("enwiki",), ("somewiki",), ("elwiki",), ("mywiki",), ("lowwiki",)],
        schema=["wiki_db"],
    )

    actual_df = get_wikis(spark=spark, table_name="canonical_wikis")

    chispa.assert_df_equality(expected_df, actual_df)
