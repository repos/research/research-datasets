from pyspark.sql import SparkSession
from research_datasets.mwaddlink.filter_dict_anchor import (
    filter_links_associated_with_unwanted_qids,
)
from research_datasets.mwaddlink.generate_anchor_dictionary import enrich_with_anchors
from research_datasets.mwaddlink.generate_backtesting_data import (
    enrich_with_link_sentences,
)
from research_datasets.mwaddlink.generate_training_data import (
    _enrich_with_page_title,
    enrich_with_ml_features,
)
from research_datasets.mwaddlink.generate_wdproperties import (
    enrich_with_wikidata_statements,
)
from syrupy.assertion import SnapshotAssertion


def test_enrich_with_anchors(
    spark: SparkSession,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    cols = (
        "page_id",
        "page_title",
        "page_redirect_title",
        "wiki_db",
        "revision_text",
        "page_namespace",
    )
    data = (
        (
            101,
            "Test page",
            "",
            "testwiki",
            "This tests [[ This | this (that) ]], [[ This | this (that) ]] and [[ Also this | this (that)]]",  # noqa: E501
            0,
        ),
        (201, "Also this", "This", "testwiki", "REDIRECT", 0),
        (301, "This", "", "testwiki", "This is a test", 0),
    )
    df = spark.createDataFrame(data=data, schema=cols)
    anchors_df = df.transform(enrich_with_anchors(spark=spark, wiki_dbs=("testwiki",)))
    assert anchors_df == dataframe_snapshot


def test_generate_wdproperties(
    spark: SparkSession,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    wikidata_item_page_link_df = spark.createDataFrame(
        data=(("Q1", "testwiki", "1"), ("Q2", "testwiki", "2")),
        schema=("item_id", "wiki_db", "page_id"),
    )

    data = (
        (
            "Q1",
            [
                {
                    "mainSnak": {
                        "property": "P111",
                        "dataType": "wikibase-item",
                        "dataValue": {"value": """{"id":"XX"}"""},
                    }
                },
                {
                    "mainSnak": {
                        "property": "P111",
                        "dataType": "wikibase-item",
                        "dataValue": {"value": """{"id":"YY"}"""},
                    }
                },
                {
                    "mainSnak": {
                        "property": "P222",
                        "dataType": "wikibase-item",
                        "dataValue": {"value": """{"id":"YY"}"""},
                    }
                },
            ],
        ),
    )

    wikidata_entity_df = spark.createDataFrame(
        data=data,
        schema="struct<id:string,claims:array<struct<mainSnak:struct<property:string,dataType:string,dataValue:struct<value:string>,hash:string>>>>",
    )

    wikidata_properties = ["P111"]
    out_df = wikidata_entity_df.transform(
        enrich_with_wikidata_statements(wikidata_item_page_link_df, wikidata_properties)
    )

    assert out_df == dataframe_snapshot


def test_filter_dict_anchor(
    spark: SparkSession,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    anchors_df = spark.createDataFrame(
        data=(
            ("testwiki", "a1", {"l1": 1, "l2": 2}),
            ("testwiki", "a2", {"l1": 3}),
            ("testwiki", "a3", {"l2": 4}),
        ),
        schema="struct<wiki_db:string,anchor:string,link_occurrence:map<string,bigint>>",
    )

    pageid_df = spark.createDataFrame(
        data=((1, "testwiki", "l1"), (2, "testwiki", "l2")),
        schema=("page_id", "wiki_db", "title"),
    )

    wdproperties_df = spark.createDataFrame(
        data=((1, "testwiki", ["Q1", "Q2"]), (2, "testwiki", ["Q1"])),
        schema=("page_id", "wiki_db", "statement_value_qid"),
    )

    unwanted_qids = {"Q2"}
    out_df = anchors_df.transform(
        filter_links_associated_with_unwanted_qids(
            pageid_df, wdproperties_df, unwanted_qids
        )
    )

    assert out_df == dataframe_snapshot


def test_generate_backtesting_data(
    spark: SparkSession,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    mediawiki_wikitext_current_df = spark.createDataFrame(
        data=(
            (
                "enwiki",
                "Not this sentence. Yes this one with a [[ link | to (that) ]].",
            ),
            ("dewiki", "Ein [[ link | hier ]] also ja. Aber nicht dieser."),
        ),
        schema=("wiki_db", "revision_text"),
    )

    canonical_wikis_df = spark.createDataFrame(
        data=(("enwiki", "en"), ("dewiki", "de")),
        schema=("database_code", "language_code"),
    )

    max_sentences_per_wiki = 2
    out_df = mediawiki_wikitext_current_df.transform(
        enrich_with_link_sentences(canonical_wikis_df, max_sentences_per_wiki)
    )

    assert out_df == dataframe_snapshot


def test_generate_training_data(
    spark: SparkSession,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    anchors_df = spark.createDataFrame(
        data=(
            ("testwiki", "a1", {"L1": 1, "L2": 2}),
            ("testwiki", "a2", {"L1": 3}),
            ("testwiki", "a3", {"L2": 4}),
        ),
        schema="struct<wiki_db:string,anchor:string,link_occurrence:map<string,bigint>>",
    )

    article_topics_df = spark.createDataFrame(
        data=(("testwiki", 1, [0.0, 1.0, 2.0]), ("testwiki", 2, [10.0, 20.0, 0.0])),
        schema=(
            "wiki_db",
            "pid_from",
            "embedding",
        ),
    )

    data = (
        (
            "testwiki",
            "L1",
            [{"anchor": "a1", "link": "L2"}],
            "s1",
        ),
        (
            "testwiki",
            "L2",
            [{"anchor": "a2", "link": "L1"}],
            "s2",
        ),
    )
    anchor_links_df = spark.createDataFrame(
        data=data, schema=("wiki_db", "page_title", "anchor_links", "sentence")
    )

    pageids_df = spark.createDataFrame(
        data=((1, "testwiki", "L1"), (2, "testwiki", "L2")),
        schema=("page_id", "wiki_db", "title"),
    )

    anchor_candidates_df = spark.createDataFrame(
        data=(("testwiki", "L1", "a1"), ("testwiki", "L1", "a2")),
        schema=("wiki_db", "page_title", "anchor"),
    )

    canonical_wikis_df = spark.createDataFrame(
        data=(("testwiki", "en"),),
        schema=("database_code", "language_code"),
    )

    embeddings_df = article_topics_df.transform(
        _enrich_with_page_title(pageids_df)
    ).alias("embeddings")

    out_df = anchor_candidates_df.transform(
        enrich_with_ml_features(
            anchors_df,
            embeddings_df,
            pageids_df,
            anchor_links_df,
            canonical_wikis_df,
        )
    )

    assert out_df == dataframe_snapshot
