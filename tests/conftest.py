from collections.abc import Generator

import pytest
from pyspark.sql import DataFrame, SparkSession
from syrupy.assertion import SnapshotAssertion
from syrupy.matchers import path_type


@pytest.fixture(scope="session")
def spark() -> Generator[SparkSession, None, None]:
    """Spark session with eager evaluation enabled."""
    spark = (
        SparkSession.builder.master("local")
        .appName("research-datasets")
        .config("spark.sql.repl.eagerEval.enabled", True)
        .config("spark.sql.repl.eagerEval.maxNumRows", 2**16)
        .config("spark.sql.shuffle.partitions", 4)
        .config(
            # Increase max number of characters for each cell
            "spark.sql.repl.eagerEval.truncate",
            2**16,
        )
        .getOrCreate()
    )
    yield spark
    spark.stop()


@pytest.fixture
def dataframe_snapshot(snapshot: SnapshotAssertion) -> SnapshotAssertion:
    return snapshot(
        matcher=path_type(
            types=(DataFrame,),
            # Replace dataframes with their schema and string
            # representation separated by a newline in the snapshot.
            replacer=lambda df, _: "\n".join(
                (df._jdf.schema().treeString(), repr(df)),
            ),
        )
    )
