from collections.abc import Generator

import pydantic.v1 as pydv1
import pytest
from pyspark.sql import SparkSession
from research_datasets.cli.core import Command


def test_command() -> None:
    def func(foo: int) -> int:
        return foo + 1

    command = Command(func)

    assert command.name == "testcli.testcommand.locals.func"
    assert issubclass(command.args, pydv1.BaseModel)
    assert command.args.__fields__.keys() == {
        "foo",
        "args",
        "kwargs",
        "v__duplicate_kwargs",
    }
    assert command.args.__fields__["foo"].type_ == int
    assert command.args.__fields__["foo"].required
    assert command('{"foo": 1}') == 2  # noqa: PLR2004 (magic value)


def test_command_positional_arg() -> None:
    def func(foo: int, bar: int) -> int:
        return foo + bar

    command = Command(func, lambda: 1)

    assert command.name == "testcli.testcommandpositionalarg.locals.func"
    assert issubclass(command.args, pydv1.BaseModel)
    assert command.args.__fields__.keys() == {
        "bar",
        "args",
        "kwargs",
        "v__duplicate_kwargs",
    }
    assert command.args.__fields__["bar"].type_ == int
    assert command.args.__fields__["bar"].required
    assert command('{"bar": 1}') == 2  # noqa: PLR2004 (magic value)


def test_command_generator_positional_arg(spark: SparkSession) -> None:
    def get_spark() -> Generator[SparkSession, None, None]:
        yield spark

    def func(spark: SparkSession, foo: int) -> int:
        assert spark.getActiveSession()
        return foo + 1

    command = Command(func, get_spark)

    assert command.name == "testcli.testcommandgeneratorpositionalarg.locals.func"
    assert issubclass(command.args, pydv1.BaseModel)
    assert command.args.__fields__.keys() == {
        "foo",
        "args",
        "kwargs",
        "v__duplicate_kwargs",
    }
    assert command.args.__fields__["foo"].type_ == int
    assert command.args.__fields__["foo"].required
    assert command('{"foo": 1}') == 2  # noqa: PLR2004 (magic value)


def test_command_default_arg() -> None:
    def func(foo: int = 1) -> int:
        return foo + 1

    command = Command(func)
    assert command("{}") == 2  # noqa: PLR2004 (magic value)


def test_command_incorrect_arg_type() -> None:
    def func(foo: int) -> int:
        return foo + 1

    command = Command(func)
    with pytest.raises(pydv1.ValidationError) as excinfo:
        command('{"foo": "bar"}')

    errors = excinfo.value.errors()
    assert errors[0]["type"] == "type_error.integer"


def test_command_missing_arg() -> None:
    def func(foo: int, bar: int) -> int:
        return foo + bar

    command = Command(func)
    with pytest.raises(pydv1.ValidationError) as excinfo:
        command('{"foo": 1}')

    errors = excinfo.value.errors()
    assert errors[0]["type"] == "value_error.missing"


def test_command_extra_arg() -> None:
    def func(foo: int) -> int:
        return foo + 1

    command = Command(func)
    with pytest.raises(pydv1.ValidationError) as excinfo:
        command('{"foo": 1, "bar": 2}')

    errors = excinfo.value.errors()
    assert errors[0]["type"] == "value_error.extra"
