import pytest
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from research_ml.pyspark_ml import assemble_features_vector
from syrupy.assertion import SnapshotAssertion


@pytest.fixture
def example_features_df(spark: SparkSession) -> DataFrame:
    feature_names = ["feature1", "feature2"]
    train_cols = ["label", *feature_names]
    train_data = (
        (True, 10, 20),
        (False, 10, -10),
        (True, 10, 21),
        (False, 10, -9),
    )
    df = spark.createDataFrame(
        data=train_data,
        schema=train_cols,
    )
    return df.withColumn("features", F.to_json(F.struct(*feature_names))).drop(
        *feature_names
    )


def test_assemble_features_vector(
    example_features_df: DataFrame,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    feature_names = ["feature1", "feature2"]
    df = example_features_df.transform(assemble_features_vector(feature_names))
    assert df == dataframe_snapshot
