import pytest
from pyspark.sql import DataFrame, SparkSession
from research_datasets.risk_observatory.pipeline import (
    _diff_columns,
    enrich_with_high_risk_editors_statistics,
    enrich_with_high_risk_pages_statistics,
    enrich_with_high_risk_revisions_statistics,
    enrich_with_reverts_statistics,
)
from syrupy.assertion import SnapshotAssertion


def test_change_columns(
    spark: SparkSession, dataframe_snapshot: SnapshotAssertion
) -> None:
    cols = ("wiki_db", "month", "col1", "col2")
    data = (
        ("wiki1", "2023-10", 4.0, 5),
        ("wiki1", "2023-11", 5.0, 6),
        ("wiki1", "2023-12", 7.0, 4),
        ("wiki2", "2023-10", 3.0, 6),
        ("wiki2", "2023-11", 2.0, 8),
        ("wiki2", "2023-12", 1.0, 12),
    )
    df = spark.createDataFrame(data, cols)

    actual_df = df.transform(_diff_columns(numerical_cols=["col1", "col2"]))

    assert actual_df == dataframe_snapshot


@pytest.fixture(scope="session")
def example_revert_risk_df(spark: SparkSession) -> DataFrame:
    cols = (
        "wiki_db",
        "revision_timestamp",
        "revision_id",
        "page_title",
        "user_name",
        "event_user_is_permanent",
        "user_is_bot",
        "revision_is_identity_reverted",
        "revision_revert_risk",
    )
    data = (
        ("wiki1", "2024-05-06 03:11:49.0", 1, "t1", "u1", True, False, False, 0.6),
        ("wiki1", "2024-05-06 03:11:49.0", 2, "t2", "u2", True, False, True, 0.99),
        ("wiki1", "2024-05-06 03:11:49.0", 3, "t1", "u2", False, False, False, 0.1),
        ("wiki1", "2024-06-06 03:11:49.0", 4, "t1", "u2", True, True, False, 0.3),
        ("wiki1", "2024-06-06 03:11:49.0", 5, "t2", "u1", None, True, True, 0.8),
        ("wiki1", "2024-06-06 03:11:49.0", 6, "t2", "u1", None, False, False, 0.2),
        ("wiki1", "2024-06-06 03:11:49.0", 7, "t1", "u1", False, False, True, 0.9),
    )
    return spark.createDataFrame(data, cols)


@pytest.fixture(scope="session")
def example_highrisk_thresholds_df(spark: SparkSession) -> DataFrame:
    cols = (
        "wiki_db",
        "event_user_is_permanent",
        "high_risk_threshold",
    )
    data = (
        ("wiki1", False, 0.8),
        ("wiki1", True, 0.7),
    )
    return spark.createDataFrame(data, cols)


def test_reverts_statistics(
    example_revert_risk_df: DataFrame,
    example_highrisk_thresholds_df: DataFrame,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    actual_df = example_revert_risk_df.transform(
        enrich_with_reverts_statistics(example_highrisk_thresholds_df)
    )

    assert actual_df == dataframe_snapshot


def test_high_risk_revisions_statistics(
    example_revert_risk_df: DataFrame,
    example_highrisk_thresholds_df: DataFrame,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    actual_df = example_revert_risk_df.transform(
        enrich_with_high_risk_revisions_statistics(example_highrisk_thresholds_df)
    )

    assert actual_df == dataframe_snapshot


def test_high_risk_editors_statistics(
    spark: SparkSession,
    example_revert_risk_df: DataFrame,
    example_highrisk_thresholds_df: DataFrame,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    cols = (
        "wiki_db",
        "ipb_address",
        "ipb_expiry",
    )
    data = (
        ("wiki1", "u2", "infinity"),
        ("wiki1", "u1", "infinity"),
    )
    ipblocks_df = spark.createDataFrame(data, cols)

    actual_df = example_revert_risk_df.transform(
        enrich_with_high_risk_editors_statistics(
            example_highrisk_thresholds_df, ipblocks_df
        )
    )

    assert actual_df == dataframe_snapshot


def test_high_risk_pages_statistics(
    example_revert_risk_df: DataFrame,
    example_highrisk_thresholds_df: DataFrame,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    actual_df = example_revert_risk_df.transform(
        enrich_with_high_risk_pages_statistics(example_highrisk_thresholds_df)
    )

    assert actual_df == dataframe_snapshot
