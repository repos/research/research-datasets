from collections.abc import Generator
from pathlib import Path

import numpy as np
import pytest
import sentence_transformers as st
from pyspark import Broadcast
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from research_datasets.article_embeddings.pipeline import (
    Granularity,
    embedding,
    get_lead_section,
    get_sections,
)


@pytest.fixture
def example_wikitext() -> str:
    return """This is a lead.

== Section I ==
Section I body. {{and a|template}}
=== Section I.A ===
Section I.A [[body]].

== Section II ==
Section II body.
"""


@pytest.fixture(scope="session")
def example_model(
    spark: SparkSession,
) -> Generator["Broadcast[st.SentenceTransformer]", None, None]:
    # This loads the model from files cached in the data folder.
    # To replace these files with the latest version from HF hub,
    # delete them from data and re-run these tests.
    model = st.SentenceTransformer(
        "sentence-transformers/paraphrase-albert-small-v2",
        cache_folder=str(Path(__file__).parent / "data"),
        device="cpu",
    )
    broadcasted_model = spark.sparkContext.broadcast(model)
    yield broadcasted_model
    broadcasted_model.destroy()


def test_get_lead_section(example_wikitext: str) -> None:
    expected = "This is a lead."
    assert get_lead_section(example_wikitext) == expected


def test_get_sections(example_wikitext: str) -> None:
    expected = [
        """== Section I ==
Section I body. {{and a|template}}
=== Section I.A ===
Section I.A [[body]].""",
        """== Section II ==
Section II body.""",
    ]
    assert get_sections(example_wikitext) == expected


def test_embeddings_lead(
    spark: SparkSession,
    example_model: "Broadcast[st.SentenceTransformer]",
    example_wikitext: str,
) -> None:
    df = spark.createDataFrame(data=[(example_wikitext,)], schema=["wikitext"])

    actual_df = df.select(
        embedding(
            F.col("wikitext"),
            model=example_model,
            granularity=Granularity.LEAD,
        ).alias("embedding")
    )

    actual_rows = actual_df.collect()
    assert len(actual_rows) == 1

    lead_embedding = actual_rows[0].embedding
    assert np.shape(lead_embedding) == (768,)
    assert np.sum(lead_embedding) != 0


def test_embeddings_sections(
    spark: SparkSession,
    example_model: "Broadcast[st.SentenceTransformer]",
    example_wikitext: str,
) -> None:
    df = spark.createDataFrame(data=[(example_wikitext,)], schema=["wikitext"])

    actual_df = df.select(
        embedding(
            F.col("wikitext"),
            model=example_model,
            granularity=Granularity.SECTIONS,
        ).alias("embeddings")
    )

    actual_rows = actual_df.collect()
    assert len(actual_rows) == 1

    embeddings = actual_rows[0].embeddings
    assert np.shape(embeddings) == (2, 768)
    assert np.sum(embeddings) != 0


def test_embeddings_full_text(
    spark: SparkSession,
    example_model: "Broadcast[st.SentenceTransformer]",
    example_wikitext: str,
) -> None:
    df = spark.createDataFrame(data=[(example_wikitext,)], schema=["wikitext"])

    actual_df = df.select(
        embedding(
            F.col("wikitext"),
            model=example_model,
            granularity=Granularity.FULL_TEXT,
        ).alias("embedding")
    )

    actual_rows = actual_df.collect()
    assert len(actual_rows) == 1

    full_text_embedding = actual_rows[0].embedding
    assert np.shape(full_text_embedding) == (768,)
    assert np.sum(full_text_embedding) != 0
