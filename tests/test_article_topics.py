import pathlib
import tempfile
from collections.abc import Generator, Iterable
from typing import TypeAlias

import fasttext  # type: ignore[import-untyped]
import pytest
from pyspark.sql import SparkSession
from research_datasets.article_topics.pipeline import (
    _enrich_with_qids,
    _enrich_with_target_pids,
    _filter_redirect_sources,
    _with_resolved_redirects,
    enrich_with_topic_predictions,
)
from syrupy.assertion import SnapshotAssertion

FastTextModel: TypeAlias = fasttext.FastText._FastText


@pytest.fixture(scope="session")
def example_fasttext_model() -> FastTextModel:
    example_outlinks = (
        "__label__STEM.Physics	Q1 Q3 Q5",
        "__label__Culture.Media.Video games	Q2 Q4 Q1 Q7",
        "__label__Geography.Regions.Oceania	Q10 Q11 Q2 Q3",
    )
    with tempfile.NamedTemporaryFile(mode="wt") as training_file:
        training_file.writelines(example_outlinks)
        training_file.flush()
        model = fasttext.train_supervised(
            input=training_file.name, epoch=3, dim=5, ws=20
        )
        return model


@pytest.fixture(scope="session")
def example_fasttext_model_path(
    example_fasttext_model: FastTextModel
) -> Generator[pathlib.Path, None, None]:
    with tempfile.NamedTemporaryFile() as model_file:
        example_fasttext_model.save_model(path=model_file.name)
        yield pathlib.Path(model_file.name)


@pytest.mark.parametrize(
    "pagelinks",
    ((("testwiki", 101, 7788),), (("testwiki", 101, 8899),)),
    ids=("target-in-namespace", "target-not-in-namespace"),
)
def test_enrich_with_target_pids(
    spark: SparkSession,
    pagelinks: tuple[tuple[str, int, int]],
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    mediawiki_pagelinks_df = spark.createDataFrame(
        schema=("wiki_db", "pl_from", "pl_target_id"),
        data=pagelinks,
    )
    mediawiki_private_linktarget_df = spark.createDataFrame(
        schema=("wiki_db", "lt_id", "lt_title"),
        data=(("testwiki", 7788, "Test article"),),
    )
    mediawiki_page_df = spark.createDataFrame(
        schema=("wiki_db", "page_id", "page_title"),
        data=(("testwiki", 201, "Test article"),),
    )

    actual_df = mediawiki_pagelinks_df.transform(
        _enrich_with_target_pids(
            mediawiki_private_linktarget_df=mediawiki_private_linktarget_df,
            mediawiki_page_df=mediawiki_page_df,
        )
    )

    assert actual_df == dataframe_snapshot


@pytest.mark.parametrize(
    "pagelinks",
    ((("testwiki", 301, 201),), (("testwiki", 101, 201),)),
    ids=("source-is-redirect", "source-not-redirect"),
)
def test_filter_redirect_sources(
    spark: SparkSession,
    pagelinks: Iterable[tuple[str, int, int]],
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    mediawiki_pagelinks_df = spark.createDataFrame(
        schema=("wiki_db", "pl_from", "pl_to"),
        data=pagelinks,
    )
    mediawiki_redirect_df = spark.createDataFrame(
        schema=("wiki_db", "rd_from", "rd_title"),
        data=(("testwiki", 301, "Test article"),),
    )

    actual_df = mediawiki_pagelinks_df.transform(
        _filter_redirect_sources(mediawiki_redirect_df=mediawiki_redirect_df)
    )

    assert actual_df == dataframe_snapshot


@pytest.mark.parametrize(
    "pagelinks",
    ((("testwiki", 101, 201),), (("testwiki", 101, 401),)),
    ids=("target-is-redirect", "target-not-redirect"),
)
def test_with_resolved_redirects(
    spark: SparkSession,
    pagelinks: Iterable[tuple[str, int, int]],
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    mediawiki_pagelinks_df = spark.createDataFrame(
        schema=("wiki_db", "pl_from", "pl_to"),
        data=pagelinks,
    )
    mediawiki_page_df = spark.createDataFrame(
        schema=("wiki_db", "page_id", "page_title"),
        data=(("testwiki", 301, "Test article"),),
    )
    mediawiki_redirect_df = spark.createDataFrame(
        schema=("wiki_db", "rd_from", "rd_title"),
        data=(("testwiki", 201, "Test article"),),
    )

    actual_df = mediawiki_pagelinks_df.transform(
        _with_resolved_redirects(
            mediawiki_redirect_df=mediawiki_redirect_df,
            mediawiki_page_df=mediawiki_page_df,
        )
    )

    assert actual_df == dataframe_snapshot


@pytest.mark.parametrize(
    "pagelinks",
    ((("testwiki", 101, 201),), (("testwiki", 301, 101),), (("testwiki", 101, 301),)),
    ids=("source-and-target-with-qids", "source-without-qid", "target-without-qid"),
)
def test_enrich_with_qids(
    spark: SparkSession,
    pagelinks: tuple[tuple[str, int, int]],
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    mediawiki_pagelinks_df = spark.createDataFrame(
        schema=("wiki_db", "pl_from", "resolved_pl_to"),
        data=pagelinks,
    )
    wikidata_item_page_link_df = spark.createDataFrame(
        schema=("wiki_db", "item_id", "page_id"),
        data=(("testwiki", "Q1", 101), ("testwiki", "Q2", 201)),
    )

    actual_df = mediawiki_pagelinks_df.transform(
        _enrich_with_qids(wikidata_item_page_link_df=wikidata_item_page_link_df)
    )

    assert actual_df == dataframe_snapshot


def test_enrich_with_predictions(
    spark: SparkSession,
    example_fasttext_model_path: pathlib.Path,
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    dataset_df = spark.createDataFrame(
        schema=("wiki_db", "pid_from", "qid_from", "qid_to"),
        data=(
            ("testwiki", 101, "Q101", "Q1"),
            ("testwiki", 101, "Q101", "Q2"),
            ("testwiki", 101, "Q101", None),
        ),
    )
    spark.sparkContext.addFile(path=str(example_fasttext_model_path))

    actual_df = dataset_df.transform(
        enrich_with_topic_predictions(model_filename=example_fasttext_model_path.name)
    )

    assert actual_df == dataframe_snapshot
