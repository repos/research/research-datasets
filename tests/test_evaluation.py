import pytest
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from research_transformations.evaluation import (
    binary_classifier_auc,
    binary_classifier_metrics,
    compare_auc,
)
from syrupy.assertion import SnapshotAssertion


@pytest.fixture(scope="function")
def example_predictions_df(spark: SparkSession) -> DataFrame:
    cols = ("revision_id", "wiki_db", "pred", "label")
    data = (
        (101, "testwiki", 0.1, False),
        (102, "testwiki", 0.6, False),
        (103, "testwiki", 0.8, True),
        (104, "testwiki", 0.9, True),
        (105, "testwiki", 0.4, True),
        (101, "test1wiki", 0.1, False),
        (102, "test1wiki", 0.6, True),
        (103, "test1wiki", 0.8, True),
        (104, "test1wiki", 0.9, True),
        (105, "test1wiki", 0.4, False),
    )
    return spark.createDataFrame(data=data, schema=cols)


@pytest.mark.parametrize(
    ("grouping_cols",),
    (
        (["wiki_db"],),
        ([],),
    ),
    ids=(
        "grouping by wiki_db",
        "no grouping",
    ),
)
def test_binary_classifier_metrics(
    example_predictions_df: DataFrame,
    grouping_cols: list[str],
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    result_df = example_predictions_df.transform(
        binary_classifier_metrics(
            pred_col=F.col("pred"),
            label_col=F.col("label"),
            grouping_cols=tuple(F.col(c) for c in grouping_cols),
            thresholds=[50, 80],
        )
    )
    assert result_df == dataframe_snapshot


@pytest.mark.parametrize(
    ("grouping_cols",),
    (
        (["wiki_db"],),
        ([],),
    ),
    ids=(
        "grouping by wiki_db",
        "no grouping",
    ),
)
def test_binary_classifier_auc(
    example_predictions_df: DataFrame,
    grouping_cols: list[str],
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    metrics_df = example_predictions_df.transform(
        binary_classifier_metrics(
            pred_col=F.col("pred"),
            label_col=F.col("label"),
            grouping_cols=tuple(F.col(c) for c in grouping_cols),
            thresholds=[50, 80],
        )
    )
    result_df = metrics_df.transform(
        binary_classifier_auc(tuple(F.col(c) for c in grouping_cols))
    )
    assert result_df == dataframe_snapshot


@pytest.mark.parametrize(
    ("grouping_cols",),
    (
        (["wiki_db"],),
        ([],),
    ),
    ids=(
        "grouping by wiki_db",
        "no grouping",
    ),
)
def test_compare_auc(
    example_predictions_df: DataFrame,
    grouping_cols: list[str],
    dataframe_snapshot: SnapshotAssertion,
) -> None:
    cols = tuple(F.col(c) for c in grouping_cols)
    auc_df = example_predictions_df.transform(
        binary_classifier_metrics(
            pred_col=F.col("pred"),
            label_col=F.col("label"),
            grouping_cols=cols,
            thresholds=[50, 80],
        )
    ).transform(binary_classifier_auc(cols))

    reference_auc_df = auc_df.withColumn(
        "auc", F.udf(lambda auc: auc + 0.1, "float")("auc")
    )

    result_df = compare_auc(auc_df, reference_auc_df, cols)
    assert result_df == dataframe_snapshot
