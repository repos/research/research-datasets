# Research Datasets

Pipelines that generate datasets used by the research team.

## Pipelines

- [Revert Risk](src/research_datasets/revert_risk/)
- [Risk Observatory](src/research_datasets/risk_observatory/)
- [Wikidiff](src/research_datasets/wikidiff/)
- [Article Embeddings](src/research_datasets/article_embeddings/)
- [Top pages](src/research_datasets/top_pages/)
- [Article Topics](src/research_datasets/article_topics/)

## Command Line Interface

`research-datasets` automatically generates CLI commands for specified functions. These commands have the form `{subpackage}.{module}.{function}`. For example, the command for the function `article_topics.pipeline.run` would be `articletopics.pipeline.run`. The main use case for these commands is to function as entry points that can be invoked to run pipelines from airflow DAGs.

### Command generation

To generate a command at runtime for a function, use the `cli.core.Command` constructor. For example:

```python
Command(
    function=research_datasets.article_topics.pipeline.run,
    get_spark_session,
)
```

If your function takes arguments that you don't want to accept through the CLI, a running spark session for example, you can pass a function that constructs this argument to the `Command` constructor. In the example above, this is the `get_spark_session` function. The reason why this is a function and not a value is because for objects that are expensive to create, we want to delay their initialization until the command is actually _invoked_. Also note that these arguments are positional and should be passed in in the same order as the underlying function expects them.

To invoke this command, you'll do:

```bash
$ python3 -m research_datasets articletopics.pipeline.run <args>
```

Dynamically generating commands this way means we don't have to write a separate entry point for each function that we want to expose as a command, where we duplicate all its parameters and defaults, which results in less duplication of code and effort.

To see commands for all pipelines in `research-datasets`, check out [research_datasets.cli.commands](https://gitlab.wikimedia.org/repos/research/research-datasets/-/blob/main/src/research_datasets/cli/commands.py).

### Argument models

Dynamically generated commands expect the same arguments as the underlying function in the form of a JSON object. This json is then parsed by a pydantic model before invoking the function. This pydantic model can also be used in places like airflow dags to construct a validated object and invoke `.json()` on it to serialize it to JSON. And since the model is fully typed and plays nicely with your IDE and typechecker, this is much easier than constructing a list of strings that you then pass as options to your command.

To generate pydantic models for all commands:

```bash
$ python3 -m research_datasets args --output args.py
```

## Development

### Prerequisites

- Create and activate the conda environment:

```bash
$ conda env create -f conda-environment.yaml
$ conda activate research-datasets
```

- `uv` is used for python dependency management. This needs to be installed only once.

```bash
$ pipx ensurepath
$ pipx install uv
```

### Environment variables

The `.envrc` file defines environment variables useful for local development. The file can be used with a tool such as [direnv](https://direnv.net/), or you can export them manually `source .envrc`.

### Dependencies

- Install dependencies, including the dev dependencies:

```bash
$ CC=gcc CXX=g++ uv sync --extra dev
```

- Install with the usual pip command also works, but beware this approach installs a lot of (unused) cuda deps, so the python env is not suitable for packaging.

```bash
$ pip install -e .[dev]
```

### Testing

To run all tests:

```bash
$ uv run python3 -m pytest -vv
```

This project uses the pytest plugin [syrupy](https://github.com/tophat/syrupy) for snapshot testing. You can pass `--snapshot-update` when invoking pytest to:

- Update a snapshot when the output changes.
- Add a new snapshot when tests are added.
- Delete unused snapshots when tests are deleted or renamed.

### Typechecking

You can make sure your code typechecks using mypy:

```bash
$ uv run python3 -m mypy src tests
```

### Hooks

To set up the pre-commit hooks, run:

```bash
$ pre-commit install
```

This will lint and format the code on every commit. By default, it will only run on changed files, but you can run it on all files manually:

```bash
$ pre-commit run --all-files
```

Tests, typechecking and hooks are also run in the CI.

### Airflow DAGS

Many workloads in this repository run as airflow dags defined in [airflow-dags](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags). The instruction below expect that the [airflow-dags](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags) is cloned into the parent directory.

For local development, you can use the `airflow-dev.sh` script to start the airflow dev instance using the `analytics-privatedata` user.
```
./airflow-dev.sh
```

Airflow dags use the Command api described above to pass arguments to research-datasets entry-points. To update the argument definitions, e.g in case a function signature has changed or if a new Command was defined:
```
uv run python3 -m research_datasets args --output args.py
cp args.py ../airflow-dags/research/config/
```
Note that `args.py` is checked into the airflow-dags repo, but not into the research-datasets repo.

The config of the airflow dags specifies which conda environment to use for spark jobs using the `conda_env` field. If you are testing a dag that depends on code not in production yet, you can change the `conda_env` variable via the "Admin" -> "Variable" -> edit your dag config. Set it to the url (for gitlab CI built envs, e.g. "https://gitlab.wikimedia.org/api/v4/projects/1699/packages/generic/research-datasets/0.2.0/research-datasets-0.2.0.conda.tgz"), or file path (if you call conda pack locally) of your working branch.
