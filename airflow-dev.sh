
me=$(whoami)
airflow_dir="/tmp/airflow_research_$me"

port=${1:-"8989"}

airflow_repo="../airflow-dags"

cd $airflow_repo
echo "creating temp directory for airflow $airflow_dir"
sudo -u analytics-privatedata mkdir -p $airflow_dir
sudo -u analytics-privatedata ./run_dev_instance.sh  -m $airflow_dir -p $port research
