{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Revert Risk predictions\n",
    "This notebook shows how to use the `revert_risk` module from `research-datasets` to get revert risk model predictions for a set of revisions. Since there are multiple revert risk models, the specific variant you want to use is configurable here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prerequisites"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Install dependencies\n",
    "\n",
    "The current way to install the `research-datasets` package is from its gitlab repo, leveraging VCS support in pip."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pip install torch --index-url https://download.pytorch.org/whl/cpu\n",
    "%pip install git+https://gitlab.wikimedia.org/repos/research/research-datasets.git"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set up a Spark Session\n",
    "\n",
    "In order to access tables from the data lake, e.g. `wmf.mediawiki_history` and run distributed prediction, you'll need an active Spark session. You'll also need to ship the `research-datasets` package to your spark executors using Conda to use it inside UDFs. You can use `research_common` to do this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pip install git+https://gitlab.wikimedia.org/repos/research/research-common.git@27efb43b36d73827073a19002178b0f6c6e149b3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from research_common.spark import create_yarn_spark_session\n",
    "\n",
    "spark = create_yarn_spark_session(\n",
    "    app_id=\"revert-risk-predictions\",\n",
    "    gitlab_project=\"repos/research/research-datasets\",\n",
    "    extra_config={\n",
    "        \"spark.driver.memory\": \"4G\",\n",
    "        \"spark.executor.cores\": 4,\n",
    "        \"spark.executor.memory\": \"16G\",\n",
    "        \"spark.executor.memoryOverhead\": \"4G\",\n",
    "        \"spark.dynamicAllocation.maxExecutors\": 64,\n",
    "        \"spark.sql.shuffle.partitions\": 1024,\n",
    "        \"spark.sql.execution.arrow.pyspark.enabled\": True,\n",
    "        \"spark.sql.adaptive.enabled\": \"true\",\n",
    "        \"spark.shuffle.io.maxRetries\": 10,\n",
    "        \"spark.task.maxFailures\": 10,\n",
    "        \"spark.shuffle.io.retryWait\": \"60s\",\n",
    "        \"spark.network.timeout\": \"1200s\",\n",
    "    },\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.sql import functions as F"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load a model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "from enum import Enum\n",
    "\n",
    "from research_datasets.revert_risk.features import (\n",
    "    Featurizer,\n",
    "    LanguageAgnosticFeaturizer,\n",
    "    MultilingualFeaturizer,\n",
    ")\n",
    "\n",
    "\n",
    "class ModelVariant(str, Enum):\n",
    "    LANGUAGE_AGNOSTIC = \"language_agnostic\"\n",
    "    MULTILINGUAL = \"multilingual\"\n",
    "\n",
    "    @property\n",
    "    def url(self) -> str:\n",
    "        match self:\n",
    "            case self.LANGUAGE_AGNOSTIC:\n",
    "                return \"https://analytics.wikimedia.org/published/wmf-ml-models/revertrisk/language-agnostic/20231117132654/model.pkl\"\n",
    "            case self.MULTILINGUAL:\n",
    "                return \"https://analytics.wikimedia.org/published/wmf-ml-models/revertrisk/multilingual/20230810110019/model.pkl\"\n",
    "\n",
    "    @property\n",
    "    def featurizer(self) -> Featurizer:\n",
    "        match self:\n",
    "            case self.LANGUAGE_AGNOSTIC:\n",
    "                return LanguageAgnosticFeaturizer\n",
    "            case self.MULTILINGUAL:\n",
    "                return MultilingualFeaturizer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_variant = ModelVariant.LANGUAGE_AGNOSTIC"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from research_datasets.revert_risk.model import load_model\n",
    "\n",
    "model = load_model(file_uri=model_variant.url)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Extract features"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "from collections.abc import Iterable\n",
    "from datetime import datetime\n",
    "\n",
    "from pyspark.sql import DataFrame\n",
    "from research_datasets.revert_risk.base_features import enrich_with_base_features\n",
    "from research_datasets.revert_risk.features import enrich_with_features\n",
    "from research_datasets.time import Period\n",
    "from research_transformations.transformation import Transformation\n",
    "\n",
    "\n",
    "def extract_features(\n",
    "    wikis: Iterable[str], period: Period, wikidiff_df: DataFrame, featurizer: Featurizer\n",
    ") -> Transformation:\n",
    "    def _(mediawiki_history: DataFrame) -> DataFrame:\n",
    "        base_features_df = mediawiki_history.transform(\n",
    "            enrich_with_base_features(\n",
    "                wikidiff_df=wikidiff_df,\n",
    "                wiki_dbs=wikis,\n",
    "                period=period,\n",
    "            )\n",
    "        )\n",
    "        features_df = base_features_df.transform(\n",
    "            enrich_with_features(\n",
    "                featurizer=featurizer,\n",
    "                max_rows_per_wiki=None,\n",
    "                drop_disputed_reverts=False,\n",
    "                drop_anonymous_edits=False,\n",
    "                drop_bot_edits=False,\n",
    "            )\n",
    "        )\n",
    "        return features_df\n",
    "\n",
    "    return _"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "snapshot = \"2024-07\"\n",
    "mediawiki_history_df = spark.table(\"wmf.mediawiki_history\")\n",
    "wikidiff_df = spark.table(\"research.wikidiff\")\n",
    "\n",
    "features_df = mediawiki_history_df.where(F.col(\"snapshot\") == snapshot).transform(\n",
    "    extract_features(\n",
    "        wikis=(\"trwiki\",),\n",
    "        period=Period(datetime(2024, 7, 1), datetime(2024, 8, 1)),\n",
    "        wikidiff_df=wikidiff_df.where(F.col(\"snapshot\") == snapshot),\n",
    "        featurizer=model_variant.featurizer\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Make predictions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from research_datasets.revert_risk.predict import with_revert_risk_probability\n",
    "\n",
    "predictions_df = features_df.transform(with_revert_risk_probability(model)).select(\n",
    "    \"rev_id\",\n",
    "    \"wiki_db\",\n",
    "    \"rev_timestamp\",\n",
    "    \"rev_is_identity_reverted\",\n",
    "    \"rev_seconds_to_identity_revert\",\n",
    "    \"user_is_anonymous\",\n",
    "    \"user_is_bot\",\n",
    "    \"user_name\",\n",
    "    \"page_id\",\n",
    "    \"page_title\",\n",
    "    \"rev_revert_risk\",\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "predictions_df.repartition(4).write.parquet(\n",
    "    f\"/tmp/revert_risk_predictions/{model_variant}/{snapshot}\", mode=\"overwrite\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": ".venv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.14"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
