{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Article Embeddings"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook shows how to use the `article_embeddings` module from `research-datasets` to get vector representations of text columns in your Spark dataframes. The models used to generate these vector representations or embeddings are from the `SentenceTransformers` package."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prerequisites"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Install dependencies\n",
    "\n",
    "The current way to install the `research-datasets` package is from its gitlab repo, leveraging VCS support in pip."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pip install torch --index-url https://download.pytorch.org/whl/cpu\n",
    "%pip install 'git+https://gitlab.wikimedia.org/repos/research/research-datasets.git'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set up a Spark Session\n",
    "\n",
    "In order to access wikitext tables, i.e. `wmf.mediawiki_wikitext_current` or `wmf.mediawiki_wikitext_history`, from the data lake and run distributed embedding generation, you'll need an active Spark session. You'll also need to ship the `research-datasets` package to your spark executors using Conda to use it inside UDFs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pip install git+https://gitlab.wikimedia.org/repos/research/research-common.git@27efb43b36d73827073a19002178b0f6c6e149b3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from research_common.spark import create_yarn_spark_session\n",
    "\n",
    "spark = create_yarn_spark_session(\n",
    "    app_id=\"article-embeddings\",\n",
    "    gitlab_project=\"repos/research/research-datasets\",\n",
    "    extra_config={\n",
    "        \"spark.driver.memory\": \"8G\",\n",
    "        \"spark.executor.cores\": 4,\n",
    "        \"spark.executor.memory\": \"32G\",\n",
    "        \"spark.dynamicAllocation.maxExecutors\": 64,\n",
    "        \"spark.sql.execution.arrow.pyspark.enabled\": True,\n",
    "    },\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prepare model\n",
    "\n",
    "The `article_embeddings` module is flexible when it comes to choosing a model. Any embedding generation model that can be used with `SentenceTransformers` is allowed. This includes [pre-trained models](https://huggingface.co/sentence-transformers) from HuggingFace Hub as well fine-tuned versions of those stored locally."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sentence_transformers as st\n",
    "\n",
    "model = st.SentenceTransformer(model_name_or_path=\"LaBSE\", device=\"cpu\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Broadcast Model\n",
    "\n",
    "To make your model available on all exectutors, you'll have to \"broadcast\" it before running any tasks so that it only has to be shipped once regardless of the number of stages or retries. Broadcasting involves:\n",
    "\n",
    "1. Serializing the model on the driver\n",
    "2. Streaming to each worker node the first time it is needed by an executor on it\n",
    "3. Deserializing it inside the python worker before use\n",
    "\n",
    "This means if you have a large model, you'll need enough memory on both the driver and the executors to perform this serialization and deserialization. The spark session in this notebook is configured to work with most mid sized models from `SentenceTransformers` but you might need to tweak it for yours."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "broadcasted_model = spark.sparkContext.broadcast(value=model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generate Embeddings\n",
    "\n",
    "### Choose a Granularity\n",
    "For transformer models like BERT the runtime and the memory requirement grows quadratically with the input length. This limits transformers to inputs of certain lengths. A common value for `SentenceTransformer` models is 512 word pieces, which corresponds to about 300-400 words (for English). Longer texts than this are truncated.\n",
    "\n",
    "In practice, this means that if you are generating embeddings for wikipedia articles, your input will most likely be truncated to the lede and the first section. But what if you want to generate embeddings that take into account all sections or just the lede and nothing else? `article_embeddings` allows you to pass in `Granularity` which controls what portion(s) of an article is used to generate embeddings:\n",
    "\n",
    "- `LEAD`: The lead section\n",
    "- `SECTIONS`: Individual embedding for each section\n",
    "- `FULL_TEXT`: The default i.e. truncated to max sequence length"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Encode articles\n",
    "\n",
    "For generating embeddings from the content in article revisions, use the `with_embeddings` function which allows choosing between `wmf.mediawiki_wikitext_history` and `wmf.mediawiki_wikitext_current`. You can also use the `period` parameter to only include revisions from a certain time period."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "import datetime\n",
    "\n",
    "from pyspark.sql import functions as F\n",
    "\n",
    "from research_datasets.article_embeddings.pipeline import (\n",
    "    Granularity,\n",
    "    WikitextTable,\n",
    "    with_embedding,\n",
    ")\n",
    "from research_datasets.time import Period\n",
    "\n",
    "snapshot = \"2024-11\"\n",
    "wikitext_table = WikitextTable.CURRENT\n",
    "wikitext_df = spark.table(wikitext_table).filter(F.col(\"snapshot\") == snapshot)\n",
    "\n",
    "# The period could also be defined in more precise\n",
    "# terms i.e. hour, minute, second etc.\n",
    "start_date = datetime.datetime(year=2024, month=11, day=1)\n",
    "end_date = datetime.datetime(year=2024, month=11, day=8)\n",
    "article_embeddings_df = wikitext_df.transform(\n",
    "    with_embedding(\n",
    "        wiki_dbs=(\"plwiki\",),\n",
    "        period=Period(start=start_date, end=end_date),\n",
    "        sentence_transformer=broadcasted_model,\n",
    "        granularity=Granularity.LEAD,\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "root\n",
      " |-- page_id: long (nullable = true)\n",
      " |-- page_namespace: integer (nullable = true)\n",
      " |-- page_title: string (nullable = true)\n",
      " |-- page_redirect_title: string (nullable = true)\n",
      " |-- page_restrictions: array (nullable = true)\n",
      " |    |-- element: string (containsNull = true)\n",
      " |-- user_id: long (nullable = true)\n",
      " |-- user_text: string (nullable = true)\n",
      " |-- revision_id: long (nullable = true)\n",
      " |-- revision_parent_id: long (nullable = true)\n",
      " |-- revision_timestamp: string (nullable = true)\n",
      " |-- revision_minor_edit: boolean (nullable = true)\n",
      " |-- revision_comment: string (nullable = true)\n",
      " |-- revision_text_bytes: long (nullable = true)\n",
      " |-- revision_text_sha1: string (nullable = true)\n",
      " |-- revision_content_model: string (nullable = true)\n",
      " |-- revision_content_format: string (nullable = true)\n",
      " |-- user_is_visible: boolean (nullable = true)\n",
      " |-- comment_is_visible: boolean (nullable = true)\n",
      " |-- content_is_visible: boolean (nullable = true)\n",
      " |-- snapshot: string (nullable = true)\n",
      " |-- wiki_db: string (nullable = true)\n",
      " |-- embedding: array (nullable = true)\n",
      " |    |-- element: float (containsNull = true)\n",
      "\n"
     ]
    }
   ],
   "source": [
    "article_embeddings_df.printSchema()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "25/01/21 11:12:42 WARN SessionState: METASTORE_FILTER_HOOK will be ignored, since hive.security.authorization.manager is set to instance of HiveAuthorizerFactory.\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "25/01/21 11:12:58 WARN YarnScheduler: Initial job has not accepted any resources; check your cluster UI to ensure that workers are registered and have sufficient resources\n",
      "[Stage 0:>                                                          (0 + 1) / 1]\r"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+-------+--------------+--------------------+-------------------+-----------------+-------+---------------+-----------+------------------+--------------------+-------------------+--------------------+-------------------+--------------------+----------------------+-----------------------+---------------+------------------+------------------+--------+-------+--------------------+\n",
      "|page_id|page_namespace|          page_title|page_redirect_title|page_restrictions|user_id|      user_text|revision_id|revision_parent_id|  revision_timestamp|revision_minor_edit|    revision_comment|revision_text_bytes|  revision_text_sha1|revision_content_model|revision_content_format|user_is_visible|comment_is_visible|content_is_visible|snapshot|wiki_db|           embedding|\n",
      "+-------+--------------+--------------------+-------------------+-----------------+-------+---------------+-----------+------------------+--------------------+-------------------+--------------------+-------------------+--------------------+----------------------+-----------------------+---------------+------------------+------------------+--------+-------+--------------------+\n",
      "|1331369|             0|           Cas Haley|                   |               []| 705096|     AndrzeiBOT|   75170194|          65153699|2024-11-07T02:32:05Z|               true|Poprawa linkowań ...|               2432|axiqcbu14calblc17...|              wikitext|            text/x-wiki|           true|              true|              true| 2024-11| plwiki|[-0.062201902, -0...|\n",
      "|1326951|             0|Dni wolne od prac...|                   |               []| 491269|Sebek Adamowicz|   75120071|          73683954|2024-11-02T22:48:33Z|              false|/* Dni ustawowo w...|              22896|etubj4kughi976z1j...|              wikitext|            text/x-wiki|           true|              true|              true| 2024-11| plwiki|[-0.01142759, -0....|\n",
      "|1311824|             0|  Nowotwory tarczycy|                   |               []|1081257|     Edytor2021|   75146620|          73029478|2024-11-05T21:21:49Z|              false|Zamiana ilości na...|              11346|rn18d7earhcvrdntx...|              wikitext|            text/x-wiki|           true|              true|              true| 2024-11| plwiki|[-0.040773977, -0...|\n",
      "|1491797|             0|           Jedy-Kuju|                   |               []| 145341|    Khan Tengri|   75111594|          75111542|2024-11-01T22:40:35Z|              false|po czyszczeniu ko...|               2087|2wspdobaoy7od4f9a...|              wikitext|            text/x-wiki|           true|              true|              true| 2024-11| plwiki|[-0.01934373, -0....|\n",
      "|1227027|             0|Błyszczka jarzynówka|                   |               []|  86170|          Gower|   75123892|          72989389|2024-11-03T12:08:16Z|               true|  +dodatkowy przypis|               4068|kprtyi3t5ec4w39zt...|              wikitext|            text/x-wiki|           true|              true|              true| 2024-11| plwiki|[-0.054537915, 0....|\n",
      "|1314857|             0|     Leniuchowcowate|                   |               []| 717975|       EmptyBot|   75110509|          74459456|2024-11-01T21:28:42Z|               true|dodanie kategorii...|              46936|mqhhe5h52lm99tfck...|              wikitext|            text/x-wiki|           true|              true|              true| 2024-11| plwiki|[-0.036458883, 0....|\n",
      "|1608620|             0|   Hrabia Shrewsbury|                   |               []| 705096|     AndrzeiBOT|   75109821|          68140846|2024-11-01T20:53:50Z|               true|Poprawa linkowań ...|               3238|27ac92lfshlkb1jw8...|              wikitext|            text/x-wiki|           true|              true|              true| 2024-11| plwiki|[-0.04057819, 0.0...|\n",
      "|1587495|             0|       E=mc^2 (film)|              E=mc²|               []| 705096|     AndrzeiBOT|   75164015|          15688049|2024-11-06T15:52:11Z|               true|Poprawa linków do...|                 17|a565ebi3kfga30y03...|              wikitext|            text/x-wiki|           true|              true|              true| 2024-11| plwiki|[0.038309064, -0....|\n",
      "|1390742|             0|Misja specjalna (...|                   |               []| 281739|     Odplewiacz|   75125085|          74997046|2024-11-03T14:11:12Z|               true|                kat.|               3178|qsj7dopk4tdfcv7k3...|              wikitext|            text/x-wiki|           true|              true|              true| 2024-11| plwiki|[-0.059358075, -0...|\n",
      "|1614745|             0|Still Life (Talking)|                   |               []|1203227|     Igor123121|   75165934|          75165903|2024-11-06T18:54:52Z|              false|Anulowanie wersji...|               2425|0nwgg4ruwuzy4rkfd...|              wikitext|            text/x-wiki|           true|              true|              true| 2024-11| plwiki|[-0.060883623, 0....|\n",
      "+-------+--------------+--------------------+-------------------+-----------------+-------+---------------+-----------+------------------+--------------------+-------------------+--------------------+-------------------+--------------------+----------------------+-----------------------+---------------+------------------+------------------+--------+-------+--------------------+\n",
      "only showing top 10 rows\n",
      "\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "                                                                                \r"
     ]
    }
   ],
   "source": [
    "article_embeddings_df.show(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Encode arbitrary text\n",
    "\n",
    "Generating embeddings is not limited to tables from the data lake. You can use the `embeddings` column function to encode any text column in any dataframe and the text does not have to be wikitext. For these arbitrary text columns that don't have lead or sections, the right `Granularity` would be `FULL_TEXT` which means no pre-processing will be done on the text before passing it to the model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = (\n",
    "    (1, \"我希望我能写得像猫一样神秘\"),\n",
    "    (2, \"أتمنى أن أكتب غامضًا مثل القطة\"),\n",
    "    (3, \"Хотілося б, щоб я міг писати так загадково, як кіт\"),\n",
    "    (4, \"Chciałbym umieć pisać tajemniczo jak kot\")\n",
    ")\n",
    "text_df = spark.createDataFrame(data, (\"id\", \"sentence\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "[Stage 2:=======================================>                   (2 + 1) / 3]\r"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+---+--------------------------+--------------------+\n",
      "| id|                  sentence|           embedding|\n",
      "+---+--------------------------+--------------------+\n",
      "|  1|我希望我能写得像猫一样神秘|[-0.028550485, -0...|\n",
      "|  2|      أتمنى أن أكتب غام...|[-0.017070455, -0...|\n",
      "|  3|      Хотілося б, щоб я...|[0.017085487, -0....|\n",
      "|  4|      Chciałbym umieć p...|[-0.026555676, -0...|\n",
      "+---+--------------------------+--------------------+\n",
      "\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "                                                                                \r"
     ]
    }
   ],
   "source": [
    "from pyspark.sql import functions as F\n",
    "from research_datasets.article_embeddings.pipeline import Granularity, embedding\n",
    "\n",
    "text_embeddings_df = text_df.withColumn(\n",
    "    \"embedding\",\n",
    "    embedding(\n",
    "        F.col(\"sentence\"),\n",
    "        model=broadcasted_model,\n",
    "        granularity=Granularity.FULL_TEXT,\n",
    "    ),\n",
    ")\n",
    "text_embeddings_df.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "conda-resds",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.15"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
