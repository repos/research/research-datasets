{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contribute to the shared research code base\n",
    "\n",
    "The [research-datasets](https://gitlab.wikimedia.org/repos/research/research-datasets) contains many of the pipelines/ML workflows that the research team has developed. The python package contains a lot of code that is useful for interactive work in jupyter notebooks. \n",
    "\n",
    "If you intend to only use research-datasets as a dependency, see the [getting_started.ipynb](https://gitlab.wikimedia.org/repos/research/research-datasets/-/blob/main/notebooks/getting_started.ipynb) notebook instead. \n",
    "\n",
    "However, \n",
    "- if you want to modify existing code in research-datasets\n",
    "- if you want to contribute new functionality for re-use (either by others, or by yourself in other notebooks)\n",
    "- if you work on a project that will be used in \"production\"\n",
    "\n",
    "see the instructions below for an example development workflow. Of course there are other tools/approaches that are equally valid.\n",
    "\n",
    "## Requirements\n",
    "\n",
    "Follow the [getting_started.ipynb](https://gitlab.wikimedia.org/repos/research/research-datasets/-/blob/main/notebooks/getting_started.ipynb) notebook to get setup. You also need to be WMF gitlab member for [research-datasets](https://gitlab.wikimedia.org/repos/research/research-datasets/-/project_members). While the repo can be cloned without credentials, to push branches and to build conda environments via CI (continious integration) you will need an [access token](https://gitlab.wikimedia.org/-/user_settings/personal_access_tokens).\n",
    "\n",
    "\n",
    "## Use research-datasets for development\n",
    "\n",
    "First, in a terminal on the same stat machine used for your jupyter notebook, clone the repository (`git clone https://gitlab.wikimedia.org/repos/research/research-datasets.git`) into a directory of your choice. \n",
    "\n",
    "The goal is to be able to\n",
    "- change code in research-datasets\n",
    "- have the code changes reflected in the notebook we are working on without having restart the notebook\n",
    "- have the code changes be available on the spark workers on yarn \n",
    "\n",
    "### Development in research-datasets\n",
    "\n",
    "The commands below are for working in a notebook, which implies that a conda environment is already active. If working in the terminal, activate a conda environment of your choice using `source conda-analytics-activate xyz`.\n",
    "\n",
    "Install research-datasets as editable package to enable local development."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pushd ~/gitlab/research-datasets # replace with path to your cloned repo\n",
    "!pip install -e ."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We would like to have changes in the research-datasets package reflected in your running jupyter notebook. The cell below instructs jupyter to reload a module, thereby picking up changes without the need to restart your notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can now edit files with your preferred editor (on a remote machine), e.g. by opening files directly in jupyter hub, vim, etc.   \n",
    "\n",
    "It is important to note: while changes to the research-datasets repo will be reflected in your running notebook, the changes will not be available elsewhere (e.g. on a spark worker). See the \"Creating a conda environment\" section below.\n",
    "\n",
    "\n",
    "#### VS code \n",
    "\n",
    "Using the interactive python and juptyer notebook support in VS code is convenient as it combines in one tool 1. editing a python package and 2. using jupyter notebooks. Follow the instructions below to setup a remote development environment on a stat machine:\n",
    "- install the VS code extensions for python, juptyer, and remote SSH development\n",
    "- open a remote ssh connection with the stat client, and open the research-datasets folder\n",
    "- open this notebook\n",
    "- click on \"select kernel -> select another kernel -> python environments -> create python environment -> venv\". This will create a python environment in the `.venv` with the research-datasets package installed"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "vscode": {
     "languageId": "plaintext"
    }
   },
   "source": [
    "## Creating a conda environment\n",
    "\n",
    "When do you have to create a new packaged conda environment? If the code changes are required to run somehwere else than your working machine (i.e the stat client you use). For example if you are running spark jobs, or skein applications running on yarn. However, if you are working with a local library such as numpy/scipy, a packaged conda env is not required. \n",
    "\n",
    "This notebook focuses on spark as most common use case requiring code running a remote machine. It is often subtle to know when changes require packaging a new conda environment. Some general pointers:\n",
    "- changes that only affect the spark execution plan (e.g. changing joins, group by columns etc) don't require packaging a conda environment, as the execution plan is generated in the spark driver\n",
    "- changes to UDFs generally require packaging a conda env \n",
    "- if new dependencies are installed via pip, packaging a conda env is required\n",
    "\n",
    "If you aren't sure if new conda package is required, the suggested strategy is trivial: try running code without packaging, if that fails or the output does not reflect your changes, then package a new conda environment. \n",
    "\n",
    "### Packaging conda environments\n",
    "\n",
    "This notebook documents two approaches to create a packaged conda environment. \n",
    "- using the gitlab continious integration (CI) is less error-prone and easier, but requires pushing changes to gitlab\n",
    "- packaging locally is more flexible, but also more error prone. \n",
    "\n",
    "#### Gitlab CI\n",
    "\n",
    "1. Commit the changes and push them to origin, as if you were to open a merge request. \n",
    "2. In the gitlab UI, navigate to the [Pipelines](https://gitlab.wikimedia.org/repos/research/research-datasets/-/pipelines) section \n",
    "3. Click on the \"Run\" button of the \"publish_conda_env\" in the deploy stage.\n",
    "<img src=\"conda_ci.png\" />\n",
    "\n",
    "4. Once the pipeline has completed, the package is listed in the [Packag registry](https://gitlab.wikimedia.org/repos/research/research-datasets/-/packages)\n",
    "\n",
    "\n",
    "#### Locally\n",
    "\n",
    "To package the conda environment locally and copy the it to your hdfs home directory. \n",
    "\n",
    "```\n",
    "pip uninstall -y research-datasets && \n",
    "pip install . && \n",
    "conda pack --ignore-editable-packages --ignore-missing-files -o research-datasets.tgz -f && \n",
    "hdfs dfs -put -f research-datasets.tgz && \n",
    "hdfs dfs -chmod 755 research-datasets.tgz\n",
    "```\n",
    "\n",
    "### Running spark jobs\n",
    "\n",
    "To create spark context that uses the conda environment from your working branch.\n",
    "\n",
    "- if the conda env was built via Gitlab CI, use the `version` argument to specify the branch"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "version = \"my_branch_name\"\n",
    "\n",
    "spark = create_yarn_spark_session(\n",
    "    app_id=\"revert-risk-predictions\",\n",
    "    gitlab_project=\"repos/research/research-datasets\",\n",
    "    version=version,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- if the conda env was built locally, use the `conda_env` argument to specify the path"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "conda_env = \"hdfs:///user/fab/research-datasets.tgz\"\n",
    "\n",
    "spark = create_yarn_spark_session(\n",
    "    app_id=\"revert-risk-predictions\",\n",
    "    conda_env=conda_env,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
