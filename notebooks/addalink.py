# %%
%load_ext autoreload
%autoreload 2

"""
observations/todos:
- cleanup:
    - the names are not appropriate anymore, e.g. anchor_dictionary/filter_dict_anchor is not actually a dictionary
    - the "generate" prefix is meaningless, remove it from files/methods
    - inconsistent "output" and "directory" references, used interchangably
    - generate_backtesting_data needs a new name, it is also generating training data
- generate_anchor_dictionary and generate_wdproperties can run in parallel in the dag
- generate_backtesting_eval, the last step
    - is very janky. It eval code is not updated to use the dataframes/parquet files instead of pickled dictionaries. So has a hack, the
    eval code is recreating the dictionaries and executing the previously existing code.
"""
# %%
from research_datasets.utils import configure_fsspec
configure_fsspec()

# %%
# !pip install git+https://gitlab.wikimedia.org/repos/data-engineering/wmfdata-python.git@spark_ci
from pyspark.sql import functions as F
from pyspark.sql import types as T

import wmfdata

# %%
# spark = wmfdata.spark.create_session()
spark = wmfdata.spark.create_session(
    python_env=wmfdata.spark.GitlabPythonEnv(
        gitlab_project="repos/research/research-datasets",
        version="latest",
        # version="addalink" # if you built a conda env for that branch via gitlab CI
    ),
    extra_settings={
        "spark.driver.memory": "24G",
        "spark.driver.maxResultSize": "40G",
    },
)
# to build from local conda env
# spark = wmfdata.spark.create_session(python_env=wmfdata.spark.ship_python_env)
# spark = wmfdata.spark.create_session(
#     python_env=wmfdata.spark.ShipPythonEnv(
#         conda_pack_kwargs={"ignore_missing_files": True}
#     )
# )

# %%
from pathlib import Path

# check available snapshots:
# hdfs dfs -ls /wmf/data/wmf/mediawiki/wikitext/current/
# hdfs dfs -ls /wmf/data/wmf/wikidata/entity
snapshot = "2024-11"
wikidata_snapshot = "2025-01-06"
wiki_dbs = ["simplewiki"]
# has to be a absolute hdfs path
output = Path("/user/mgerlach/test_addalink")
model_id = "model_0"

# %%
# !hdfs dfs -du -h test_addalink/

# %%
from research_datasets.mwaddlink import generate_anchor_dictionary

generate_anchor_dictionary.run(spark, snapshot, wiki_dbs, output)

# %%

from research_datasets.mwaddlink import generate_wdproperties

wikidata_properties = ["P31"]
generate_wdproperties.run(
    spark, wikidata_snapshot, wiki_dbs, wikidata_properties, output
)

# %%

from research_datasets.mwaddlink import filter_dict_anchor

filter_dict_anchor.run(spark, output)

# %%

from research_datasets.mwaddlink import generate_backtesting_data

generate_backtesting_data.run(
    spark, snapshot, wiki_dbs, output, max_sentences_per_wiki=200000
)

# %%

from research_datasets.mwaddlink import generate_training_data

generate_training_data.run(spark, snapshot, wiki_dbs, output, files_per_wiki=5)


# %%

from research_datasets.mwaddlink import generate_addlink_model
generate_addlink_model.run(
    wiki_dbs, model_id=model_id, directory=output, grid_search=False
)
# Number of Wikis: 1
# Picked up JAVA_TOOL_OPTIONS: -Dfile.encoding=UTF-8
# simplewiki 2100736
# Total training data size: 2100736
# XGBClassifier(base_score=None, booster=None, callbacks=None,
#               colsample_bylevel=None, colsample_bynode=None,
#               colsample_bytree=None, early_stopping_rounds=None,
#               enable_categorical=False, eval_metric=None, feature_types=None,
#               gamma=None, gpu_id=None, grow_policy=None, importance_type=None,
#               interaction_constraints=None, learning_rate=0.1, max_bin=None,
#               max_cat_threshold=None, max_cat_to_onehot=None,
#               max_delta_step=None, max_depth=8, max_leaves=None,
#               min_child_weight=None, missing=nan, monotone_constraints=None,
#               n_estimators=250, n_jobs=8, num_parallel_tree=None,
#               predictor=None, random_state=None, ...)
# ROC AUC=0.989
# F1 SCORE=0.719
# Average Precision Score=0.802

# %%
# !hdfs dfs -ls test_addalink/model_0

# Picked up JAVA_TOOL_OPTIONS: -Dfile.encoding=UTF-8
# Found 2 items
# -rw-r-----   3 mgerlach mgerlach        764 2025-02-14 08:35 test_addalink/model_0/model_0.encoder.joblib
# -rw-r-----   3 mgerlach mgerlach    2051545 2025-02-14 08:35 test_addalink/model_0/model_0.linkmodel.joblib

# %%

from research_datasets.mwaddlink import generate_backtesting_eval

generate_backtesting_eval.run(
    spark, model_id=model_id, wiki_dbs=wiki_dbs, directory=output, thresholds=[0.5], n_max=1000
)

# threshold:  0.5
# finished: 1000 sentences
# micro_precision:	 0.783363802559415
# micro_recall:	 0.380550621669627
# ----------------------

# !hdfs dfs -ls test_addalink/model_0

# Picked up JAVA_TOOL_OPTIONS: -Dfile.encoding=UTF-8
# Found 3 items
# -rw-r-----   3 mgerlach mgerlach        105 2025-02-14 08:37 test_addalink/model_0/model_0.backtest.eval.csv
# -rw-r-----   3 mgerlach mgerlach        764 2025-02-14 08:35 test_addalink/model_0/model_0.encoder.joblib
# -rw-r-----   3 mgerlach mgerlach    2051545 2025-02-14 08:35 test_addalink/model_0/model_0.linkmodel.joblib
