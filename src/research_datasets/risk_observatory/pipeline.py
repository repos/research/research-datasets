from collections.abc import Iterable, Sequence
from datetime import datetime
from pathlib import Path

from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql.window import Window

from research_datasets.revert_risk.evaluate import enrich_with_revert_risk_model_metrics
from research_datasets.revert_risk.features import (
    LanguageAgnosticFeaturizer,
)
from research_datasets.revert_risk.predict import (
    generate_revert_risk_predictions,
)
from research_datasets.risk_observatory.config import (
    default_files_config as files_config,
)
from research_datasets.time import Period
from research_transformations.transformation import Transformation


def _enrich_with_best_thresholds(metrics_df: DataFrame) -> DataFrame:
    """Compute the threshold that maximizes the accuracy for each wiki
    and user type (user and ip edit)"""

    # TODO, once we use spark 3.3, remove hackery and instead use
    # .agg(F.max_by('threshold', 'accuracy').alias('standard_quality'))
    @F.udf(returnType="string")
    def max_by_encode(accuracy: float, threshold: int) -> str:
        return f"{accuracy}|{threshold}"

    @F.udf(returnType="int")
    def max_by_decode(k: str) -> int:
        return int(k.split("|")[1])

    return (
        metrics_df.withColumn("encoded", max_by_encode("accuracy", "threshold"))
        .groupBy("wiki_db", "event_user_is_permanent")
        .agg(F.max("encoded").alias("encoded"))
        .withColumn("high_risk_threshold", 0.01 * max_by_decode("encoded"))
        .drop("encoded")
    )


def _diff_columns(numerical_cols: Iterable[str]) -> Transformation:
    """Compute the change to the previous month for each numerical column"""

    def _(stats: DataFrame) -> DataFrame:
        prev_window = Window.partitionBy("wiki_db").orderBy("month")
        change_cols = [
            (F.col(col) - F.lag(col).over(prev_window)).alias(f"{col}_change")
            for col in numerical_cols
        ]
        return stats.select(*(stats.columns + change_cols))

    return _


def enrich_with_reverts_statistics(high_risk_thresholds: DataFrame) -> Transformation:
    """Aggregate statistics for reverts"""

    def _(revert_risk_df: DataFrame) -> DataFrame:
        reverts_stats = (
            revert_risk_df.withColumn(
                "month", F.date_format(F.col("revision_timestamp"), "yyyy-MM")
            )
            .join(high_risk_thresholds, on=["wiki_db", "event_user_is_permanent"])
            .groupBy("wiki_db", "month")
            .agg(
                F.count("*").alias("revision_count"),
                F.count(
                    F.when(F.col("event_user_is_permanent"), True).otherwise(False)
                ).alias("registered_revision_count"),
                F.count(F.when(F.col("revision_is_identity_reverted"), True)).alias(
                    "reverted_revision_count"
                ),
                F.count(
                    F.when(
                        F.col("event_user_is_permanent")
                        & ~F.col("user_is_bot")
                        & (
                            F.col("revision_revert_risk")
                            >= F.col("high_risk_threshold")
                        ),
                        True,
                    )
                ).alias("high_risk_revision_count"),
                F.count(
                    F.when(
                        F.col("event_user_is_permanent")
                        & ~F.col("user_is_bot")
                        & (F.col("revision_is_identity_reverted"))
                        & (
                            F.col("revision_revert_risk")
                            >= F.col("high_risk_threshold")
                        ),
                        True,
                    )
                ).alias("reverted_high_risk_revision_count"),
            )
        )

        reverts_stats = (
            reverts_stats.withColumn(
                "reverted_revision_ratio",
                F.col("reverted_revision_count") / F.col("revision_count"),
            )
            .withColumn(
                "high_risk_revision_ratio",
                F.col("high_risk_revision_count") / F.col("revision_count"),
            )
            .withColumn(
                "reverted_high_risk_revision_ratio",
                F.col("reverted_high_risk_revision_count")
                / F.col("high_risk_revision_count"),
            )
            .select(
                "month",
                "wiki_db",
                "revision_count",
                "reverted_revision_ratio",
                "high_risk_revision_ratio",
                "reverted_high_risk_revision_ratio",
            )
        )

        numerical_cols = reverts_stats.columns[2:]
        return reverts_stats.transform(_diff_columns(numerical_cols))

    return _


def enrich_with_high_risk_revisions_statistics(
    high_risk_thresholds: DataFrame,
) -> Transformation:
    """Aggregate statistics for the high risk revisions"""

    def _(revert_risk_df: DataFrame) -> DataFrame:
        return (
            revert_risk_df.filter(F.col("event_user_is_permanent"))
            .filter(~F.col("user_is_bot"))
            .withColumn("month", F.date_format(F.col("revision_timestamp"), "yyyy-MM"))
            .join(high_risk_thresholds, on=["wiki_db", "event_user_is_permanent"])
            .filter(F.col("revision_revert_risk") >= F.col("high_risk_threshold"))
            .select(
                "month",
                "wiki_db",
                "revision_id",
                "revision_timestamp",
                F.col("revision_is_identity_reverted").alias("revision_is_reverted"),
                "revision_revert_risk",
                "page_title",
                F.col("user_name").alias("user_text"),
            )
        )

    return _


def enrich_with_high_risk_editors_statistics(
    high_risk_thresholds: DataFrame, ipblocks: DataFrame
) -> Transformation:
    """Aggregate statistics for the high risk revisions"""

    def _(revert_risk_df: DataFrame) -> DataFrame:
        blocked_editors = (
            ipblocks.where(F.col("ipb_expiry") == "infinity")
            .select("wiki_db", F.col("ipb_address").alias("user_name"))
            .withColumn("is_blocked", F.lit(True))
        )
        return (
            revert_risk_df.filter(F.col("event_user_is_permanent"))
            .filter(~F.col("user_is_bot"))
            .withColumn("month", F.date_format(F.col("revision_timestamp"), "yyyy-MM"))
            .join(high_risk_thresholds, on=["wiki_db", "event_user_is_permanent"])
            .groupBy("wiki_db", "month", "user_name")
            .agg(
                F.count("*").alias("revision_count"),
                F.count(
                    F.when(
                        (F.col("revision_revert_risk") >= F.col("high_risk_threshold")),
                        True,
                    )
                ).alias("highrisk_revision_count"),
                F.mean("revision_revert_risk").alias("revision_revert_risk_avg"),
            )
            .filter(F.col("highrisk_revision_count") > 0)
            .withColumn(
                "highrisk_revision_ratio",
                F.col("highrisk_revision_count") / F.col("revision_count"),
            )
            .join(blocked_editors, how="left", on=["wiki_db", "user_name"])
            .withColumn(
                "is_blocked", F.when(F.col("is_blocked"), True).otherwise(False)
            )
            .select(
                "month",
                "wiki_db",
                F.col("user_name").alias("user_text"),
                "revision_count",
                "highrisk_revision_count",
                "highrisk_revision_ratio",
                "revision_revert_risk_avg",
                "is_blocked",
            )
        )

    return _


def enrich_with_high_risk_pages_statistics(
    high_risk_thresholds: DataFrame,
) -> Transformation:
    """Aggregate statistics for the high risk pages"""

    def _(revert_risk_df: DataFrame) -> DataFrame:
        return (
            revert_risk_df.filter(F.col("event_user_is_permanent"))
            .filter(~F.col("user_is_bot"))
            .withColumn("month", F.date_format(F.col("revision_timestamp"), "yyyy-MM"))
            .join(high_risk_thresholds, on=["wiki_db", "event_user_is_permanent"])
            .groupBy("wiki_db", "month", "page_title")
            .agg(
                F.count("*").alias("revision_count"),
                F.count(
                    F.when(
                        (F.col("revision_revert_risk") >= F.col("high_risk_threshold")),
                        True,
                    )
                ).alias("highrisk_revision_count"),
                F.mean("revision_revert_risk").alias("revision_revert_risk_avg"),
            )
            .filter(F.col("highrisk_revision_count") > 0)
            .withColumn(
                "highrisk_revision_ratio",
                F.col("highrisk_revision_count") / F.col("revision_count"),
            )
            .select(
                "month",
                "wiki_db",
                "page_title",
                "revision_count",
                "highrisk_revision_count",
                "revision_revert_risk_avg",
                "highrisk_revision_ratio",
            )
        )

    return _


# TODO, this should be in revert_risk.predict, and
# the risk observatory should use it as input
def run_revert_risk_prediction(
    spark: SparkSession,
    snapshot: str,
    wikis: Sequence[str],
    period: Period,
    model_uri: str,
    hdfs_dir: Path,
    hive_database: str | None = None,
) -> None:
    """
    Batch prediction spark job for the revert risk models
    """

    predictions_df = (
        generate_revert_risk_predictions(
            spark, snapshot, wikis, period, LanguageAgnosticFeaturizer, model_uri
        )
        .select(
            "revision.wiki_db",
            "revision.page_id",
            "revision.page_title",
            "revision.revision_id",
            "revision.revision_timestamp",
            "revision.revision_is_identity_reverted",
            "revision.revision_seconds_to_identity_revert",
            "revision.event_user_is_permanent",
            (F.size("revision.event_user_is_bot_by_historical") > 0).alias(
                "user_is_bot"
            ),
            F.col("revision.event_user_text").alias("user_name"),
            "revision_revert_risk",
        )
        .cache()
    )

    # repartition into a smaller number of files as the output data
    # is small compared to the reducer configuration required
    # for the computation
    num_files = 50
    predictions_df.repartition(num_files).write.mode("overwrite").parquet(
        str(hdfs_dir / files_config.revert_risk_predictions)
    )

    # If the hive database is specified, this is an incremental job. The
    # revert risk predictions are appended
    if hive_database:
        time_partition_format = "yyyy-MM"
        partitions_per_time_partition = 10
        (
            predictions_df.withColumn(
                "time_partition",
                F.date_format("revision_timestamp", time_partition_format),
            )
            .repartition(partitions_per_time_partition)
            .write.mode("append")
            .partitionBy("time_partition")
            .saveAsTable(f"{hive_database}.revert_risk_predictions")
        )


def run_high_risk_threshold_computation(
    spark: SparkSession,
    hdfs_dir: Path,
    hive_database: str,
    predictions_hive_database: str | None = None,
    start_time: datetime | None = None,
    end_time: datetime | None = None,
) -> None:
    """Compute the revert risk threshold for a revision to be considered high risk.
    There are separate thresholds per wiki and per user type (registered user vs ip
    edit)
    """
    # If the hive database is specified, production revert risk
    # predictions for the specified period is used, otherwise
    # the theshold are based on the revert predictions of the
    # current pipeline run
    if predictions_hive_database:
        revert_risk_df = spark.table(
            f"{predictions_hive_database}.revert_risk_predictions"
        )
        if start_time:
            revert_risk_df = revert_risk_df.where(
                F.col("revision_timestamp") >= f"{start_time}"
            )
        if end_time:
            revert_risk_df = revert_risk_df.where(
                F.col("revision_timestamp") < f"{end_time}"
            )
    else:
        revert_risk_df = spark.read.parquet(
            str(hdfs_dir / files_config.revert_risk_predictions)
        )

    grouping_cols = (
        F.col("wiki_db"),
        F.col("event_user_is_permanent"),
    )
    metrics_df = revert_risk_df.transform(
        enrich_with_revert_risk_model_metrics(grouping_cols=grouping_cols)
    ).cache()

    thresholds_df = metrics_df.transform(_enrich_with_best_thresholds).cache()

    # repartition into a smaller number of files as the output data
    # is small compared to the reducer configuration required
    # for the computation
    num_files = 1
    metrics_df.repartition(num_files).write.mode("overwrite").parquet(
        str(hdfs_dir / files_config.accuracy_statistics)
    )
    thresholds_df.repartition(num_files).write.mode("overwrite").parquet(
        str(hdfs_dir / files_config.highrisk_thresholds)
    )

    thresholds_df.repartition(num_files).write.mode("overwrite").saveAsTable(
        f"{hive_database}.high_risk_thresholds"
    )


def run_risk_statistics_computation(
    spark: SparkSession,
    snapshot: str,
    hdfs_dir: Path,
    hive_database: str,
) -> None:
    """Compute statistics and sample datasets for high revert risk"""
    # inputs
    revert_risk_df = spark.read.parquet(
        str(hdfs_dir / files_config.revert_risk_predictions)
    ).cache()
    # TODO, should this support aggregating from a the
    # hive table with start end time?
    # revert_risk_df = spark.table(
    #     f"{hive_database}.{files_config.revert_risk_predictions}"
    # ).cache()
    highrisk_thresholds_df = spark.read.parquet(
        str(hdfs_dir / files_config.highrisk_thresholds)
    )

    # function to save a dataframe in the hive table specified in the args
    # partition by the month and append to existing tables
    def save_to_hive(
        df: DataFrame,
        table_name: str,
        num_files: int | None = None,
    ) -> None:
        hive_table = f"{hive_database}.{table_name}"
        df = df.repartition(num_files) if num_files else df
        return df.write.partitionBy("month").mode("append").saveAsTable(hive_table)

    # revert stats
    reverts = revert_risk_df.transform(
        enrich_with_reverts_statistics(highrisk_thresholds_df)
    )
    save_to_hive(reverts, "reverts", 10)

    # high risk revisions
    revisions = revert_risk_df.transform(
        enrich_with_high_risk_revisions_statistics(highrisk_thresholds_df)
    )
    save_to_hive(revisions, "highrisk_revisions", 50)

    # high risk editors
    ipblocks = spark.table("wmf_raw.mediawiki_ipblocks").where(
        F.col("snapshot") == snapshot
    )
    editors = revert_risk_df.transform(
        enrich_with_high_risk_editors_statistics(
            high_risk_thresholds=highrisk_thresholds_df, ipblocks=ipblocks
        )
    )
    save_to_hive(editors, "highrisk_editors", 50)

    # high risk pages
    pages = revert_risk_df.transform(
        enrich_with_high_risk_pages_statistics(highrisk_thresholds_df)
    )
    save_to_hive(pages, "highrisk_pages", 50)
