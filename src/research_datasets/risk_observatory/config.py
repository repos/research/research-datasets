from dataclasses import dataclass


# Config for intermediate files
@dataclass(frozen=True)
class FilesConfig:
    revision_features: str
    revert_risk_predictions: str
    accuracy_statistics: str
    highrisk_thresholds: str


default_files_config = FilesConfig(
    revision_features="revision_features",
    revert_risk_predictions="revert_risk_predictions",
    accuracy_statistics="accuracy_statistics",
    highrisk_thresholds="highrisk_thresholds",
)
