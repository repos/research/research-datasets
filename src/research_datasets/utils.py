import contextlib
import os
import pathlib
import signal
import subprocess
import tempfile
from collections.abc import Generator
from types import FrameType
from typing import NoReturn

import requests


def configure_fsspec() -> None:
    os.environ["CLASSPATH"] = (
        subprocess.check_output(["hadoop", "classpath", "--glob"])
        .decode("utf-8")
        .strip()
    )
    os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-1.8.0-openjdk-amd64"
    os.environ["HADOOP_HOME"] = "/usr/lib/hadoop"


def download_file(
    url: str, destination: str | os.PathLike[str] | None = None
) -> pathlib.Path:
    if not destination:
        destination = tempfile.NamedTemporaryFile(delete=False).name

    destination = pathlib.Path(destination)
    with requests.get(url, stream=True) as resp:
        resp.raise_for_status()
        with open(destination, mode="wb") as file:
            for chunk in resp.iter_content(chunk_size=8192):
                file.write(chunk)

    return destination


@contextlib.contextmanager
def timeout(seconds: int = 10) -> Generator[None, None, None]:
    """Schedule a `TimeoutError` to be raised after `seconds`.

    Can be used as a context manager or a decorator.
    When used as a decorator, it raises any exception the passed function would
    raise and returns what the function would return, unless timeout is exceeded.

    NOTE: Exceptions raised by signal handlers are propagated to the main thread,
    so this would not work correctly in a multithreaded context. See also:
    https://docs.python.org/3/library/signal.html#execution-of-python-signal-handlers
    """

    def _handle_timeout(signum: int, frame: FrameType | None) -> NoReturn:
        raise TimeoutError

    signal.signal(signal.SIGALRM, _handle_timeout)
    signal.alarm(seconds)
    try:
        yield
    finally:
        # Cancel scheduled alarm in case
        # function execution finished within timeout.
        signal.alarm(0)
