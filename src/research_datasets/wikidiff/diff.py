from diff_match_patch import diff_match_patch  # type: ignore[import-untyped]
from pyspark.sql import Column as SparkColumn
from pyspark.sql import functions as F
from pyspark.sql import types as T


def unified_diff(old: SparkColumn, new: SparkColumn) -> SparkColumn:
    """Generate a unified diff for a pair of text columns."""
    dmp = diff_match_patch()

    def _make_text_diff(old: str | None, new: str | None) -> str | None:
        if old is None or new is None:
            return None
        patches = dmp.patch_make(old, new)
        return str(dmp.patch_toText(patches))

    unified_diff_udf = F.udf(_make_text_diff, returnType=T.StringType())
    return unified_diff_udf(old, new)


def apply_diff(original: SparkColumn, diff: SparkColumn) -> SparkColumn:
    """Apply `diff` to `original` to get changed text."""
    dmp = diff_match_patch()

    def _apply_text_diff(old: str, diff: str | None) -> str | None:
        if diff is None:
            return None
        patch = dmp.patch_fromText(diff)
        new, _ = dmp.patch_apply(patch, old)
        return str(new)

    apply_diff_udf = F.udf(_apply_text_diff, returnType=T.StringType())
    return apply_diff_udf(original, diff)
