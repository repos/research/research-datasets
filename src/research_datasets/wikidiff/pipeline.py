from collections.abc import Sequence
from pathlib import Path

from pyspark.sql import DataFrame
from pyspark.sql import functions as F
from pyspark.sql.session import SparkSession

from research_datasets.wikidiff.diff import unified_diff
from research_transformations.transformation import (
    Transformation,
    with_parent_revision_history,
)


def _filter_batch(current_batch: int, num_batches: int) -> Transformation:
    def _(df: DataFrame) -> DataFrame:
        # TODO: investigate the best hashing function.
        # (wiki_db, page_id)? page_id backfilled?
        # (wiki_db, page_title)? title is unstable?
        batch_number = F.abs(F.hash("page_id") % num_batches)
        return df.filter(batch_number == current_batch)

    return _


def _with_parent_revision_diff(revision_df: DataFrame) -> DataFrame:
    return revision_df.withColumn(
        colName="parent_revision_diff",
        col=unified_diff(
            F.col("revision.revision_text"), F.col("parent.revision_text")
        ),
    )


def enrich_with_wikitext_diff(
    current_batch: int = 0, num_batches: int = 1
) -> Transformation:
    """Enrich with revision text diff for a wikitext batch.

    The returned dataframe contains all columns in `wikitext_df` for both
    the child and parent revisions except for the parent revision_text column.
    `num_batches` is the total number of chunks to split `wikitext_df` in and
    `current_batch` is the chunk to generate the dataset for, discarding all
    the other ones.
    """

    def _(wikitext_df: DataFrame) -> DataFrame:
        wikitext_history_batch_df = wikitext_df.transform(
            _filter_batch(current_batch=current_batch, num_batches=num_batches)
        )
        # Cache on the filtered level in preparation of the self join
        wikitext_history_batch_df = wikitext_history_batch_df.cache()

        revision_diff_df = (
            wikitext_history_batch_df.transform(
                with_parent_revision_history(
                    parent_df=wikitext_history_batch_df,
                    rev_id="revision_id",
                    parent_rev_id="revision_parent_id",
                    keep_nulls=True,
                )
            )
            .transform(_with_parent_revision_diff)
            # drop the parent revision fields, flatten revision
            .select("revision.*", "parent_revision_diff")
        )

        return revision_diff_df

    return _


def run_batch(
    spark: SparkSession,
    hive_table_dir: Path,
    snapshot: str,
    wiki_db: str,
    current_batch: int,
    num_batches: int,
    num_files: int | None = None,
) -> None:
    """Run wikidiff for a single batch for computing diffs for a given wiki.
    Note that the batch configuration is computed on the airflow side, this application
    is only useful in conjunction with airflow.
    """
    # TODO: test parallel execution on airflow side, does a concurrent append to the
    # same external hive directory work?
    # TODO: in airflow, use:
    # https://airflow.apache.org/docs/apache-airflow/stable/configurations-ref.html#max-active-tasks-per-dag

    diff_df = (
        spark.table("wmf.mediawiki_wikitext_history")
        .where(
            f"""
            snapshot = '{snapshot}'
            AND wiki_db = '{wiki_db}'
            AND page_namespace = 0
            AND page_id > 0
            AND page_title !=''
            AND page_redirect_title = ''
        """
        )
        .transform(
            enrich_with_wikitext_diff(
                current_batch=current_batch, num_batches=num_batches
            )
        )
    )
    partition_cols = ("snapshot", "wiki_db")

    if num_files:
        diff_df = diff_df.repartition(num_files)

    (
        diff_df.write.partitionBy(*partition_cols)
        .mode("append")
        .format("avro")
        .save(str(hive_table_dir))
    )


def run_wikis(
    spark: SparkSession,
    hive_table_dir: Path,
    snapshot: str,
    wikis: Sequence[str],
    num_files: Sequence[int],
) -> None:
    """Generate wikidiff for a collection of small wikis using the same spark context,
    but using separate jobs. Note that this application is only useful in conjunction
    with airflow.
    """
    # TODO: test parallel execution on airflow side, does a concurrent append to the
    # same external hive directory work?
    # TODO: in airflow, use:
    # https://airflow.apache.org/docs/apache-airflow/stable/configurations-ref.html#max-active-tasks-per-dag

    assert len(wikis) == len(num_files), "wikis and num_files length have to match"

    for wiki_db, n_files in zip(wikis, num_files):
        run_batch(
            spark=spark,
            hive_table_dir=hive_table_dir,
            snapshot=snapshot,
            wiki_db=wiki_db,
            current_batch=0,
            num_batches=1,
            num_files=n_files,
        )
        # clear the cached dataframes
        spark.catalog.clearCache()
