from collections.abc import Callable, Iterable
from enum import Enum, unique
from pathlib import Path
from typing import TypeAlias, cast

import mwparserfromhell as mwp  # type: ignore
import pandas as pd
import sentence_transformers as st
from pyspark import Broadcast
from pyspark.sql import Column as SparkColumn
from pyspark.sql import DataFrame as SparkDataFrame
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T
from typing_extensions import assert_never

from research_datasets.time import Period
from research_transformations.transformation import Transformation


@unique
class Granularity(str, Enum):
    """Article unit to be used for embeddings."""

    LEAD = "lead"
    SECTIONS = "sections"
    FULL_TEXT = "full_text"


@unique
class WikitextTable(str, Enum):
    """Name of the wikitext dataset in the data lake."""

    CURRENT = "wmf.mediawiki_wikitext_current"
    HISTORY = "wmf.mediawiki_wikitext_history"


Preprocessor: TypeAlias = Callable[[str], str | list[str] | None]


def get_lead_section(wikitext: str) -> str | None:
    sections = mwp.parse(wikitext).get_sections(levels=[1], include_lead=True)
    if not sections:
        return None
    # Casting to str as the actual type is Wikicode which behaves like a str.
    lead = cast(str, sections[0].strip())
    return lead


def get_sections(wikitext: str) -> list[str]:
    sections = mwp.parse(wikitext).get_sections(levels=[2], include_headings=True)
    stripped_sections = [section.strip() for section in sections]
    return stripped_sections


def embedding(
    wikitext: SparkColumn,
    model: "Broadcast[st.SentenceTransformer]",
    granularity: Granularity,
) -> SparkColumn:
    """Generate embedding(s) for a wikitext column.

    Depending on the `granularity` supplied, the wikitext column
    will be split into one or more units, each of which will be encoded using `model`.
    The resulting column will have a single embedding or an array of embeddings
    if the article has been split into multiple units like sections
    """
    preprocessor: Preprocessor | None
    result_series_type: T.DataType

    match granularity:
        case Granularity.LEAD:
            preprocessor = get_lead_section
            result_series_type = T.ArrayType(T.FloatType())
        case Granularity.SECTIONS:
            preprocessor = get_sections
            result_series_type = T.ArrayType(T.ArrayType(T.FloatType()))
        case Granularity.FULL_TEXT:
            preprocessor = None
            result_series_type = T.ArrayType(T.FloatType())
        case _:
            assert_never(granularity)

    # Resorting to ignoring type errors for now because the current annotations
    # for pandas udf stuff in pyspark make it impossible to appease the type checker.
    @F.pandas_udf(returnType=result_series_type)  # type: ignore[call-overload,misc]
    def compute_embeddings_udf(articles: pd.Series) -> pd.Series:  # type: ignore[type-arg]
        if preprocessor:
            articles = articles.map(preprocessor)

        # If the pre-processed articles are single strings,
        # compute embeddings in batches of 32 articles. Otherwise,
        # if articles have been pre-processed into multiple strings each
        # i.e. sections etc. encode a single article at a time.
        embeddings = (
            pd.Series(list(model.value.encode(articles.tolist())))
            if len(articles) > 0 and isinstance(articles[0], str)
            else articles.map(lambda sections: list(model.value.encode(sections)))  # type: ignore[arg-type,return-value]
        )
        return embeddings

    return cast(SparkColumn, compute_embeddings_udf(wikitext))


def with_embedding(
    wiki_dbs: Iterable[str],
    period: Period,
    sentence_transformer: "Broadcast[st.SentenceTransformer]",
    granularity: Granularity,
) -> Transformation:
    """Append a contextual embedding column computed from article wikitext."""

    def _(wikitext_df: SparkDataFrame) -> SparkDataFrame:
        wikitext_df = wikitext_df.filter(
            (F.col("wiki_db").isin(*wiki_dbs))
            & (F.col("page_namespace") == 0)
            & (F.col("revision_timestamp").between(period.start, period.end))
        )
        embeddings_df = wikitext_df.withColumn(
            colName="embedding",
            col=embedding(
                F.col("revision_text"),
                model=sentence_transformer,
                granularity=granularity,
            ),
        ).drop("revision_text")

        return embeddings_df

    return _


def run(
    spark: SparkSession,
    snapshot: str,
    wikis: Iterable[str],
    output: Path,
    period: Period,
    model: str = "LaBSE",
    granularity: Granularity = Granularity.LEAD,
    wikitext_table: WikitextTable = WikitextTable.CURRENT,
    max_files_per_wiki: int = 4,
) -> None:
    """Generate a dataset of wikitext embeddings."""
    sentence_transformer = st.SentenceTransformer(model, device="cpu")
    broadcasted_model = spark.sparkContext.broadcast(sentence_transformer)
    wikitext_df = spark.table(wikitext_table).filter(F.col("snapshot") == snapshot)
    dataset_df = wikitext_df.transform(
        with_embedding(
            wiki_dbs=wikis,
            period=period,
            sentence_transformer=broadcasted_model,
            granularity=granularity,
        )
    )

    # Repartitioning instead of coalesce because a drastic coalesce
    # can result in upstream stages taking place on fewer nodes than the
    # default partitioning. See the docs for coalesce for more info.
    dataset_df = dataset_df.repartition(max_files_per_wiki)
    dataset_df.write.partitionBy("wiki_db").mode("overwrite").format("parquet").save(
        str(output)
    )
