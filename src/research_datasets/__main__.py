import pathlib
from typing import Annotated

import typer

from research_datasets.cli.commands import COMMANDS
from research_datasets.cli.utils import generate_args_definitions
from research_datasets.utils import configure_fsspec


def main() -> None:
    app = typer.Typer()

    for cmd in COMMANDS:
        app.command(name=cmd.name)(cmd.callback)

    @app.command()
    def args(
        output: Annotated[
            pathlib.Path,
            typer.Option(help="Python file to write the pydantic models to."),
        ],
    ) -> None:
        """Generate pydantic model definitions for args to all commands."""

        instructions = """\"\"\"NOTE: This file has been auto-generated using the command:
$ python3 -m research_datasets args --output args.py

Do not edit. Use the above command to regenerate.
See the research-datasets readme for more information:
https://gitlab.wikimedia.org/repos/research/research-datasets/-/blob/main/README.md
\"\"\"

"""  # noqa: E501 (Line too long)
        generate_args_definitions(
            commands=COMMANDS,
            output_path=output,
            instructions=instructions,
        )

    @app.callback()
    def entry_point() -> None:
        """Run research pipelines by passing json encoded arguments.

        These commands are mainly expected to be invoked from airflow DAGs, where
        pydantic models for args can be used to parse DAG props. Such model objects can
        then be serialized to json before being passed to the respective commands.
        """
        configure_fsspec()

    app()


if __name__ == "__main__":
    main()
