from collections.abc import Sequence
from typing import Literal

from pyspark.sql import SparkSession
from pyspark.sql import functions as F

from research_datasets.article_quality.features import with_article_features
from research_datasets.article_quality.scores import (
    LanguageAgnosticCoefficients,
    StandardQualityThresholds,
    with_quality_scores,
)
from research_datasets.time import Period


def run_article_features(
    spark: SparkSession,
    period: Period,
    output_table: str,
    write_mode: Literal["append", "overwrite"],
    wiki_ids: Sequence[str],
    coalesce_features: int = 5,
) -> None:
    wikitext_col = (
        F.col("revision_content_slots").getItem("main").getField("content_body")
    )
    wikitext_df = (
        spark.table("wmf_content.mediawiki_content_history_v1")
        .withColumn("revision_text", wikitext_col)
        .drop("revision_content_slots")
        .where(F.col("wiki_id").isin(*wiki_ids))
        .where(F.col("revision_dt") >= f"{period.start}")
        .where(F.col("revision_dt") < f"{period.end}")
    )
    heading_level = 3
    features_df = wikitext_df.transform(with_article_features(heading_level))

    spark.sql("SET hive.exec.dynamic.partition = true")
    spark.sql("SET hive.exec.dynamic.partition.mode = nonstrict")
    time_partition_format = "yyyy-MM"
    (
        features_df.withColumn(
            "time_partition", F.date_format("revision_dt", time_partition_format)
        )
        .repartition(coalesce_features, "time_partition")
        .write.partitionBy(["time_partition"])
        .mode(write_mode)
        .format("hive")
        .saveAsTable(output_table)
    )


def run_article_scores(
    spark: SparkSession,
    wikidata_snapshot: str,
    output_table: str,
    wiki_ids: Sequence[str],
    article_features_table: str = "research.article_features",
    language_agnostic_coefficients: LanguageAgnosticCoefficients = (
        LanguageAgnosticCoefficients()
    ),
    standard_quality_thresholds: StandardQualityThresholds = (
        StandardQualityThresholds()
    ),
    coalesce_scores: int = 200,
) -> None:
    article_features_df = spark.table(article_features_table).where(
        F.col("wiki_id").isin(*wiki_ids)
    )

    wikidata_item_page_link_df = (
        spark.table("wmf.wikidata_item_page_link")
        .where(F.col("snapshot") == wikidata_snapshot)
        .where(F.col("wiki_db").isin(*wiki_ids))
        .where(F.col("page_namespace") == 0)
    )

    scores_df = article_features_df.transform(
        with_quality_scores(
            wikidata_item_page_link_df,
            language_agnostic_coefficients=language_agnostic_coefficients,
            standard_quality_thresholds=standard_quality_thresholds,
        )
    )

    (
        scores_df.repartition(coalesce_scores)
        .write.mode("overwrite")
        .saveAsTable(output_table)
    )
