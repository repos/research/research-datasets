import logging
import re
from dataclasses import dataclass

from pyspark.sql import DataFrame
from pyspark.sql import functions as F
from pyspark.sql import types as T

from research_datasets.mwaddlink.wikitext_to_plaintext import (
    CAT_ALIASES,
    CAT_PREFIXES,
    MEDIA_ALIASES,
    MEDIA_PREFIXES,
)
from research_transformations.transformation import Transformation

IMAGE_EXTENSIONS = [
    ".jpg",
    ".png",
    ".svg",
    ".gif",
    ".jpeg",
    ".tif",
    ".bmp",
    ".webp",
    ".xcf",
]
VIDEO_EXTENSIONS = [".ogv", ".webm", ".mpg", ".mpeg"]
AUDIO_EXTENSIONS = [".ogg", ".mp3", ".mid", ".webm", ".flac", ".wav", ".oga"]
MEDIA_EXTENSIONS = list(set(IMAGE_EXTENSIONS + VIDEO_EXTENSIONS + AUDIO_EXTENSIONS))
MAX_MEDIA_LENGTH = 240

# build regex that checks for all media extensions
EXTEN_REGEX = ("(" + "|".join([e + r"\b" for e in MEDIA_EXTENSIONS]) + ")").replace(
    ".", r"\."
)
# join in the extension regex with one that requiries at least one
# alphanumeric and/or a few special characters before it
EXTEN_PATTERN = re.compile(rf"([\w ',().-]+){EXTEN_REGEX}", flags=re.UNICODE)
ref_singleton = re.compile(r"<ref(\s[^/>]*)?/>", re.M | re.I)
ref_tag = re.compile(r"<ref(\s[^/>]*)?>[\s\S]*?</ref>", re.M | re.I)

# The maximum page size allowed. 309043 correspond
# to the 0.999 quantile. The top 0.001 are usually vandalism or
# data errors, we mark them with num_refs = -1, in order to allow
# further investigaiton.
MAX_PAGE_LENGTH = 309043


@dataclass(frozen=True)
class Features:
    page_length: int
    num_refs: int
    num_wikilinks: int
    num_categories: int
    num_media: int
    num_headings: int


@F.udf(
    returnType=T.StructType(
        [
            T.StructField("page_length", T.IntegerType()),
            T.StructField("num_refs", T.IntegerType()),
            T.StructField("num_wikilinks", T.IntegerType()),
            T.StructField("num_categories", T.IntegerType()),
            T.StructField("num_media", T.IntegerType()),
            T.StructField("num_headings", T.IntegerType()),
        ]
    )
)
def extract_article_features(wikitext: str, wiki_id: str, level: int) -> Features:
    """Gather counts of article components directly from wikitext.

    Pros:
    * Much faster than mwparserfromhell (10x speed-up in testing) --
      e.g., 200 µs vs. 2 ms for medium-sized article
    * Can be easily extended to catch edge-cases -- e.g., images added
      via infoboxes/galleries that lack bracket syntax

    Cons/Issues:
    * Misses intra-nested links:
        * e.g. [[File:Image.jpg|Image with a [[caption]]]] only catches
          the File and not the [[caption]]
        * Could be extended by also regexing each link found, which
          should catch almost all
    * Misses references added via templates w/o ref tags -- e.g.,
      shortened-footnote templates.
    * Misses media added via templates / gallery tags that lack brackets
    """
    try:
        lang = wiki_id.replace("wiki", "")
        cat_prefixes = CAT_PREFIXES + CAT_ALIASES.get(lang, [])
        med_prefixes = MEDIA_PREFIXES + MEDIA_ALIASES.get(lang, [])
        page_length = len(wikitext)
        if page_length < MAX_PAGE_LENGTH:
            refs = len(ref_singleton.findall(wikitext)) + len(ref_tag.findall(wikitext))
            links = [
                m.split("|", maxsplit=1)[0]
                for m in re.findall(r"(?<=\[\[)(.*?)(?=]])", wikitext, flags=re.DOTALL)
            ]
            categories = len(
                [1 for link in links if link.split(":", maxsplit=1)[0] in cat_prefixes]
            )
            media_bra = [
                link.split(":", maxsplit=1)[1]
                for link in links
                if link.split(":", maxsplit=1)[0] in med_prefixes
            ]
            # The media_ext regex is very expensive. Diego assumes that
            # media_ext are unusual. So, first check if there is string
            # matching with the extensions and just in that case I run
            # the regex.
            exts = False
            for m in MEDIA_EXTENSIONS:
                if m in wikitext:
                    exts = True
                    break
            if exts:
                media_ext = [
                    "".join(m).strip()
                    for media in EXTEN_PATTERN.findall(wikitext)
                    if len(media[0]) <= MAX_MEDIA_LENGTH
                ]
            else:
                media_ext = []
            media = len(set(media_bra).union(set(media_ext)))
            wikilinks = len(links) - categories - len(media_bra)
            headings = len(
                [
                    1
                    for heading in re.findall("(={2,})(.*?)(={2,})", wikitext)
                    if len(heading[0]) <= level
                ]
            )
            return Features(page_length, refs, wikilinks, categories, media, headings)
        else:
            return Features(page_length, -1, -1, -1, -1, -1)
    except Exception as e:
        logging.exception(e)
        return Features(-2, -2, -2, -2, -2, -2)


def with_article_features(
    max_heading_level: int = 3,
) -> Transformation:
    """
    The quality model requires the following attributes:
    * Quantity:
      * # bytes (page length)
      * # links
      * # of headers (just levels 2 + 3)
      * # of images
    * Accessibility
      * bytes / header
    * Reliability
      * # of references
    * Annotations
      * # of categories

    Not included but considered:
    * Wikidata item completeness
    * alt-text coverage
    * issue templates
    * # of assessments / WikiProjects
    """

    excluded_wikis = [
        "wikidatawiki",
        "testwiki",
        "metawiki",
        "specieswiki",
        "mediawikiwiki",
    ]

    def transform(wikitext_df: DataFrame) -> DataFrame:
        with_features_df = (
            wikitext_df.where(F.col("page_namespace_id") == 0)
            .where(F.col("page_redirect_target").isNull())
            .where(~F.col("wiki_id").isin(*excluded_wikis))
            .withColumn(
                "features",
                extract_article_features(
                    "revision_text", "wiki_id", F.lit(max_heading_level)
                ),
            )
        )

        return with_features_df.select(
            "wiki_id",
            "page_id",
            "revision_id",
            "revision_dt",
            F.coalesce("features.page_length", F.lit(0)).alias("page_length"),
            F.coalesce("features.num_refs", F.lit(0)).alias("num_refs"),
            F.coalesce("features.num_wikilinks", F.lit(0)).alias("num_wikilinks"),
            F.coalesce("features.num_categories", F.lit(0)).alias("num_categories"),
            F.coalesce("features.num_media", F.lit(0)).alias("num_media"),
            F.coalesce("features.num_headings", F.lit(0)).alias("num_headings"),
        )

    return transform
