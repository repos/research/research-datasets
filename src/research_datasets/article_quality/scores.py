import dataclasses
import functools
from dataclasses import dataclass
from typing import Literal, TypeAlias

from pyspark.sql import DataFrame
from pyspark.sql import functions as F
from pyspark.sql import types as T

from research_datasets.article_quality.features import Features
from research_transformations.transformation import Transformation

StandardQuality: TypeAlias = Literal[0, 1]


@dataclass(frozen=True)
class LanguageAgnosticCoefficients:
    length: float = 0.395
    media: float = 0.114
    ref: float = 0.181
    heading: float = 0.123
    category: float = 0.070
    link: float = 0.115


def enrich_with_language_agnostic_quality(
    coefficients: LanguageAgnosticCoefficients,
    qual_pctile: float = 0.95,
) -> Transformation:
    """Enrich a article features with article scores for the language-agnostic
    quality model.

    https://meta.wikimedia.org/wiki/Research:Prioritization_of_Wikipedia_Articles/Language-Agnostic_Quality

    Using 95% percentile captures top quality but filters out extreme outliers.
    """
    # Determined empirically -- these are straightforward because they
    # are raw counts.
    MIN_MEDIA = 2
    MIN_CATS = 5

    # Determined empirically and equivalent to 10000 characters which is
    # exceeded even in Chinese Wikipedia (12526 characters in 95th
    # percentile). Remember it's a single transformation: sqrt(# bytes
    # in page).
    MIN_SQRTLENGTH = 100

    # Determined empirically -- mostly penalizes bot-driven wikis that
    # have many articles with lede paragraphs so 0 sections. Remember
    # it's a single transformation: # headings / sqrt(# bytes in page).
    # Equivalent to 1 section @ 100 characters; 2 sections at 400
    # characters; 3 sections at 900 characters etc.
    MIN_HEADINGS_PER = 0.1

    # Determined empirically but wikis it affects are mixed bags (some
    # bot-driven wikis have very heavily-referenced short articles).
    # Many wikis are closer to 0.25 - 0.3 range. Remember it's a single
    # transformation: # references / sqrt(# bytes in page). Equivalent
    # to 1 ref @ 45 characters; 2 refs at 180 characters; 3 refs at 400
    # characters etc. Or ~2 references per section taking the
    # min_headings_per logic.
    MIN_REFS_PER = 0.15

    # Determined empirically -- almost all wikis are over 0.1 with CJK
    # getting close to 0.2. Remember it's a double transformation:
    # sqrt(# wikilinks) / sqrt(# bytes in page). Equivalent to 1 link
    # per 100 characters, which is ~1 link per sentence.
    MIN_SQRTLINKS_PER = 0.1

    @F.udf(returnType=T.FloatType())
    def predict_quality(features: Features) -> float:
        """Predict quality of article."""
        return (
            (coefficients.length * features.page_length)
            + (coefficients.media * features.num_media)
            + (coefficients.ref * features.num_refs)
            + (coefficients.heading * features.num_headings)
            + (coefficients.category * features.num_categories)
            + (coefficients.link * features.num_wikilinks)
        )

    def _(article_features_df: DataFrame) -> DataFrame:
        in_columns = article_features_df.columns
        features = [
            ("page_length", MIN_SQRTLENGTH),
            ("num_media", MIN_MEDIA),
            ("num_headings", MIN_HEADINGS_PER),
            ("num_refs", MIN_REFS_PER),
            ("num_categories", MIN_CATS),
            ("num_wikilinks", MIN_SQRTLINKS_PER),
        ]

        non_null_features = functools.reduce(
            lambda c1, c2: (c1 & c2),
            [F.col(f).isNotNull() for f, _ in features],
        )

        transformed_features = F.struct(
            F.sqrt("page_length").alias("page_length"),
            F.col("num_media"),
            (F.col("num_headings") / F.sqrt("page_length")).alias("num_headings"),
            (F.col("num_refs") / F.sqrt("page_length")).alias("num_refs"),
            F.col("num_categories"),
            (F.sqrt("num_wikilinks") / F.sqrt("page_length")).alias("num_wikilinks"),
        )

        # apply transformations to some of the features
        article_features_df = article_features_df.where(non_null_features).withColumn(
            "transformed_features", transformed_features
        )

        max_aggs = [
            F.greatest(
                F.lit(min_value),
                F.percentile_approx(F.col(feature), qual_pctile),
            ).alias(f"max_{feature}")
            for feature, min_value in features
        ]
        max_vals_df = (
            article_features_df.select("wiki_id", "transformed_features.*")
            .groupby("wiki_id")
            .agg(*max_aggs)
        )

        trimmed_features = [
            (
                F.coalesce(
                    F.least(F.col(f"transformed_features.{f}"), F.col(f"max_{f}")),
                    F.lit(0),
                )
                / F.col(f"max_{f}")
            )
            .cast(T.FloatType())
            .alias(f)
            for f, _ in features
        ]

        return article_features_df.join(max_vals_df, on="wiki_id").select(
            *in_columns,
            predict_quality(F.struct(*trimmed_features)).alias(
                "language_agnostic_quality"
            ),
        )

    return _


@dataclass(frozen=True)
class StandardQualityThresholds:
    min_page_length: int = 8 * 1024
    min_num_categories: int = 1
    min_num_headings: int = 7
    min_num_media: int = 1
    min_num_references: int = 4
    min_num_wikilinks: int = 2
    min_num_conditions: int = 5


def with_standard_quality(
    thresholds: StandardQualityThresholds,
) -> Transformation:
    """Apply a global quality threshold based on
    simple heuristics. For an article to pass the
    threshold, it needs to satisfy 5 of
    these 6 conditions:
        - It should be at least 8kB in size
        - It should have at least 1 category
        - It should have at least 7 sections
        - It should be illustrated at least 1 image
        - It should have at least 4 references
        - It should have  at least 2 intra wiki links.
    """

    @F.udf(returnType=T.IntegerType())
    def apply_standard_quality_threshold(features: Features) -> StandardQuality:
        """Returns 1 if article satisfies enough conditions, else 0."""
        print(f"XXXX {features}")
        num_conditions = sum(
            [
                features.page_length >= thresholds.min_page_length,
                features.num_categories >= thresholds.min_num_categories,
                features.num_headings >= thresholds.min_num_headings,
                features.num_media >= thresholds.min_num_media,
                features.num_refs >= thresholds.min_num_references,
                features.num_wikilinks >= thresholds.min_num_wikilinks,
            ]
        )

        return 1 if num_conditions >= thresholds.min_num_conditions else 0

    def _(article_features_df: DataFrame) -> DataFrame:
        return article_features_df.withColumn(
            "standard_quality",
            apply_standard_quality_threshold(
                F.struct(*[f.name for f in dataclasses.fields(Features)])
            ),
        )

    return _


def with_quality_scores(
    wikidata_item_page_link_df: DataFrame,
    language_agnostic_coefficients: LanguageAgnosticCoefficients = (
        LanguageAgnosticCoefficients()
    ),
    standard_quality_thresholds: StandardQualityThresholds = (
        StandardQualityThresholds()
    ),
) -> Transformation:
    """Computes the article quailty scores based for the language agnostic model and the
    standard quality model for dataframe of article features"""

    def _(article_features_df: DataFrame) -> DataFrame:
        return (
            article_features_df.join(
                wikidata_item_page_link_df.select(
                    F.col("wiki_db").alias("wiki_id"), "page_id", "item_id"
                ),
                on=["wiki_id", "page_id"],
            )
            .transform(with_standard_quality(standard_quality_thresholds))
            .transform(
                enrich_with_language_agnostic_quality(language_agnostic_coefficients)
            )
            .drop("time_partition", *[f.name for f in dataclasses.fields(Features)])
        )

    return _
