import logging
import pathlib
from collections.abc import Iterable, Iterator

import fasttext  # type: ignore[import-untyped]
import pandas as pd
import pyspark.sql.types as T
from pyspark import SparkFiles
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F

from research_datasets.utils import download_file
from research_datasets.wikis import get_wikis
from research_transformations.transformation import Transformation


def _enrich_with_target_pids(
    mediawiki_private_linktarget_df: DataFrame, mediawiki_page_df: DataFrame
) -> Transformation:
    """Enrich target links in `mediawiki_pagelinks_df` with their page ids."""

    def _(mediawiki_pagelinks_df: DataFrame) -> DataFrame:
        page_links_df = mediawiki_pagelinks_df.alias("source").join(
            mediawiki_private_linktarget_df.alias("target"),
            how="inner",
            on=(
                (F.col("source.wiki_db") == F.col("target.wiki_db"))
                & (F.col("source.pl_target_id") == F.col("target.lt_id"))
            ),
        )
        page_links_df = page_links_df.select(
            "source.wiki_db",
            "source.pl_from",
            F.col("target.lt_title").alias("pl_title"),
        )
        enriched_page_links_df = (
            page_links_df.alias("pagelinks")
            .join(
                mediawiki_page_df.alias("page"),
                how="inner",
                on=(
                    (F.col("pagelinks.wiki_db") == F.col("page.wiki_db"))
                    & (F.col("pagelinks.pl_title") == F.col("page.page_title"))
                ),
            )
            .select("pagelinks.*", F.col("page.page_id").alias("pl_to"))
        )
        return enriched_page_links_df

    return _


def _filter_redirect_sources(mediawiki_redirect_df: DataFrame) -> Transformation:
    """Filter out all source links in `mediawiki_pagelinks_df` that are redirects."""

    def _(mediawiki_pagelinks_df: DataFrame) -> DataFrame:
        df = mediawiki_pagelinks_df.alias("pagelinks").join(
            mediawiki_redirect_df.alias("redirects"),
            how="left_anti",
            on=(
                (F.col("pagelinks.pl_from") == F.col("redirects.rd_from"))
                & (F.col("pagelinks.wiki_db") == F.col("redirects.wiki_db"))
            ),
        )
        return df.select("pagelinks.*")

    return _


def _with_resolved_redirects(
    mediawiki_redirect_df: DataFrame, mediawiki_page_df: DataFrame
) -> Transformation:
    """Append a new column for resolved target links in `mediawiki_pagelinks_df`."""

    def _(mediawiki_pagelinks_df: DataFrame) -> DataFrame:
        redirect_df = mediawiki_redirect_df.alias("redirect").join(
            mediawiki_page_df.alias("page"),
            how="inner",
            on=(
                (F.col("redirect.wiki_db") == F.col("page.wiki_db"))
                & (F.col("redirect.rd_title") == F.col("page.page_title"))
            ),
        )
        redirect_df = redirect_df.select(
            "redirect.*", F.col("page.page_id").alias("rd_to")
        )

        # Resolve redirects -- e.g.
        # [Barack Obama -> Chicago, Illinois (redirect) -> Chicago] becomes
        # [Barack Obama -> Chicago].
        resolved_pagelinks_df = mediawiki_pagelinks_df.alias("pagelinks").join(
            redirect_df.alias("redirects"),
            how="left",
            on=(
                (F.col("pagelinks.pl_to") == F.col("redirects.rd_from"))
                & (F.col("pagelinks.wiki_db") == F.col("redirects.wiki_db"))
            ),
        )
        resolved_pagelinks_df = resolved_pagelinks_df.select(
            "pagelinks.*",
            F.coalesce("redirects.rd_to", "pagelinks.pl_to").alias("resolved_pl_to"),
        )
        return resolved_pagelinks_df

    return _


def _enrich_with_qids(wikidata_item_page_link_df: DataFrame) -> Transformation:
    """Align source and target links in "mediawiki_pagelinks_df" with their QIDs."""

    def _(mediawiki_pagelinks_df: DataFrame) -> DataFrame:
        enriched_df = (
            mediawiki_pagelinks_df.alias("pagelinks")
            .join(
                wikidata_item_page_link_df.alias("source"),
                how="left",
                on=(
                    (F.col("pagelinks.pl_from") == F.col("source.page_id"))
                    & (F.col("pagelinks.wiki_db") == F.col("source.wiki_db"))
                ),
            )
            .join(
                wikidata_item_page_link_df.alias("target"),
                how="left",
                on=(
                    (F.col("pagelinks.resolved_pl_to") == F.col("target.page_id"))
                    & (F.col("pagelinks.wiki_db") == F.col("target.wiki_db"))
                ),
            )
            .select(
                "pagelinks.wiki_db",
                F.col("source.item_id").alias("qid_from"),
                F.col("pagelinks.pl_from").alias("pid_from"),
                F.col("target.item_id").alias("qid_to"),
                F.col("pagelinks.resolved_pl_to").alias("pid_to"),
            )
        )
        return enriched_df

    return _


def _unfasttextify_labels(labels: Iterable[str]) -> list[str]:
    return [label.removeprefix("__label__") for label in labels]


def enrich_with_topic_predictions(model_filename: str) -> Transformation:
    """Predict article topics for observations in `dataset_df`.

    Expects `model_filename` to be the name of the model file that has been
    added to the current spark context. Note that this is not the model path.
    Returns a new dataframe by appending the `outlinks`, `predicted_labels`
    and `embedding` columns to `dataset_df`.
    """

    def predict_udf(outlinks_batches: Iterable[pd.DataFrame]) -> Iterator[pd.DataFrame]:
        model = fasttext.load_model(SparkFiles.get(model_filename))
        for batch in outlinks_batches:
            batch_preds = (
                (
                    *model.predict(outlinks, k=-1),
                    model.get_sentence_vector(outlinks).tolist(),
                )
                for outlinks in batch["outlinks"]
            )
            batch_labels, batch_probabilities, batch_embeddings = zip(*batch_preds)
            batch["label"] = list(map(_unfasttextify_labels, batch_labels))
            batch["probability"] = batch_probabilities
            batch["embedding"] = batch_embeddings
            yield batch

    def _(dataset_df: DataFrame) -> DataFrame:
        outlinks_df = (
            dataset_df.filter(F.col("qid_to").isNotNull())
            .groupBy("wiki_db", "pid_from", "qid_from")
            .agg(F.concat_ws(" ", F.collect_set("qid_to")).alias("outlinks"))
        )

        # Schema for the dataframe returned by `predict_udf` which is all of
        # outlinks_df fields with prediction columns. Note that ideally we'd want to
        # return a list of structs like [(label, probability),...] for labels but pandas
        # udfs use arrow for serde which does not currently support arrays of structs,
        # so we return struct of lists instead.
        predictions_schema = (
            T.StructType(list(outlinks_df.schema.fields))
            .add("label", T.ArrayType(T.StringType()))
            .add("probability", T.ArrayType(T.FloatType()))
            .add("embedding", T.ArrayType(T.FloatType()))
        )
        predictions_df = outlinks_df.mapInPandas(
            predict_udf,  # type: ignore[arg-type]
            schema=predictions_schema,
        )
        predictions_df = predictions_df.withColumn(
            "predicted_labels", F.arrays_zip("label", "probability")
        ).drop("label", "probability")

        return predictions_df

    return _


def enrich_with_article_topics(
    spark: SparkSession,
    mediawiki_page_df: DataFrame,
    mediawiki_private_linktarget_df: DataFrame,
    mediawiki_redirect_df: DataFrame,
    wikidata_item_page_link_df: DataFrame,
    model_path: pathlib.Path,
    wiki_dbs: Iterable[str] | None = None,
) -> Transformation:
    """Enrich articles in `mediawiki_pagelinks_df` with topic labels."""

    if not wiki_dbs:
        # TODO: Modify this so that we no longer need the `spark` argument.
        wiki_dbs = [
            row.wiki_db for row in get_wikis(spark=spark, is_canonical=True).collect()
        ]

    filtered_page_df = mediawiki_page_df.filter(
        (F.col("page_namespace") == 0) & (F.col("wiki_db").isin(*wiki_dbs))
    ).select("page_id", "page_title", "wiki_db")

    filtered_private_linktarget_df = mediawiki_private_linktarget_df.filter(
        (F.col("lt_namespace") == 0) & (F.col("wiki_db").isin(*wiki_dbs))
    ).select("wiki_db", "lt_id", "lt_title")

    filtered_redirect_df = mediawiki_redirect_df.filter(
        (F.col("rd_namespace") == 0) & (F.col("wiki_db").isin(*wiki_dbs))
    ).select("wiki_db", "rd_from", "rd_title")

    filtered_item_page_link_df = wikidata_item_page_link_df.filter(
        (F.col("page_namespace") == 0) & (F.col("wiki_db").isin(*wiki_dbs))
    ).select("wiki_db", "page_id", "item_id")

    # `FastText` objects can not be pickled and thus broadcasted so we
    # add the model file to the spark context and then retrieve and load it
    # on every executor once for multiple batches of rows.
    spark.sparkContext.addFile(path=str(model_path))

    def _(mediawiki_pagelinks_df: DataFrame) -> DataFrame:
        filtered_pagelinks_df = mediawiki_pagelinks_df.filter(
            (F.col("pl_from_namespace") == 0) & (F.col("wiki_db").isin(*wiki_dbs))
        )

        article_topics_df = (
            filtered_pagelinks_df.select("wiki_db", "pl_from", "pl_target_id")
            .transform(
                _enrich_with_target_pids(
                    mediawiki_private_linktarget_df=filtered_private_linktarget_df,
                    mediawiki_page_df=filtered_page_df,
                )
            )
            .transform(
                _filter_redirect_sources(
                    mediawiki_redirect_df=filtered_redirect_df,
                )
            )
            .transform(
                _with_resolved_redirects(
                    mediawiki_redirect_df=filtered_redirect_df,
                    mediawiki_page_df=filtered_page_df,
                )
            )
            .transform(
                _enrich_with_qids(
                    wikidata_item_page_link_df=filtered_item_page_link_df,
                )
            )
            .transform(
                enrich_with_topic_predictions(
                    model_filename=model_path.name,
                ),
            )
        )
        return article_topics_df

    return _


def run(
    spark: SparkSession,
    mediawiki_snapshot: str,
    wikidata_snapshot: str,
    model_url: str,
    output: pathlib.Path,
    max_files_per_wiki: int,
    wikis: Iterable[str] | None = None,
) -> None:
    """Generate a dataset of article topic predictions."""
    logging.info("Downloading the model from: %s", model_url)
    model_path = download_file(url=model_url)

    mediawiki_page_df = spark.table("wmf_raw.mediawiki_page").filter(
        F.col("snapshot") == mediawiki_snapshot
    )
    mediawiki_redirect_df = spark.table("wmf_raw.mediawiki_redirect").filter(
        F.col("snapshot") == mediawiki_snapshot
    )
    mediawiki_target_df = spark.table("wmf_raw.mediawiki_private_linktarget").filter(
        F.col("snapshot") == mediawiki_snapshot
    )
    wikidata_item_page_link_df = spark.table("wmf.wikidata_item_page_link").filter(
        F.col("snapshot") == wikidata_snapshot
    )

    article_topics_pipeline = enrich_with_article_topics(
        spark=spark,
        wiki_dbs=wikis,
        model_path=model_path,
        mediawiki_page_df=mediawiki_page_df,
        mediawiki_redirect_df=mediawiki_redirect_df,
        mediawiki_private_linktarget_df=mediawiki_target_df,
        wikidata_item_page_link_df=wikidata_item_page_link_df,
    )
    dataset_df = (
        spark.table("wmf_raw.mediawiki_pagelinks")
        .filter(F.col("snapshot") == mediawiki_snapshot)
        .transform(article_topics_pipeline)
        .withColumn("snapshot", F.lit(mediawiki_snapshot))
    )

    # Repartitioning instead of coalesce because a drastic coalesce
    # can result in upstream stages taking place on fewer nodes than the
    # default partitioning. See the docs for coalesce for more info.
    dataset_df = dataset_df.repartition(max_files_per_wiki)
    dataset_df.write.partitionBy("snapshot", "wiki_db").mode("overwrite").format(
        "parquet"
    ).save(str(output))
