from datetime import datetime
from pathlib import Path

from pyspark.sql import DataFrame, SparkSession, Window
from pyspark.sql import functions as F

from research_transformations.transformation import Transformation


def _filter_qualifying_pageviews(pageview_actor_df: DataFrame) -> DataFrame:
    return (
        pageview_actor_df.where(F.col("page_id").isNotNull())
        .where(F.col("agent_type") == "user")
        .where(F.col("normalized_host").getItem("project_class") == "wikipedia")
    )


def _filter_pageviews_for_hour(hour: datetime) -> Transformation:
    def _(pageview_actor_df: DataFrame) -> DataFrame:
        return pageview_actor_df.where(
            f"year={hour.year} and month={hour.month} "
            f"and day={hour.day} and hour={hour.hour}"
        )

    return _


def _aggregate_pageviews_by_country(pageview_actor_df: DataFrame) -> DataFrame:
    agg_keys = [
        F.col("normalized_host").getItem("project").alias("project"),
        F.col("pageview_info").getItem("page_title").alias("page_title"),
        F.col("geocoded_data").getItem("country").alias("country"),
    ]
    return pageview_actor_df.groupby(agg_keys).agg(
        F.count("*").alias("pageviews"),
        F.countDistinct(F.hash("ip", "user_agent", "accept_language")).alias(
            "fingerprints"
        ),
    )


def _filter_top_pageviews_by_country(top_n: int) -> Transformation:
    def _(pageviews_df: DataFrame) -> DataFrame:
        window = Window.partitionBy("project", "country").orderBy(
            F.col("pageviews").desc()
        )
        top_pageviews_df = pageviews_df.withColumn(
            "rank", F.row_number().over(window)
        ).where(f"rank<={top_n}")
        return top_pageviews_df

    return _


def enrich_with_top_pages(
    hour_to_process: datetime,
    pageviews_threshold: int,
    fingerprint_threshold: int,
    top_n: int,
) -> Transformation:
    def _(pageview_actor_df: DataFrame) -> DataFrame:
        top_pages_df = (
            pageview_actor_df.transform(_filter_pageviews_for_hour(hour_to_process))
            .transform(_filter_qualifying_pageviews)
            .transform(_aggregate_pageviews_by_country)
            .where(F.col("pageviews") > pageviews_threshold)
            .where(F.col("fingerprints") > fingerprint_threshold)
            .transform(_filter_top_pageviews_by_country(top_n))
            .select("country", "project", "page_title")
        )
        return top_pages_df

    return _


def run(
    spark: SparkSession,
    output: Path,
    hour_to_process: datetime,
    top_n: int = 100,
    pageviews_threshold: int = 500,
    fingerprint_threshold: int = 100,
) -> None:
    """Generate a dataset of top pages by pageviews."""
    dataset_df = spark.table("wmf.pageview_actor").transform(
        enrich_with_top_pages(
            hour_to_process=hour_to_process,
            top_n=top_n,
            pageviews_threshold=pageviews_threshold,
            fingerprint_threshold=fingerprint_threshold,
        )
    )
    (
        dataset_df.orderBy("country", "project", "page_title")
        .repartition(1)
        .write.csv(str(output), compression="uncompressed", mode="overwrite")
    )
