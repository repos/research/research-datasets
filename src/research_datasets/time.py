from dataclasses import dataclass
from datetime import datetime


@dataclass(frozen=True)
class Period:
    start: datetime
    end: datetime
