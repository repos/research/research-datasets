from collections.abc import Iterable
from dataclasses import dataclass
from pathlib import Path
from typing import Final

import fsspec  # type: ignore
import pandas as pd
from pyspark.sql import Column as SparkColumn
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F

from research_datasets.utils import configure_fsspec
from research_transformations.evaluation import binary_classifier_metrics, compare_auc
from research_transformations.transformation import Transformation

REGRESSION_THRESHOLD_PROJECTS: Final[float] = 0.05
"""percentage of projects allowed to have a small regression in model quality"""

AUC_DELTA_THRESHOLD: Final[float] = 0.1
"""the "allowed" delta in auc, a larger decrease is considered a regression"""

AUC_MAX_DELTA: Final[float] = 0.25
"""maximum allowed decrease in auc"""


def enrich_with_revert_risk_model_metrics(
    grouping_cols: Iterable[SparkColumn] = (),
) -> Transformation:
    return binary_classifier_metrics(
        pred_col=F.col("revision_revert_risk"),
        label_col=F.col("revision_is_identity_reverted"),
        grouping_cols=grouping_cols,
    )


@dataclass(frozen=True)
class ModelComparison:
    auc_dist: pd.DataFrame
    desc: str
    promote_decision: bool


def compare_revert_risk_model(
    candidate_metrics_df: DataFrame,
    reference_metrics_df: DataFrame,
    compare_cols: Iterable[SparkColumn] = (),
) -> ModelComparison:
    candidate_auc_df = candidate_metrics_df.select(*compare_cols, "auc").distinct()
    reference_auc_df = reference_metrics_df.select(*compare_cols, "auc").distinct()

    auc_dist_df = compare_auc(
        candidate_auc_df=candidate_auc_df,
        reference_auc_df=reference_auc_df,
        compare_cols=compare_cols,
    )

    auc_diff_regression_threshold = F.col("auc_diff") < -AUC_DELTA_THRESHOLD
    auc_diff_improvement_threshold = F.col("auc_diff") > AUC_DELTA_THRESHOLD

    stats = (
        auc_dist_df.agg(
            (F.min("auc_diff")).alias("max_regression"),
            (F.max("auc_diff")).alias("max_improvement"),
            (
                F.sum(
                    F.when(auc_diff_regression_threshold, F.col("count")).otherwise(0)
                )
                / F.sum(F.col("count"))
            ).alias("regression_p"),
            (
                F.sum(
                    F.when(auc_diff_improvement_threshold, F.col("count")).otherwise(0)
                )
                / F.sum(F.col("count"))
            ).alias("improvement_p"),
        )
    ).collect()[0]

    promote_decision = True
    desc = ""
    if stats.regression_p > REGRESSION_THRESHOLD_PROJECTS:
        promote_decision = False
        desc += (
            f"Reason: for {stats.regression_p:.2f}% of projects the AUC has"
            f"decreased by more than {REGRESSION_THRESHOLD_PROJECTS}\n"
        )
    if stats.max_regression < -AUC_MAX_DELTA:
        promote_decision = False
        desc += (
            f"Reason: the AUC for a project has "
            "decreased by {-stats.max_regression:.2f}, "
            f"while the max allowed regression is {AUC_MAX_DELTA}\n"
        )

    if promote_decision:
        desc += "No quality issues found for model"
    else:
        desc = "Model quality issues found:\n" + desc

    auc_dist: pd.DataFrame = auc_dist_df.toPandas()  # type: ignore
    return ModelComparison(
        auc_dist=auc_dist, desc=desc, promote_decision=promote_decision
    )


def run_model_evaluation(
    spark: SparkSession,
    predictions_dataset: Path,
    output: Path,
) -> None:
    """Evaluate model performance based on a predictions dataset"""
    predictions_df = spark.read.parquet(str(predictions_dataset))

    # calculate evaluation metrics for each wiki
    grouping_cols = (F.col("wiki_db"),)
    metrics_df = predictions_df.transform(
        enrich_with_revert_risk_model_metrics(grouping_cols=grouping_cols)
    ).cache()
    metrics_df.repartition(1).write.mode("overwrite").format("parquet").save(
        str(output / "metrics")
    )


def run_model_comparison(
    spark: SparkSession,
    candidate_model_dir: Path,
    reference_model_dir: Path,
    output: Path,
) -> None:
    """Compare the metrics of a candidate model to a reference model."""

    candidate_metrics_df = spark.read.parquet(str(candidate_model_dir / "metrics"))
    reference_metrics_df = spark.read.parquet(str(reference_model_dir / "metrics"))
    compare_cols = (F.col("wiki_db"),)
    model_comparison = compare_revert_risk_model(
        candidate_metrics_df=candidate_metrics_df,
        reference_metrics_df=reference_metrics_df,
        compare_cols=compare_cols,
    )

    configure_fsspec()

    def hdfs_path(path: Path) -> str:
        return f"hdfs://analytics-hadoop{path}"

    promote_file = "ACCEPTED" if model_comparison.promote_decision else "REJECTED"
    with fsspec.open(hdfs_path(output / promote_file), mode="w") as _:
        pass
    with fsspec.open(hdfs_path(output / "DECISION"), mode="wt") as file:
        file.write(promote_file)
    with fsspec.open(hdfs_path(output / "description.txt"), mode="wt") as file:
        file.write(model_comparison.desc)
    with fsspec.open(hdfs_path(output / "auc_diff.csv"), mode="wt") as file:
        file.write(model_comparison.auc_dist.to_csv(index=False))
