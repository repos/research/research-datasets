import json
import logging
from collections.abc import Callable, Sequence
from pathlib import Path
from typing import Any, TypeAlias, cast

from knowledge_integrity.models.revertrisk import RevertRiskModel
from pyspark.ml.evaluation import BinaryClassificationEvaluator
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T
from xgboost.spark import (
    SparkXGBClassifier,
    SparkXGBClassifierModel,
)

from research_datasets.revert_risk.features import LanguageAgnosticFeaturizer
from research_datasets.revert_risk.model import make_revert_risk_model, save_model
from research_ml.pyspark_ml import FeatureNames, assemble_features_vector
from research_transformations.transformation import stratified_sample

# default number of yarn executors to use
NUM_WORKERS = 10
# label column for revert risk model
IS_REVERTED_COL = "revision.revision_is_identity_reverted"


XGBClassifierTrainer: TypeAlias = Callable[[DataFrame], SparkXGBClassifierModel]


def train_cross_validated_model(
    binary_label_col: str,
    num_workers: int,
    grid_params: Sequence[tuple[str, Sequence[float]]],
    feature_names: FeatureNames,
    # kwargs are additional params passed to the SparkXGBClassifier
    # e.g. missing=0.0
    **kwargs: Any,
) -> XGBClassifierTrainer:
    classifier = SparkXGBClassifier(
        features_col="features",
        feature_names=feature_names,
        label_col="label",
        num_workers=num_workers,
        # gpu device requires cuda
        device="cpu",
        **kwargs,
    )

    evaluator = BinaryClassificationEvaluator(
        rawPredictionCol="rawPrediction",
        labelCol="label",
    )

    param_grid_builder = ParamGridBuilder()
    for name, config in grid_params:
        param_grid_builder = param_grid_builder.addGrid(
            classifier.getParam(name), list(config)
        )

    param_grid = param_grid_builder.build()

    cross_validator = CrossValidator(
        estimator=classifier, evaluator=evaluator, estimatorParamMaps=param_grid
    )

    def transform(df: DataFrame) -> SparkXGBClassifierModel:
        features_df = (
            df.transform(assemble_features_vector(feature_names))
            .drop(*feature_names)
            .withColumn("label", F.col(binary_label_col).cast(T.DoubleType()))
            .cache()
        )

        cross_validated = cross_validator.fit(features_df)

        logging.info("Evaluation metrics for param grid search")
        for params, metric in zip(param_grid, cross_validated.avgMetrics):
            s = "Params("
            s += ",".join([f"{p.name}={v}" for p, v in params.items()])
            s += f") Evaluation: {metric}"
            logging.info(s)

        best_model = cast(SparkXGBClassifierModel, cross_validated.bestModel)

        return best_model

    return transform


def train_model(
    label_col: str,
    num_workers: int,
    feature_names: FeatureNames,
    # kwargs are additional params passed to the SparkXGBClassifier
    # e.g. missing=0.0
    **kwargs: Any,
) -> XGBClassifierTrainer:
    classifier = SparkXGBClassifier(
        features_col="features",
        feature_names=feature_names,
        label_col=label_col,
        num_workers=num_workers,
        # gpu device requires cuda
        device="cpu",
        **kwargs,
    )

    def transform(df: DataFrame) -> SparkXGBClassifierModel:
        # Transform features into a spark ML Vector type
        df = df.transform(assemble_features_vector(feature_names)).drop(*feature_names)
        model: SparkXGBClassifierModel = classifier.fit(df)

        return model

    return transform


# TODO: Discuss with Diego, is it worth doing a param grid search? For now, using
# the xgboost classifier defaults
# grid_params = [
#     ("max_depth", [2, 5]),
#     ("n_estimators", [10, 100]),
# ]
# trainer = train_cross_validated_model("rev_is_identity_reverted", 40, grid_params)


def train_language_agnostic_model(
    features_df: DataFrame, num_workers: int = NUM_WORKERS, balance_labels: bool = True
) -> RevertRiskModel:
    if balance_labels:
        num_reverts = features_df.where(F.col(IS_REVERTED_COL)).count()
        features_df = features_df.transform(
            stratified_sample(
                cols=[IS_REVERTED_COL],
                strata_size=num_reverts,
                seed=43,
                repartition=True,
            )
        )

    revert_risk_trainer = train_model(
        label_col=IS_REVERTED_COL,
        num_workers=num_workers,
        feature_names=LanguageAgnosticFeaturizer.feature_names,
        missing=0.0,
    )

    spark_xgboost_model: SparkXGBClassifierModel = revert_risk_trainer(features_df)
    return make_revert_risk_model(spark_xgboost_model)


def run(
    spark: SparkSession,
    training_dataset: Path,
    model_uri: str,
    balance_labels: bool = False,
    xgboost_param_grid: str | None = None,
) -> None:
    """Generate a dataset for the revert risk models."""
    # spark = (
    #     SparkSession.builder.config("spark.dynamicAllocation.enabled", False)
    #     .config("spark.locality.wait", 0)
    #     .getOrCreate()
    # )
    # extract the number of workers, which is required as dymamic allocation is disabled
    instances = int(spark.sparkContext.getConf().get("spark.executor.instances"))
    num_workers = int(instances * 0.8)
    training_df = spark.read.parquet(str(training_dataset))

    if xgboost_param_grid:
        # Param grid search not used for production revert risk model training
        # This is for experimentation, uses
        # research_ml.xgboost.train.train_model_param_grid directly
        feature_names = LanguageAgnosticFeaturizer.feature_names
        param_grid = json.loads(xgboost_param_grid)
        trainer = train_cross_validated_model(
            IS_REVERTED_COL, num_workers, param_grid, feature_names, missing=0.0
        )
        xgb_model = trainer(training_df)
        model = make_revert_risk_model(xgb_model)

    else:
        model = train_language_agnostic_model(
            training_df, num_workers=num_workers, balance_labels=balance_labels
        )

    save_model(model, model_uri)
