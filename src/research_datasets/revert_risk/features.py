import functools
import json
import logging
from collections.abc import Iterable
from pathlib import Path
from typing import Any, Final, Protocol, TypeAlias

import knowledge_integrity.models.revertrisk as rrla
import knowledge_integrity.models.revertrisk_multilingual.model as rrml
from knowledge_integrity.schema import (
    InvalidJSONError,
    Revision,
)
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T

from research_datasets.revert_risk import ModelKind
from research_datasets.revert_risk.base_features import (
    generate_base_features,
    ki_revision_json,
)
from research_datasets.time import Period
from research_transformations.transformation import (
    Transformation,
    filter_disputed_reverts,
    stratified_sample,
)

Features: TypeAlias = dict[str, Any]


class Featurizer(Protocol):
    @classmethod
    def extract(cls, revision: Revision) -> Features: ...


class LanguageAgnosticFeaturizer:
    feature_names: Final[list[str]] = [
        "user_groups",
        "page_seconds_since_previous_revision",
        "user_revision_count",
        "user_age",
        "page_age",
        "user_is_anonymous",
        "revision_text_bytes",
        "revision_text_bytes_diff",
        "has_comment",
        "quality_score",
        "parent_quality_score",
        "quality_change",
        "quality_change_range",
        "quality_range",
        "text_length",
        "parent_text_length",
        "reference_count",
        "parent_reference_count",
        "wikilink_count",
        "parent_wikilink_count",
        "category_count",
        "parent_category_count",
        "media_count",
        "parent_media_count",
        "heading_count",
        "parent_heading_count",
    ]

    @classmethod
    def extract(cls, revision: Revision) -> Features:
        return rrla.extract_features(revision=revision, features=cls.feature_names)


class MultilingualFeaturizer:
    feature_names: Final[list[str]] = [
        "change_Argument",
        "change_Category",
        "change_Comment",
        "change_ExternalLink",
        "change_Gallery",
        "change_HTMLEntity",
        "change_Heading",
        "change_List",
        "change_Media",
        "change_Paragraph",
        "change_Punctuation",
        "change_Reference",
        "change_Section",
        "change_Sentence",
        "change_Table",
        "change_Table Element",
        "change_Template",
        "change_Text",
        "change_Text Formatting",
        "change_Whitespace",
        "change_Wikilink",
        "change_Word",
        "comment",
        "insert_Argument",
        "insert_Category",
        "insert_Comment",
        "insert_ExternalLink",
        "insert_Gallery",
        "insert_HTMLEntity",
        "insert_Heading",
        "insert_List",
        "insert_Media",
        "insert_Paragraph",
        "insert_Punctuation",
        "insert_Reference",
        "insert_Section",
        "insert_Sentence",
        "insert_Table",
        "insert_Table Element",
        "insert_Template",
        "insert_Text",
        "insert_Text Formatting",
        "insert_Whitespace",
        "insert_Wikilink",
        "insert_Word",
        "is_android_app_edit",
        "is_autopatrolled",
        "is_autoreview",
        "is_autoreviewer",
        "is_bureaucrat",
        "is_editor",
        "is_extendedconfirmed",
        "is_interface-admin",
        "is_ios_app_edit",
        "is_ipblock-exempt",
        "is_mobile_app_edit",
        "is_mobile_edit",
        "is_mobile_web_edit",
        "is_patroller",
        "is_reviewer",
        "is_rollbacker",
        "is_suppressredirect",
        "is_sysop",
        "is_templateeditor",
        "is_trusted",
        "is_uploader",
        "is_visualeditor",
        "is_wikieditor",
        "page_title",
        "remove_Argument",
        "remove_Category",
        "remove_Comment",
        "remove_ExternalLink",
        "remove_Gallery",
        "remove_HTMLEntity",
        "remove_Heading",
        "remove_List",
        "remove_Media",
        "remove_Paragraph",
        "remove_Punctuation",
        "remove_Reference",
        "remove_Section",
        "remove_Sentence",
        "remove_Table",
        "remove_Table Element",
        "remove_Template",
        "remove_Text",
        "remove_Text Formatting",
        "remove_Whitespace",
        "remove_Wikilink",
        "remove_Word",
        "revision_text_bytes_diff",
        "texts_change",
        "texts_insert",
        "texts_removed",
        "user_is_anonymous",
        "wiki_db",
    ]

    @classmethod
    def extract(cls, revision: Revision) -> Features:
        return rrml.extract_features(revision=revision, features=cls.feature_names)


def get_featurizer(model_kind: ModelKind) -> Featurizer:
    match model_kind:
        case ModelKind.LANGUAGE_AGNOSTIC:
            return LanguageAgnosticFeaturizer
        case ModelKind.MULTILINGUAL:
            return MultilingualFeaturizer


def extract_features(featurizer: Featurizer, revision_json: str) -> str | None:
    """Extract revision features from a revision info json object.

    Builds a `CurrentRevision` instance from the revision json and passes it
    to `featurizer` to extract features from it. Returns the resulting dictionary
    of features as a json object.
    """
    try:
        revision = Revision.from_json(revision_json)
        extracted_features = featurizer.extract(revision)
        features_json = json.dumps(extracted_features)
        return features_json
    except InvalidJSONError as err:
        logging.exception(err)
        return None


def with_revision_features(featurizer: Featurizer) -> Transformation:
    """Append a revision features column to the dataframe.

    The `features` column contains a json object with keys representing feature names
    and the features in this object depend on `featurizer`. This transformation depends
    on all columns required to create a `knowledge_integrity.schema.Revision`
    instance existing in the dataframe that it gets applied to.
    """

    def transform(base_features_df: DataFrame) -> DataFrame:
        # TODO: this udf currently returns the features as a json string.
        # In order to return a struct we'll need to define a schema but there
        # are currently dozens for each model all of which will need to
        # be in the schema!
        extract_features_udf = F.udf(
            f=functools.partial(extract_features, featurizer),
            returnType=T.StringType(),
        )

        features_df = base_features_df.withColumn(
            "features",
            extract_features_udf(ki_revision_json()),
        )
        return features_df

    return transform


def enrich_with_features(
    featurizer: Featurizer,
    max_rows_per_wiki: int | None = None,
    drop_disputed_reverts: bool = False,
    drop_non_permanent_user_edits: bool = False,
    drop_bot_edits: bool = False,
) -> Transformation:
    """Get a training or testing feature set for revert risk models.

    Returns a transformation that expects a dataframe of base features.
    If `filter_disputed_reverts` is True, all reverts whose reverting edits were
    also reverted will be removed. Similarly, if `filter_anonymous_edits` is True,
    all edits by ip users will be removed.
    """

    def transform(base_features_df: DataFrame) -> DataFrame:
        if drop_non_permanent_user_edits:
            base_features_df = base_features_df.filter(
                F.col("revision.event_user_is_permanent"),
            )
        if drop_bot_edits:
            base_features_df = base_features_df.filter(
                ~(F.size("revision.event_user_is_bot_by_historical") > 0)
            )
        if drop_disputed_reverts:
            base_features_df = base_features_df.transform(
                filter_disputed_reverts(
                    reverting_rev_df=base_features_df,
                    rev_id="revision.revision_id",
                    reverting_rev_id="revision.revision_first_identity_reverting_revision_id",
                    is_reverted="revision.revision_is_identity_reverted",
                    page_id="revision.page_id",
                    wiki_db="revision.wiki_db",
                )
            )
        if max_rows_per_wiki:
            base_features_df = base_features_df.transform(
                stratified_sample(
                    cols=["revision.wiki_db"],
                    strata_size=max_rows_per_wiki,
                    seed=42,
                    repartition=True,
                )
            )

        return base_features_df.transform(with_revision_features(featurizer=featurizer))

    return transform


def generate_revert_risk_features(
    spark: SparkSession,
    snapshot: str,
    wikis: Iterable[str],
    period: Period,
    featurizer: Featurizer,
) -> DataFrame:
    """Returns a dataframe with features for a specific revert risk model. This
    intended for inference use cases, without storing the base or model features
    on disk at all. For training/evaluation datasets use the `run` method."""
    return generate_base_features(spark, snapshot, wikis, period).transform(
        with_revision_features(featurizer)
    )


def run(
    spark: SparkSession,
    base_features: Path,
    output: Path,
    model: ModelKind = ModelKind.LANGUAGE_AGNOSTIC,
    partition_output: tuple[str, int] | None = None,
    split_dataset: tuple[str, float] | None = None,
    max_rows_per_wiki: int | None = None,
    disputed_reverts: bool = False,
    drop_non_permanent_user_edits: bool = False,
    bot_edits: bool = True,
) -> None:
    """Generate datasets for training revert risk models."""
    # TODO, since the typer command mirroring is changing, I am
    # ommitting the wiki/start/end arguments here
    base_features_df = spark.read.parquet(str(base_features))

    features_df = base_features_df.transform(
        enrich_with_features(
            featurizer=get_featurizer(model_kind=model),
            drop_disputed_reverts=not disputed_reverts,
            drop_non_permanent_user_edits=drop_non_permanent_user_edits,
            drop_bot_edits=not bot_edits,
            max_rows_per_wiki=max_rows_per_wiki,
        )
    )

    def write_df(df: DataFrame, path: str) -> None:
        if partition_output:
            partition_cols = partition_output[0].split(",")
            max_files_per_partition = partition_output[1]
            # Repartitioning instead of coalesce because a drastic coalesce
            # can result in upstream stages taking place on fewer nodes than the
            # default partitioning. See the docs for coalesce for more info.
            df.repartition(numPartitions=max_files_per_partition).write.partitionBy(
                *partition_cols
            ).mode("overwrite").format("parquet").save(path)
        else:
            df.write.mode("overwrite").format("parquet").save(path)

    if split_dataset:
        second_output_path = split_dataset[0]
        split = split_dataset[1]
        features_df = features_df.cache()
        first, second = features_df.randomSplit([1 - split, split])
        write_df(first, str(output))
        write_df(second, second_output_path)
    else:
        write_df(features_df, str(output))
