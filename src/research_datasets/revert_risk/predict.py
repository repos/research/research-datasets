from collections.abc import Iterable, Sequence
from pathlib import Path

from knowledge_integrity.models.revertrisk import RevertRiskModel
from pyspark.ml.linalg import DenseVector
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T
from xgboost.spark import SparkXGBClassifierModel

from research_datasets.revert_risk.features import (
    Featurizer,
    generate_revert_risk_features,
)
from research_datasets.revert_risk.model import load_model, make_spark_xgb_classifier
from research_datasets.time import Period
from research_ml.pyspark_ml import assemble_features_vector
from research_transformations.transformation import Transformation


def with_predictions(
    model: SparkXGBClassifierModel,
    feature_names: Sequence[str],
) -> Transformation:
    """The training_df is the output of the features pipeline"""

    def _(features_df: DataFrame) -> DataFrame:
        df = features_df.transform(assemble_features_vector(feature_names))
        predictions_df: DataFrame = model.transform(df).drop("features")
        return predictions_df

    return _


def with_revert_risk_probability(model: RevertRiskModel) -> Transformation:
    """Returns a transformation that expects a dataframe with a `features`
    column that contains the features used by the `model` and returns the dataframe
    with additional revision_revert_risk column.
    """
    spark_xgb_classifier = make_spark_xgb_classifier(model)

    assert model.classifier.feature_names, "feature names required"

    make_predictions = with_predictions(
        model=spark_xgb_classifier,
        feature_names=model.classifier.feature_names,
    )

    # the ML related columns that the pyspark xgboost classifier is generating
    ml_cols = ["rawPrediction", "prediction", "probability"]

    @F.udf(returnType=T.FloatType())
    def rev_revert_risk(probability: DenseVector) -> float:
        return float(probability[1])

    def transform(features_df: DataFrame) -> DataFrame:
        return (
            features_df.transform(make_predictions)
            .withColumn("revision_revert_risk", rev_revert_risk("probability"))
            .drop(*ml_cols)
        )

    return transform


def generate_revert_risk_predictions(
    spark: SparkSession,
    snapshot: str,
    wikis: Iterable[str],
    period: Period,
    # TODO the featurizer and model_uri should be tied.
    featurizer: Featurizer,
    model_uri: str,
) -> DataFrame:
    """Generate a dataframe of revert risk predictions.
    Without any materialized intermediate state, this job reads from datalake
    sources and returns a dataframe that will run batch inference for the requested
    wikis, period and model."""
    model = load_model(model_uri)
    return generate_revert_risk_features(
        spark, snapshot, wikis, period, featurizer
    ).transform(with_revert_risk_probability(model))


def run(
    spark: SparkSession,
    features_dataset: Path,
    model_uri: str,
    output: Path,
) -> None:
    """Generate a dataset for the revert risk models predictions"""
    features_df = spark.read.parquet(str(features_dataset))
    model = load_model(model_uri)
    predictions_df = features_df.transform(with_revert_risk_probability(model))
    predictions_df.write.mode("overwrite").format("parquet").save(str(output))
