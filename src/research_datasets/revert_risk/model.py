"""Utils for using the revert risk model on spark"""

import ssl

import fsspec  # type: ignore
import joblib  # type: ignore
from knowledge_integrity.models.revertrisk import MODEL_VERSION, RevertRiskModel
from xgboost import XGBClassifier
from xgboost.spark import SparkXGBClassifierModel

from research_datasets.utils import configure_fsspec


def make_revert_risk_model(
    spark_xgboost_model: SparkXGBClassifierModel,
) -> RevertRiskModel:
    """Construct a SparkXGBClassifierModel to use for distributed inference"""
    return RevertRiskModel(
        model_version=MODEL_VERSION,
        classifier=spark_xgboost_model.get_booster(),
        supported_wikis=[],  # TODO default for the language-agnostic classifier?
    )


def make_spark_xgb_classifier(model: RevertRiskModel) -> SparkXGBClassifierModel:
    """Construct a SparkXGBClassifierModel to use for distributed inference"""
    xgb_classifier = XGBClassifier(device="cpu")
    xgb_classifier._Booster = model.classifier
    spark_xbg = SparkXGBClassifierModel(xgb_classifier)
    spark_xbg.set_device("cpu")
    spark_xbg.set(spark_xbg.getParam("use_gpu"), False)
    spark_xbg.set(spark_xbg.getParam("tree_method"), "exact")
    return spark_xbg


# TODO this should be in KI package, or preferably a more general
# model library, including the env variable hacking
def save_model(model: RevertRiskModel, file_uri: str) -> None:
    if file_uri.startswith("hdfs"):
        configure_fsspec()
    with fsspec.open(file_uri, mode="wb") as file:
        joblib.dump(model, file)


# the load_model method in KI only supports local files
# TODO this should be in KI package, or preferably a more general
# model library
def load_model(file_uri: str) -> RevertRiskModel:
    if file_uri.startswith("hdfs"):
        configure_fsspec()

    ssl_ctx = ssl.create_default_context()
    ssl_ctx.load_verify_locations("/etc/ssl/certs/ca-certificates.crt")
    with fsspec.open(file_uri, mode="rb", ssl=ssl_ctx) as file:
        model: RevertRiskModel = joblib.load(file)
        return model
