from enum import Enum, unique


@unique
class ModelKind(str, Enum):
    LANGUAGE_AGNOSTIC = "language-agnostic"
    MULTILINGUAL = "multilingual"
