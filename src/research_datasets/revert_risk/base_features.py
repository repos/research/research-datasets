"""Base features for ML models. Currently geared towards revert risk models,
to be generalized to a shared ML features dataset pipeline"""

from collections.abc import Iterable
from pathlib import Path

from pyspark.sql import Column, DataFrame, SparkSession
from pyspark.sql import functions as F

from research_datasets.time import Period
from research_datasets.wikidiff.diff import apply_diff
from research_transformations.transformation import (
    Transformation,
    with_parent_revision_history,
)


def ki_revision_json() -> Column:
    """
    A json column based on the base features dataframe that
    can be parsed into a Revision base model.
    """
    user_col = F.struct(
        F.col("revision.event_user_id").alias("id"),
        F.col("revision.event_user_text").alias("name"),
        F.col("revision.event_user_revision_count").alias("editcount"),
        F.col("revision.event_user_groups").alias("groups"),
        F.col("revision.event_user_registration_timestamp").alias(
            "registration_timestamp"
        ),
    ).alias("user")

    page_col = F.struct(
        F.col("revision.page_id").alias("id"),
        F.col("revision.page_title").alias("title"),
        F.col("revision.page_first_edit_timestamp").alias("first_edit_timestamp"),
    ).alias("page")

    lang_col = F.regexp_replace("revision.wiki_db", "wiki", "").alias("lang")

    parent_revision_col = F.struct(
        F.col("parent.revision_id").alias("id"),
        lang_col,
        F.col("parent.revision_text").alias("text"),
        F.col("parent.event_comment").alias("comment"),
        F.col("parent.event_timestamp").alias("timestamp"),
        F.col("parent.revision_text_bytes").alias("bytes"),
        F.col("parent.revision_tags").alias("tags"),
    ).alias("parent")

    revision_col = F.struct(
        F.col("revision.revision_id").alias("id"),
        lang_col,
        F.col("revision.revision_text").alias("text"),
        F.col("revision.event_comment").alias("comment"),
        F.col("revision.event_timestamp").alias("timestamp"),
        F.col("revision.revision_text_bytes").alias("bytes"),
        F.col("revision.revision_tags").alias("tags"),
        parent_revision_col,
        page_col,
        user_col,
    ).alias("revision")

    return F.to_json(revision_col).alias("ki_revision_json")


def _with_revision_texts(
    wikidiff_df: DataFrame,
) -> Transformation:
    wikidiff_df = wikidiff_df.select(
        "wiki_db", "revision_id", "revision_text", "parent_revision_diff"
    )

    def _(mediawiki_history_df: DataFrame) -> DataFrame:
        revision_text_df = (
            mediawiki_history_df.join(
                wikidiff_df,
                on=["wiki_db", "revision_id"],
                how="inner",
            )
            .withColumn(
                "parent_revision_text",
                apply_diff(
                    F.col("revision_text"),
                    F.col("parent_revision_diff"),
                ),
            )
            .drop("parent_revision_diff")
        )
        return revision_text_df

    return _


def enrich_with_base_features(
    wikidiff_df: DataFrame,
    wiki_dbs: Iterable[str],
    period: Period,
) -> Transformation:
    """Get a dataset of base features to be used for generating revert risk features.

    Returns a dataframe obtained by joining revision history with wikitext
    for the period of interest.
    """
    gte_period_start = F.col("revision_timestamp") >= f"{period.start}"
    lte_period_end = F.col("revision_timestamp") < f"{period.end}"

    # The wikidiff dataframe can be filtered for the period as the parent
    # revision diff is already present even though its timestamp could be outside
    # the specified period.
    wikidiff_df = wikidiff_df.filter(
        (F.col("wiki_db").isin(*wiki_dbs)) & gte_period_start & lte_period_end
    )

    def _(mediawiki_history_df: DataFrame) -> DataFrame:
        revisions_df = (
            mediawiki_history_df.filter(F.col("wiki_db").isin(*wiki_dbs))
            .filter(F.col("page_namespace_is_content"))
            .filter(F.col("event_entity") == "revision")
            .withColumn("revision_timestamp", F.col("event_timestamp"))
        )

        # Filter out all revisions newer than period end so joins are cheaper.
        # Don't filter older revisions so we don't miss revisions with older parents.
        parent_revisions_df = revisions_df.filter(lte_period_end)

        # Self join the mediawiki history to get all revision
        # and parent revision columns
        features_df = (
            revisions_df.filter(F.col("revision_parent_id") != 0)
            .filter(gte_period_start & lte_period_end)
            .transform(with_parent_revision_history(parent_df=parent_revisions_df))
        )

        # Add revision and parent revision wikitext columns
        features_df = (
            features_df.withColumn("wiki_db", F.col("revision.wiki_db"))
            .withColumn("revision_id", F.col("revision.revision_id"))
            .transform(_with_revision_texts(wikidiff_df=wikidiff_df))
            .select(
                F.struct(F.col("revision.*"), F.col("revision_text")).alias("revision"),
                F.struct(
                    F.col("parent.*"),
                    F.col("parent_revision_text").alias("revision_text"),
                ).alias("parent"),
            )
        )

        # Fail at planning time if the Revision base model columns are not valid
        _ = features_df.select(ki_revision_json())

        return features_df

    return _


def generate_base_features(
    spark: SparkSession,
    snapshot: str,
    wikis: Iterable[str],
    period: Period,
) -> DataFrame:
    """Returns a dataframe of base features as itermediate representation
    before model specific features are extracted using the knowledge integrity repo."""

    mediawiki_history_df = spark.table("wmf.mediawiki_history").filter(
        F.col("snapshot") == snapshot
    )

    wikidiff_df = spark.table("research.wikidiff").filter(F.col("snapshot") == snapshot)

    return mediawiki_history_df.transform(
        enrich_with_base_features(
            wikidiff_df=wikidiff_df,
            wiki_dbs=wikis,
            period=period,
        )
    )


def run(
    spark: SparkSession,
    snapshot: str,
    wikis: Iterable[str],
    period: Period,
    output: Path,
    partition_output: tuple[str, int] | None = ("wiki_db", 10),
) -> None:
    """Generate a dataset of base features"""
    features_df = generate_base_features(spark, snapshot, wikis, period)
    if partition_output:
        partition_cols = partition_output[0].split(",")
        max_files_per_partition = partition_output[1]
        features_df.repartition(
            numPartitions=max_files_per_partition
        ).write.partitionBy(*partition_cols).mode("overwrite").format("parquet").save(
            str(output)
        )
    else:
        features_df.write.mode("overwrite").format("parquet").save(str(output))
