import pathlib
from collections.abc import Iterable, Iterator, Sequence

import mwparserfromhell as mwph  # type: ignore[import-untyped]
import pandas as pd
import rbloom
import wikitextparser as wtp  # type: ignore[import-untyped]
from Levenshtein import jaro as levenshtein_score
from mwtokenizer.config.symbols import (  # type: ignore[import-untyped]
    ALL_UNICODE_PUNCTUATION,
)
from pyspark import SparkFiles
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T
from scipy.stats import kurtosis  # type: ignore[import-untyped]

from research_datasets.mwaddlink.generate_anchor_dictionary import (
    ParsedLink,
    compute_hash,
)
from research_datasets.mwaddlink.utils import (
    embeddings_distance,
    get_tokenizer,
    ngram_iterator,
    normalise_anchor,
    normalise_title,
    wtpGetLinkAnchor,
)
from research_transformations.transformation import Transformation


def _filter_links_without_page(pageids_df: DataFrame) -> Transformation:
    def _(links_df: DataFrame) -> DataFrame:
        return (
            links_df.alias("links")
            .join(
                pageids_df.where(F.col("page_id").isNotNull()).alias("pageids"),
                on=(
                    (F.col("links.wiki_db") == F.col("pageids.wiki_db"))
                    & (F.col("links.parsed_link.link") == F.col("pageids.title"))
                ),
                how="inner",
            )
            .select("links.*")
        )

    return _


def _with_resolved_link_redirects(redirects_df: DataFrame) -> Transformation:
    def _(links_df: DataFrame) -> DataFrame:
        return (
            links_df.alias("links")
            .join(
                redirects_df.alias("redirects"),
                on=(
                    (F.col("links.wiki_db") == F.col("redirects.wiki_db"))
                    & (F.col("links.parsed_link.link") == F.col("redirects.title_from"))
                ),
                how="left",
            )
            .select(
                "links.wiki_db",
                "links.page_title",
                "links.sentence",
                F.col("parsed_link.anchor").alias("anchor"),
                F.coalesce(F.col("title_to"), F.col("links.parsed_link.link")).alias(
                    "link"
                ),
            )
        )

    return _


def extract_candidate_anchors(
    wiki_db: str,
    language_code: str,
    sentence: str,
    existing_anchors: list[str],
    anchors: rbloom.Bloom,
) -> list[str]:
    """
    Extract valid anchor candidates
    """
    tokenizer = get_tokenizer(language_code)
    wikitext_nolinks = mwph.parse(sentence).strip_code()
    tested_mentions = set()
    for gram in ngram_iterator(wikitext_nolinks, tokenizer, 10, 1):
        mention = gram.lower()
        # if the mention exist in the DB
        # it was not previously linked (or part of a link)
        # none of its candidate links is already used
        # it was not tested before (for efficiency)
        if (wiki_db, mention) in anchors and mention not in tested_mentions:
            tested_mentions.add(mention)
    # we also add the mentions inp_pairs to make sure we have positive examples
    for mention in existing_anchors:
        if (wiki_db, mention) in anchors:
            tested_mentions.add(mention)
    return [normalise_anchor(mention) for mention in tested_mentions]


def enrich_with_candidate_anchors(
    spark: SparkSession,
    canonical_wikis: DataFrame,
    anchors_file: pathlib.Path,
) -> Transformation:
    spark.sparkContext.addFile(path=str(anchors_file))

    candidate_anchors_schema = T.StructType(
        [
            T.StructField("wiki_db", T.StringType()),
            T.StructField("page_title", T.StringType()),
            T.StructField("anchor", T.StringType()),
        ]
    )

    def candidate_anchors_udf(
        sentence_batches: Iterable[pd.DataFrame],
    ) -> Iterator[pd.DataFrame]:
        # Load the bloom filter containing all known anchors ahead of
        # processing any batches so we only load it once.
        anchors = rbloom.Bloom.load(SparkFiles.get(anchors_file.name), compute_hash)

        for batch in sentence_batches:
            batch["anchor"] = [
                extract_candidate_anchors(
                    wiki_db=wiki_db,
                    language_code=language_code,
                    sentence=sentence,
                    existing_anchors=[pl["anchor"] for pl in anchor_links],
                    anchors=anchors,
                )
                for wiki_db, language_code, sentence, anchor_links in zip(
                    batch["wiki_db"],
                    batch["language_code"],
                    batch["sentence"],
                    batch["anchor_links"],
                )
            ]

            # Explode candidates column into multiple rows.
            df = batch.explode("anchor")
            yield df[["wiki_db", "page_title", "anchor"]]

    def _(anchor_links_df: DataFrame) -> DataFrame:
        return anchor_links_df.join(
            F.broadcast(
                canonical_wikis.select(
                    F.col("database_code").alias("wiki_db"), "language_code"
                )
            ),
            on="wiki_db",
        ).mapInPandas(
            candidate_anchors_udf,  # type: ignore[arg-type]
            schema=candidate_anchors_schema,
        )

    return _


def enrich_with_links(
    pageids_df: DataFrame,
    redirects_df: DataFrame,
) -> Transformation:
    """
    Parse anchor / link pairs
    """

    @F.udf(
        returnType=T.ArrayType(
            T.StructType(
                [
                    T.StructField("anchor", T.StringType()),
                    T.StructField("link", T.StringType()),
                ]
            )
        )
    )
    def extract_anchor_link_pairs(wikitext: str) -> list[ParsedLink]:
        parsed_links = []
        for unparsed_link in wtp.parse(str(wikitext)).wikilinks:
            link, anchor = wtpGetLinkAnchor(unparsed_link)
            parsed_links.append(ParsedLink(anchor, link))
        return parsed_links

    def _(sentences_df: DataFrame) -> DataFrame:
        return (
            sentences_df.withColumn(
                "parsed_link", F.explode(extract_anchor_link_pairs("sentence"))
            )
            .transform(_filter_links_without_page(pageids_df))
            .transform(_with_resolved_link_redirects(redirects_df))
            .groupBy("wiki_db", "page_title")
            .agg(
                F.collect_list(F.struct("anchor", "link")).alias("anchor_links"),
                F.first(F.col("sentence")).alias("sentence"),
            )
        )

    return _


def _with_global_anchor_features(anchors_df: DataFrame) -> DataFrame:
    @F.udf(returnType=T.FloatType())
    def kurtosis_udf(link_occurrence: dict[str, int], ambig: int) -> float:
        # Skew of usage text/link distribution
        return float(
            kurtosis(
                sorted(list(link_occurrence.values()), reverse=True)
                + [1] * (1000 - ambig)
            )
        )

    return anchors_df.withColumn("ambig", F.size("link_occurrence")).withColumn(
        "kurtosis", kurtosis_udf(F.col("link_occurrence"), F.col("ambig"))
    )


def _enrich_with_page_title(pageids_df: DataFrame) -> Transformation:
    """
    Returns a dataframe that contains page title and embeddings vector
    """

    def _(embeddings_df: DataFrame) -> DataFrame:
        return (
            embeddings_df.alias("embeddings")
            .join(
                pageids_df.alias("pageids"),
                on=(
                    (F.col("embeddings.wiki_db") == F.col("pageids.wiki_db"))
                    & (F.col("embeddings.pid_from") == F.col("pageids.page_id"))
                ),
            )
            .select(
                "pageids.wiki_db",
                F.col("pageids.title").alias("page_title"),
                "embeddings.embedding",
            )
        )

    return _


def enrich_with_ml_features(
    anchors_df: DataFrame,
    embeddings_df: DataFrame,
    pageids_df: DataFrame,
    anchor_links_df: DataFrame,
    canonical_wikis: DataFrame,
) -> Transformation:
    @F.udf(returnType=T.IntegerType())
    def extract_label(
        candidate: str, link: str, anchor_links: list[dict[str, str]]
    ) -> int:
        label = next(
            (
                True
                for pl in anchor_links
                if (pl["anchor"] == candidate and pl["link"] == link)
            ),
            False,
        )
        return int(label)

    @F.udf(returnType=T.IntegerType())
    def ngram(anchor: str, language_code: str) -> int:
        tokenizer = get_tokenizer(language_code)
        ngram = list(
            tokenizer.word_tokenize(anchor, use_abbreviation=True)
        )  # tokenize text
        ngram = list(
            filter(lambda x: not x.startswith(" "), ngram)
        )  # could be a single or multiple spaces that needs to be removed
        ngram = list(
            filter(lambda x: x not in ALL_UNICODE_PUNCTUATION, ngram)
        )  # remove punctuation
        return len(ngram)

    def _(anchor_candidates_df: DataFrame) -> DataFrame:
        candidate_anchors_with_features_df = (
            # enrich candidates with global anchor features
            # also ensure that only previously existing anchors are candidates
            anchor_candidates_df.join(
                anchors_df.transform(_with_global_anchor_features),
                on=["wiki_db", "anchor"],
                how="inner",
            )
            .join(
                F.broadcast(
                    canonical_wikis.select(
                        F.col("database_code").alias("wiki_db"), "language_code"
                    )
                ),
                on="wiki_db",
            )
            .withColumn("ngram", ngram("anchor", "language_code"))
            .drop("language_code")
            # join with embedding of page
            .join(
                embeddings_df,
                on=["wiki_db", "page_title"],
            )
            .withColumnRenamed("embedding", "page_embedding")
            # re-enrich the anchor_links array for label extraction
            .join(
                anchor_links_df.select(["wiki_db", "page_title", "anchor_links"]),
                on=["wiki_db", "page_title"],
            )
        )
        return (
            # explode the links for that page and enrich with features
            candidate_anchors_with_features_df.select(
                *candidate_anchors_with_features_df.columns,
                F.explode("link_occurrence").alias("link", "freq"),
            )
            .withColumn(
                "link", F.udf(normalise_title, returnType=T.StringType())("link")
            )
            .withColumn("label", extract_label("anchor", "link", "anchor_links"))
            .withColumn(
                "levenshtein",
                F.udf(levenshtein_score, returnType=T.FloatType())(
                    F.lower("anchor"), F.lower("link")
                ),
            )
            .drop("link_occurrence", "anchor_links")
            # join with embedding of page
            .join(
                embeddings_df.withColumnRenamed("page_title", "link"),
                on=["wiki_db", "link"],
            )
            .withColumnRenamed("embedding", "link_embedding")
            .withColumn(
                "w2v",
                F.udf(embeddings_distance, returnType=T.FloatType())(
                    "page_embedding", "link_embedding"
                ),
            )
            .drop("link_embedding", "page_embedding")
        )

    return _


def run(
    spark: SparkSession,
    snapshot: str,
    wiki_dbs: Sequence[str],
    directory: pathlib.Path,
    files_per_wiki: int = 20,
) -> None:
    """
    Generates training dataset.

    This job extracts examples from the backtesting protocol
    and reduces them to gold triple: (page, mention, link)
    We turn the gold triple into features and generate negatives examples
    Positive example: The correct link
    Negative example: identified mentions and all candidate links
    """
    anchors_file = pathlib.Path.cwd() / "anchor.bloom"
    anchors_df = spark.read.parquet(str(directory / "anchors_filtered"))
    pageids_df = (
        spark.read.parquet(str(directory / "pageids"))
        .withColumnRenamed("pid", "page_id")
        .drop("wikitext")
    )
    redirects_df = spark.read.parquet(str(directory / "redirects"))

    canonical_wikis_df = spark.table("canonical_data.wikis")

    sentences_df = spark.read.parquet(
        str(directory / "training/sentences_train.parquet")
    )

    embeddings_df = (
        spark.table("research.article_topics")
        .where(F.col("snapshot") == snapshot)
        .where(F.col("wiki_db").isin(*wiki_dbs))
        .transform(_enrich_with_page_title(pageids_df))
        .alias("embeddings")
        .cache()
    )

    anchor_links_df = sentences_df.transform(
        enrich_with_links(pageids_df, redirects_df)
    ).cache()
    features_df = anchor_links_df.transform(
        enrich_with_candidate_anchors(spark, canonical_wikis_df, anchors_file)
    ).transform(
        enrich_with_ml_features(
            anchors_df,
            embeddings_df,
            pageids_df,
            anchor_links_df,
            canonical_wikis_df,
        )
    )

    features_df.repartition(files_per_wiki).write.partitionBy("wiki_db").mode(
        "overwrite"
    ).parquet(str(directory / "training/link_train.parquet"))

    # save embeddings for inference
    embeddings_df.repartition(files_per_wiki).write.partitionBy("wiki_db").mode(
        "overwrite"
    ).parquet(str(directory / "embeddings"))
