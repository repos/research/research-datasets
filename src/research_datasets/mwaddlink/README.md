# research/mwaddlink

This is the repository that backs the [Wikimedia Link Recommendation service](https://wikitech.wikimedia.org/wiki/Add_Link).

It contains code for training a model and generating datasets.

The method is context-free and can be scaled to (virtually) any language, provided that we have enough existing links to learn from.

## Setting up the virtual environment

Follow the instructions in the repository [ReadMe.md](../../../README.md) to set up the conda environement.

## Training the model

The full pipeline to train the model and generate the underlying datasets for a Wikipedia is run as a dag in airflow. Follwing is a description of all the scripts used to collect data and train a language agnostic link recommendation model.

#### generate_anchor_dictionary_spark.py
Spark-job that generates the anchor dictionary (\*.anchors.pkl) and helper-dictionaries for lookup (\*.pageids.pkl, \*.redirects.pkl) from dumps. This generates the following files:
- ```<HDFS_PATH>/<WIKI_ID>/<WIKI_ID>.anchors.pkl``` Format: {mention: [candidate_link_title:number_of_occurrences,] }
- ```<HDFS_PATH>/<WIKI_ID>/<WIKI_ID>.pageids.pkl``` Format: {page_id:page_title}
- ```<HDFS_PATH>/<WIKI_ID>/<WIKI_ID>.redirects.pkl``` Format: {redirect_from_title:redirect_to_title}

#### generate_wdproperties_spark.py
Spark-job that generates the wikidata-property dictionary (\*.wdproperties.pkl). For each pageid, it stores the Wikidata-items listed as values for a pre-defined set of properties (e.g. P31). This generates the following files:
- ```<HDFS_PATH>/<WIKI_ID>/<WIKI_ID>.wdproperties.pkl``` Format: {page_id:[wikidata_item,]}

#### filter_dict_anchor.py
Filters all pages from the anchor-dictionary that have a Wikidata-property from a pre-defined set (e.g. instance_of=date). The filter is defined manually at the beginning of the script. This generates the following files:
- ```<HDFS_PATH>/<WIKI_ID>/<WIKI_ID>.anchors.pkl``` Note:this file already exists before and is only filtered so that some items are removed

#### generate_backtesting_data.py
Spark job to extract a pre-defined number of sentences containing links (from each article only the first sentence that contains at least one link ). The sentences are split into training and testing. This generates the following files:
- ```<HDFS_PATH>/<WIKI_ID>/training/sentences_train.csv``` Format: page_title \t sentence_wikitext \n
- ```<HDFS_PATH>/<WIKI_ID>/testing/sentences_test.csv``` Format: page_title \t sentence_wikitext \n

#### generate_training_data.py
Parse the training sentences and transform into a training set of positive and negative examples of links with features. This generates the following files:
- ```<HDFS_PATH>/<WIKI_ID>/training/link_train.csv``` Format: page_title \t mention_text \t link_title \t feature_1 \t … \t feature_n \t label

#### generate_addlink_model.py
Train a classifier-model using [XGBoost](https://xgboost.readthedocs.io) to predict links based on features. The script can combined any number of languge wikis to create a language agnostic model. This generates the following files:
- ```<HDFS_PATH>/<MODEL_ID>/<MODEL_ID>.linkmodel.joblib``` contains parameters of the model, can be loaded via XGBoost.

#### generate_backtesting_eval.py
Run backtesting evaluation of the link recommendation model on the test sentences. Output is precision and recall metrics for several values of the link-threshold. This generates the following files:
- ```<HDFS_PATH>/<MODEL_ID>/<WIKI_ID>.backtest.eval.csv``` Format:  index, threshold, number_of_sentences, precision, recall \n
- The numbers of precision and recall should not be too low. One can compare with the numbers reported in previous experiments on 10+ deployed wikis ([meta](https://meta.wikimedia.org/wiki/Research:Link_recommendation_model_for_add-a-link_structured_task#Third_set_of_results_(2021-06))). For the default threshold 0.5, the precision should be around 75% (or more) and the recall should not drop below 20% so there are still enough links to generate.
