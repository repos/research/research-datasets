import pathlib
from collections.abc import Sequence

from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F

from research_transformations.transformation import Transformation


def filter_main_namespace_page_links(wiki_dbs: Sequence[str]) -> Transformation:
    def _(wikidata_item_page_link_df: DataFrame) -> DataFrame:
        return wikidata_item_page_link_df.where(F.col("wiki_db").isin(*wiki_dbs)).where(
            F.col("page_namespace") == 0
        )

    return _


def _enrich_with_parsed_wikidata_value(
    wikidata_properties: Sequence[str],
) -> Transformation:
    def _(claims_with_page_df: DataFrame) -> DataFrame:
        parsed_value_df = (
            claims_with_page_df.where(
                F.col("claim.mainSnak.dataType") == "wikibase-item"
            )
            .withColumn("statement_property", F.col("claim.mainSnak.property"))
            .where(F.col("statement_property").isin(*wikidata_properties))
            .withColumn(
                "parsed_value",
                F.from_json(F.col("claim.mainSnak.dataValue.value"), "id string"),
            )
            .where(F.col("parsed_value").isNotNull())
            .select(
                "wiki_db",
                "page_id",
                "item_id",
                "statement_property",
                F.col("parsed_value.id").alias("statement_value"),
            )
        )
        return parsed_value_df.groupby("wiki_db", "page_id").agg(
            F.collect_set(F.col("statement_value")).alias("statement_value_qid")
        )

    return _


def enrich_with_wikidata_statements(
    wikidata_item_page_link_df: DataFrame, wikidata_properties: Sequence[str]
) -> Transformation:
    def _(wikidata_entity_df: DataFrame) -> DataFrame:
        claims_with_page_df = (
            wikidata_entity_df.withColumnRenamed("id", "item_id")
            .where(F.col("claims").isNotNull())
            .join(
                wikidata_item_page_link_df.select("item_id", "wiki_db", "page_id"),
                on="item_id",
                how="inner",
            )
            .select(
                "wiki_db",
                "page_id",
                "item_id",
                F.explode(F.col("claims")).alias("claim"),
            )
        )
        return claims_with_page_df.transform(
            _enrich_with_parsed_wikidata_value(wikidata_properties)
        )

    return _


def run(
    spark: SparkSession,
    wikidata_snapshot: str,
    wiki_dbs: Sequence[str],
    wikidata_properties: Sequence[str],
    directory: pathlib.Path,
) -> None:
    """
    Generates dictionary of Wikidata properties with which to filter achor dictionary.
    """

    wikidata_item_page_link_df = (
        spark.read.table("wmf.wikidata_item_page_link")
        .where(F.col("snapshot") == wikidata_snapshot)
        .transform(filter_main_namespace_page_links(wiki_dbs))
    )

    wikidata_entity_df = spark.read.table("wmf.wikidata_entity").where(
        F.col("snapshot") == wikidata_snapshot
    )

    (
        wikidata_entity_df.transform(
            enrich_with_wikidata_statements(
                wikidata_item_page_link_df, wikidata_properties
            )
        )
        .write.mode("overwrite")
        .parquet(str(directory / "wdproperties"))
    )
