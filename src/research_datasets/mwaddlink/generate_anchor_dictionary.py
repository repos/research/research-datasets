import collections
import functools
import hashlib
import logging
import pathlib
import pickle
import re
from collections.abc import Iterable, Iterator, Sequence
from dataclasses import dataclass
from typing import Any

import mwparserfromhell  # type: ignore[import-untyped]
import pandas as pd
import rbloom
from mwtokenizer import Tokenizer  # type: ignore[import-untyped]
from pyspark import SparkFiles
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T

from research_datasets.mwaddlink import ngram_utils, utils
from research_transformations.transformation import Transformation

MIN_LINK_PROBABILITY = 0.065
REFERENCE_RE = re.compile(r"<ref[^>]*>[^<]+<\/ref>")
LINK_RE = re.compile(
    r"\[\[(?P<link>[^\n\|\]\[\<\>\{\}]{0,256})(?:\|(?P<anchor>[^\[]*?))?\]\]"
)


@dataclass(frozen=True)
class ParsedLink:
    anchor: str
    link: str


@functools.cache
def get_tokenizer(wiki_db: str) -> Tokenizer:
    wiki_url = utils.get_wiki_url(wiki_db)
    language_code = utils.get_language_code(wiki_url)
    return Tokenizer(language_code)


@F.udf
def normalized_title(title: str) -> str:
    return utils.normalise_title(title)


def _with_normalized_page_titles(mediawiki_wikitext_current_df: DataFrame) -> DataFrame:
    return mediawiki_wikitext_current_df.withColumn(
        colName="normalized_page_title",
        col=normalized_title("page_title"),
    ).withColumn(
        colName="normalized_page_redirect_title",
        col=normalized_title("page_redirect_title"),
    )


def _filter_empty_articles(mediawiki_wikitext_current_df: DataFrame) -> DataFrame:
    return mediawiki_wikitext_current_df.filter(
        (F.col("revision_text").isNotNull()) & (F.length(F.col("revision_text")) > 0)
    )


@F.udf(
    returnType=T.ArrayType(
        T.StructType(
            [
                T.StructField(name="anchor", dataType=T.StringType()),
                T.StructField(name="link", dataType=T.StringType()),
            ]
        )
    )
)
def links(wikitext: str) -> list[ParsedLink]:
    return [
        ParsedLink(
            anchor=utils.normalise_anchor(anchor or normalized_link),
            link=normalized_link,
        )
        for link, anchor in LINK_RE.findall(wikitext)
        if (normalized_link := utils.normalise_title(link))
    ]


def _with_resolved_links(redirects_df: DataFrame) -> Transformation:
    def _(links_df: DataFrame) -> DataFrame:
        resolved_redirects_df = links_df.alias("links").join(
            redirects_df.alias("redirects"),
            how="left",
            on=(
                (F.col("links.wiki_db") == F.col("redirects.wiki_db"))
                & (F.col("links.link") == F.col("redirects.title_from"))
            ),
        )
        return resolved_redirects_df.select(
            "links.*",
            F.coalesce(F.col("redirects.title_to"), F.col("link")).alias(
                "resolved_link"
            ),
        )

    return _


def _filter_unlinkable_anchors(articles_df: DataFrame) -> Transformation:
    def _(links_df: DataFrame) -> DataFrame:
        return links_df.alias("links").join(
            articles_df.alias("articles"),
            how="leftsemi",
            on=(
                (F.col("links.wiki_db") == F.col("articles.wiki_db"))
                & (F.col("links.link") == F.col("articles.title"))
            ),
        )

    return _


def _enrich_with_anchor_linked_frequency(links_df: DataFrame) -> DataFrame:
    anchor_is_non_numeric = (
        "anchor not rlike '^[0-9]+$' and anchor not rlike '^[0-9]+[/-][0-9]+$'"
    )
    linked_frequency_df = (
        links_df.where(F.length("anchor") > 0)
        .where(anchor_is_non_numeric)
        .groupBy("wiki_db", "anchor")
        .agg(F.count("*").alias("linked_frequency"))
        .where(F.col("linked_frequency") > 1)
    )
    return linked_frequency_df


def compute_hash(obj: Any) -> int:
    obj_hash = hashlib.sha256(pickle.dumps(obj), usedforsecurity=False).digest()
    return int.from_bytes(obj_hash[:16], "big") - 2**127


def make_bloom_filter(
    max_items: int, items: Iterable[Any], output_path: pathlib.Path
) -> None:
    bf = rbloom.Bloom(
        expected_items=max_items,
        false_positive_rate=0.01,
        hash_func=compute_hash,
    )
    bf.update(items)
    bf.save(output_path)


def tokenize(tokenizer: Tokenizer, text: str) -> list[str]:
    return [
        stripped_token
        for token in tokenizer.sentence_tokenize(text, use_abbreviation=True)
        if (stripped_token := token.strip())
    ]


@F.udf
def stripped_wikitext(wikitext: str) -> str:
    stripped_wikitext = LINK_RE.sub(".", wikitext)
    stripped_wikitext = REFERENCE_RE.sub(".", stripped_wikitext)

    try:
        # Try to strip additional markup such as templates via mwparserfromhell.
        stripped_wikitext = mwparserfromhell.parse(stripped_wikitext).strip_code()
    except Exception as err:
        logging.exception(err)

    return stripped_wikitext.lower()


def count_occurring_anchors(
    text: str, wiki_db: str, anchors: rbloom.Bloom
) -> list[tuple[str, int]]:
    tokenizer = get_tokenizer(wiki_db)
    sentences = tokenize(tokenizer, text)
    extracted_anchors: list[str] = []
    for sentence in sentences:
        for anchor_ngram_count in range(10, 0, -1):
            tokens = ngram_utils.get_tokens(sentence, tokenizer)
            for ngram in ngram_utils.get_ngrams(tokens, anchor_ngram_count):
                if (wiki_db, ngram) in anchors:
                    # NOTE: We are supposed to also replace found ngram in sentence
                    # so we don't catch overlapping anchors, but this implementation
                    # doesn't do that. These comments explain why:
                    # https://gitlab.wikimedia.org/repos/research/research-datasets/-/merge_requests/31#note_92027
                    extracted_anchors.append(ngram)

    anchor_counts = collections.Counter(extracted_anchors).most_common()
    return anchor_counts


def _enrich_with_anchor_frequency(
    spark: SparkSession, anchors_file: pathlib.Path
) -> Transformation:
    """Calculate frequency of all known anchors across articles texts."""
    spark.sparkContext.addFile(path=str(anchors_file))

    anchor_frequency_schema = T.StructType(
        [
            T.StructField(name="wiki_db", dataType=T.StringType()),
            T.StructField(name="pid", dataType=T.IntegerType()),
            T.StructField(name="anchor", dataType=T.StringType()),
            T.StructField(name="anchor_frequency", dataType=T.IntegerType()),
        ]
    )

    def anchor_frequency_udf(
        articles_batches: Iterable[pd.DataFrame],
    ) -> Iterator[pd.DataFrame]:
        # Load the bloom filter containing all known anchors ahead of
        # processing any batches so we only load it once.
        anchors = rbloom.Bloom.load(SparkFiles.get(anchors_file.name), compute_hash)

        for batch in articles_batches:
            batch["article_anchor_frequency"] = [
                count_occurring_anchors(text=wikitext, wiki_db=wiki_db, anchors=anchors)
                for wiki_db, wikitext in zip(batch["wiki_db"], batch["wikitext"])
            ]

            # Explode article_anchor_frequency column into multiple rows.
            df = batch.explode("article_anchor_frequency")
            df = df.dropna(subset=("article_anchor_frequency",))

            # Pop the exploded column and create a new dataframe from rows of tuples.
            anchor_frequency_df = pd.DataFrame(
                df.pop("article_anchor_frequency").tolist(),
                columns=["anchor", "anchor_frequency"],
                index=df.index,
            )
            # Concatenate the original pid column to the new dataframe to
            # get (pid, anchor, anchor_frequency) rows.
            yield pd.concat([df[["wiki_db", "pid"]], anchor_frequency_df], axis=1)

    def _(articles_df: DataFrame) -> DataFrame:
        anchor_frequency_df = (
            articles_df.select(
                "pid", "wiki_db", stripped_wikitext("wikitext").alias("wikitext")
            )
            .mapInPandas(
                anchor_frequency_udf,  # type: ignore[arg-type]
                schema=anchor_frequency_schema,
            )
            .groupBy("wiki_db", "anchor")
            .agg(F.sum("anchor_frequency").alias("anchor_frequency"))
        )
        return anchor_frequency_df

    return _


def _with_link_probability(
    linked_frequency_df: DataFrame, anchor_frequency_df: DataFrame
) -> Transformation:
    def _(links_df: DataFrame) -> DataFrame:
        link_probability_df = linked_frequency_df.join(
            anchor_frequency_df,
            on=["wiki_db", "anchor"],
            how="left",
        )
        link_probability_df = link_probability_df.fillna(
            {"anchor_frequency": 0}
        ).withColumn(
            colName="link_probability",
            col=F.col("linked_frequency")
            / (F.col("anchor_frequency") + F.col("linked_frequency")),
        )

        return (
            links_df.alias("links")
            .join(link_probability_df, on=["wiki_db", "anchor"], how="left")
            .select("links.*", "link_probability")
        )

    return _


def _enrich_with_link_probability(
    spark: SparkSession, articles_df: DataFrame
) -> Transformation:
    def _(links_df: DataFrame) -> DataFrame:
        linked_frequency_df = links_df.transform(_enrich_with_anchor_linked_frequency)

        # Bring all unique anchors back to the driver, *one partition at a time*, and
        # add them to a bloom filter which can be later used to test if an arbitrary
        # word is a known anchor. Bloom filters have a much smaller footprint than sets
        # (~50 MiB vs ~3 GiB for 50M anchors) and a small number of false positives are
        # not a problem since our usecase involves joining with the anchors dataframe
        # afterwards, anyway.
        anchors = (
            (row.wiki_db, row.anchor)
            for row in linked_frequency_df.select("wiki_db", "anchor").toLocalIterator()
        )
        anchors_path = pathlib.Path.cwd() / "anchor.bloom"
        make_bloom_filter(
            max_items=50_000_000,
            items=anchors,
            output_path=anchors_path,
        )

        anchor_frequency_df = articles_df.transform(
            _enrich_with_anchor_frequency(spark=spark, anchors_file=anchors_path)
        )

        link_probability_df = links_df.distinct().transform(
            _with_link_probability(
                linked_frequency_df=linked_frequency_df,
                anchor_frequency_df=anchor_frequency_df,
            )
        )
        return link_probability_df

    return _


def _aggregate_link_occurrence_by_anchor(links_df: DataFrame) -> DataFrame:
    return (
        links_df.groupBy("wiki_db", "anchor", "link")
        .agg(F.count(F.col("title")).alias("n"))
        .groupBy("wiki_db", "anchor")
        .agg(
            F.map_from_entries(F.collect_list(F.struct("link", "n"))).alias(
                "link_occurrence"
            )
        )
    )


def enrich_with_articles(wiki_dbs: Sequence[str]) -> Transformation:
    def _(mediawiki_wikitext_current_df: DataFrame) -> DataFrame:
        filtered_wikitext_df = (
            mediawiki_wikitext_current_df.where(F.col("wiki_db").isin(*wiki_dbs))
            .where(F.col("page_namespace") == 0)
            .transform(_filter_empty_articles)
            .transform(_with_normalized_page_titles)
        )
        articles_df = filtered_wikitext_df.where(
            F.col("page_redirect_title") == ""
        ).select(
            F.col("wiki_db"),
            F.col("page_id").alias("pid"),
            F.col("normalized_page_title").alias("title"),
            F.col("revision_text").alias("wikitext"),
        )
        return articles_df

    return _


def enrich_with_redirects(wiki_dbs: Sequence[str]) -> Transformation:
    def _(mediawiki_wikitext_current_df: DataFrame) -> DataFrame:
        filtered_wikitext_df = (
            mediawiki_wikitext_current_df.where(F.col("wiki_db").isin(*wiki_dbs))
            .where(F.col("page_namespace") == 0)
            .transform(_filter_empty_articles)
            .transform(_with_normalized_page_titles)
        )
        redirects_df = (
            filtered_wikitext_df.where(F.col("page_redirect_title") != "")
            .select(
                F.col("wiki_db"),
                F.col("normalized_page_title").alias("title_from"),
                F.col("normalized_page_redirect_title").alias("title_to"),
            )
            .distinct()
        )
        return redirects_df

    return _


def enrich_with_anchors(spark: SparkSession, wiki_dbs: Sequence[str]) -> Transformation:
    def _(mediawiki_wikitext_current_df: DataFrame) -> DataFrame:
        filtered_wikitext_df = (
            mediawiki_wikitext_current_df.where(F.col("wiki_db").isin(*wiki_dbs))
            .where(F.col("page_namespace") == 0)
            .transform(_filter_empty_articles)
            .transform(_with_normalized_page_titles)
        )
        articles_df = filtered_wikitext_df.where(
            F.col("page_redirect_title") == ""
        ).select(
            F.col("wiki_db"),
            F.col("page_id").alias("pid"),
            F.col("normalized_page_title").alias("title"),
            F.col("revision_text").alias("wikitext"),
        )
        redirects_df = (
            filtered_wikitext_df.where(F.col("page_redirect_title") != "")
            .select(
                F.col("wiki_db"),
                F.col("normalized_page_title").alias("title_from"),
                F.col("normalized_page_redirect_title").alias("title_to"),
            )
            .distinct()
        )

        anchors_df = (
            articles_df.withColumn("links", F.explode(links("wikitext")))
            .select("wiki_db", "pid", "title", "links.link", "links.anchor")
            .transform(_with_resolved_links(redirects_df))
            .drop("link")
            .withColumnRenamed("resolved_link", "link")
            .transform(_filter_unlinkable_anchors(articles_df))
            .transform(
                _enrich_with_link_probability(spark=spark, articles_df=articles_df)
            )
            .where(F.col("link_probability") >= MIN_LINK_PROBABILITY)
            .transform(_aggregate_link_occurrence_by_anchor)
        )

        return anchors_df

    return _


def run(
    spark: SparkSession, snapshot: str, wiki_dbs: Sequence[str], output: pathlib.Path
) -> None:
    mediawiki_wikitext_current = spark.table("wmf.mediawiki_wikitext_current").where(
        F.col("snapshot") == snapshot
    )

    (
        mediawiki_wikitext_current.transform(
            enrich_with_articles(wiki_dbs),
        )
        .write.mode("overwrite")
        .parquet(str(output / "pageids"))
    )

    (
        mediawiki_wikitext_current.transform(
            enrich_with_redirects(wiki_dbs),
        )
        .write.mode("overwrite")
        .parquet(str(output / "redirects"))
    )

    (
        mediawiki_wikitext_current.transform(
            enrich_with_anchors(spark=spark, wiki_dbs=wiki_dbs),
        )
        .write.mode("overwrite")
        .parquet(str(output / "anchors"))
    )
