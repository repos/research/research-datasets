import multiprocessing
import pathlib
from collections.abc import Sequence

import fsspec  # type: ignore[import-untyped]
import joblib  # type: ignore[import-untyped]
import pandas as pd
import xgboost
from sklearn import model_selection  # type: ignore[import-untyped]
from sklearn.metrics import (  # type: ignore[import-untyped]
    average_precision_score,
    f1_score,
    roc_auc_score,
)
from sklearn.model_selection import (  # type: ignore[import-untyped]
    GridSearchCV,
    StratifiedShuffleSplit,
)
from sklearn.preprocessing import OrdinalEncoder  # type: ignore[import-untyped]

from research_datasets.mwaddlink.utils import hdfs_path

seed = 7
max_train_samples = 100000  # for combined model
max_train_samples_grid = 50000  # for grid_search


def get_training_data(
    df: pd.DataFrame,
) -> tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame, OrdinalEncoder]:
    # use OrdinalEncoder to encode wiki_db as categorical feature for the model
    wiki_db_encoder = OrdinalEncoder(
        handle_unknown="use_encoded_value", unknown_value=-1
    )
    df["wiki_db"] = wiki_db_encoder.fit_transform(
        df["wiki_db"].to_numpy().reshape(-1, 1)
    )  # encode wiki_db

    # split data into X and y (features and labels)
    features = ["ngram", "freq", "ambig", "kurtosis", "w2v", "levenshtein", "wiki_db"]
    X = df[features].values
    Y = df["label"].values.astype(int)

    stratify_data = [f"{x}_{y}" for x, y in zip(df["wiki_db"].values, Y)]

    # Generate a random Train/Test split
    # note: the dataset is large enough to avoid Cross Validation
    test_size = 0.33
    split_dfs = model_selection.train_test_split(
        X, Y, test_size=test_size, random_state=seed, stratify=stratify_data
    )
    X_train, X_test, y_train, y_test = split_dfs
    return X_train, X_test, y_train, y_test, wiki_db_encoder


def perform_grid_search(df_small: pd.DataFrame, n_cpus_max: int) -> GridSearchCV:
    X_train, X_test, y_train, y_test, _ = get_training_data(df_small)

    # Define the hyperparameter grid
    # default eta: 0.3, max_depth 6, subsample 1, estimators 100
    param_grid = {
        "max_depth": [
            6,
            8,
            10,
            12,
        ],  # depth leads to overfitting # if overfits, tune gamma, lambda, alpha
        "learning_rate": [0.7, 0.5, 0.3, 0.1, 0.01, 0.001],
        # 'subsample': [0.5, 0.7, 1], # lowering will prevent overfitting by training on
        # a subset of data
        "n_estimators": [40, 70, 100, 130, 160, 190, 250],
    }

    # Create the XGBoost model object
    xgb_model = xgboost.XGBClassifier(n_jobs=n_cpus_max, tree_method="approx")

    # get CV splits (1 split is enough for this large dataset)
    cv = StratifiedShuffleSplit(n_splits=1, random_state=seed, test_size=0.3)

    # Create the GridSearchCV object
    grid_search_model = GridSearchCV(
        xgb_model,
        param_grid,
        cv=cv,
        scoring=["roc_auc", "f1", "average_precision"],
        refit="f1",
        n_jobs=n_cpus_max,
        verbose=3,
    )
    grid_search_model.fit(X_train, y_train)

    # Print the best set of hyperparameters and the corresponding score
    print("Best set of hyperparameters: ", grid_search_model.best_params_)
    print("Best score: ", grid_search_model.best_score_)

    return grid_search_model


def run(  # noqa: PLR0915
    wiki_dbs: Sequence[str],
    model_id: str,
    directory: pathlib.Path,
    grid_search: bool = False,
) -> None:
    """
    Trains and saves a language agnostic XGBoost model on one or more languages.

    :param wikis: List of Wiki IDs with which to train the model (e.g. enwiki)
    :param model_id: Model name. Can be the Wiki ID for single language wiki
    :param directory: Path to input/output directory
    :param grid_search: Whether to perform grid search and store best model
    """
    print("Number of Wikis:", len(wiki_dbs))

    df = pd.DataFrame()

    if grid_search:
        df_small = pd.DataFrame()

    for wiki in wiki_dbs:
        try:
            path = directory / "training" / "link_train.parquet" / f"wiki_db={wiki}"
            wiki_df = pd.read_parquet(hdfs_path(path))
            wiki_df["wiki_db"] = wiki
        except Exception as e:
            print("ERROR: ", type(e).__name__, e)  # if file not found, skip wiki
            continue

        print(wiki, len(wiki_df))

        if len(wiki_dbs) > 1:
            # when a list of wikis is provided, training data from each language is
            # downsampled to at most max_train_samples
            wiki_df, _ = model_selection.train_test_split(
                wiki_df,
                train_size=min(len(wiki_df) - 2, max_train_samples),
                random_state=seed,
                stratify=wiki_df["label"],
            )
            print("Reduced", wiki, len(wiki_df))
            df = pd.concat([df, wiki_df])
        else:
            # for single-language models we dont downsample the training data
            df = pd.concat([df, wiki_df])

        if grid_search:
            wiki_df_small, _ = model_selection.train_test_split(
                wiki_df,
                train_size=min(len(wiki_df) - 2, max_train_samples_grid),
                random_state=seed,
                stratify=wiki_df["label"],
            )
            print("Reduced (grid search)", wiki, len(wiki_df_small))
            df_small = pd.concat([df_small, wiki_df_small])

    print("Total training data size:", len(df))
    if grid_search:
        print("Total training data size reduced:", len(df_small))

    # Fit model to the training data
    n_cpus_max = min([int(multiprocessing.cpu_count() / 4), 8])

    if grid_search:
        grid_search_model = perform_grid_search(df_small, n_cpus_max)
        model = xgboost.XGBClassifier(
            **grid_search_model.best_params_, n_jobs=n_cpus_max
        )
    else:
        model = xgboost.XGBClassifier(
            n_estimators=250, max_depth=8, learning_rate=0.1, n_jobs=n_cpus_max
        )

    X_train, X_test, y_train, y_test, wiki_db_encoder = get_training_data(df)

    model.fit(X_train, y_train)
    print(model)

    # evaluate predictions
    predictions = model.predict_proba(X_test)[:, 1]
    try:
        print(f"ROC AUC={roc_auc_score(y_test, predictions):.3f}")

        preds = model.predict(X_test)
        print(f"F1 SCORE={f1_score(y_test, preds):.3f}")
        avg_prec = average_precision_score(y_test, predictions)
        print(f"Average Precision Score={avg_prec:.3f}")

    except ValueError as e:
        if (
            str(e) == "Only one class present in y_true. "
            "ROC AUC score is not defined in that case."
        ):
            print("ROC AUC not defined")
        else:
            raise

    # save the model
    link_model_path = directory / model_id / f"{model_id}.linkmodel.joblib"
    with fsspec.open(hdfs_path(link_model_path), "wb") as file:
        joblib.dump(model, file)

    encoder_path = directory / model_id / f"{model_id}.encoder.joblib"
    with fsspec.open(hdfs_path(encoder_path), "wb") as file:
        joblib.dump(wiki_db_encoder, file)
