import pathlib

from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T

from research_transformations.transformation import Transformation

list_qid_filter = {
    # non-content pages
    "Q4167410",  # disambiguation-pages
    "Q13406463",  # list-pages
    # dates
    "Q3186692",  # calendar year-pages
    "Q577",  # year
    "Q573",  # day
    "Q39911",  # decade
    "Q578",  # century
    "Q36507",  # millenium
    "Q47150325",  # calendar day of a given year
    "Q47018478",  # calendar month of a given year
    "Q205892",  # calendar date
    "Q14795564",  # Point in time with respect to recurrent timeframe
    "Q18340514",  # events in a specific year or time period
    # units
    "Q1978718",  # unit of length
    "Q1790144",  # unit of time
    "Q1302471",  # unit of volume
    "Q3647172",  # unit of mass
    "Q2916980",  # unit of energy
    "Q3550873",  # unit of information
    "Q8142",  # currency
    "Q27084",  # parts-per-notation
    # names
    "Q202444",  # given name
    "Q1243157",  # double name
    "Q3409032",  # unisex given name
    "Q12308941",  # male given name
    "Q11879590",  # female given name
}


def filter_duplicate_page_ids(pageid_df: DataFrame) -> DataFrame:
    # it is possible to have multiple page ids with the same title,
    # this affects only a small number of pages, and are usually fixed quickly
    # in the live DB but happened during the database export.
    return (
        pageid_df.groupBy("wiki_db", "title")
        .agg(F.count("*").alias("count"), F.first("page_id").alias("page_id"))
        .where(F.col("count") == 1)
        .drop("count")
    )


def filter_links_associated_with_unwanted_qids(
    pageid_df: DataFrame, wdproperties_df: DataFrame, unwanted_qids: set[str]
) -> Transformation:
    @F.udf(returnType=T.BooleanType())
    def link_associated_with_unwanted_qid(link_qids: list[str]) -> bool:
        # returns True if any unwanted qid is associated with a link
        # returns False if there are no qids are associated with a link
        return bool(unwanted_qids.intersection(link_qids)) if link_qids else False

    valid_links = wdproperties_df.where(
        ~link_associated_with_unwanted_qid("statement_value_qid")
    ).join(pageid_df, on=["wiki_db", "page_id"], how="inner")

    def _(anchors_df: DataFrame) -> DataFrame:
        # join to enrich anchor links with page id and qids of links
        anchors_with_page_and_wikidata_df = anchors_df.select(
            "wiki_db",
            "anchor",
            F.explode("link_occurrence").alias("title", "count"),
        ).join(
            valid_links,
            on=["wiki_db", "title"],
            how="inner",
        )
        # filter undesired links and reconstruct the re-aggregate to anchors map
        return (
            anchors_with_page_and_wikidata_df.groupBy("wiki_db", "anchor")
            .agg(
                F.map_from_entries(F.collect_list(F.struct("title", "count"))).alias(
                    "link_occurrence"
                )
            )
            .where(F.size("link_occurrence") > 0)
        )

    return _


def run(
    spark: SparkSession,
    directory: pathlib.Path,
) -> None:
    """
    Filters anchor dictionary that are an instance-of the `list_qid_filter`
    Wikidata items
    """
    anchors_df = spark.read.parquet(str(directory / "anchors"))
    pageid_df = (
        spark.read.parquet(str(directory / "pageids"))
        .withColumnRenamed("pid", "page_id")
        .drop("wikitext")
        .transform(filter_duplicate_page_ids)
    )
    wdproperties_df = spark.read.parquet(str(directory / "wdproperties"))

    anchors_df.transform(
        filter_links_associated_with_unwanted_qids(
            pageid_df, wdproperties_df, list_qid_filter
        )
    ).write.mode("overwrite").parquet(str(directory / "anchors_filtered"))
