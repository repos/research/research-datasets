import pathlib
import re
from collections.abc import Sequence

import mwparserfromhell as mwph  # type: ignore[import-untyped]
from mwparserfromhell.nodes import Text, Wikilink  # type: ignore[import-untyped]
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T

from research_datasets.mwaddlink import utils
from research_datasets.mwaddlink.ngram_utils import tokenize_sentence
from research_datasets.mwaddlink.wikitext_to_plaintext import (
    wikitext_to_plaintext,
)
from research_transformations.transformation import Transformation, stratified_sample

# Remove links to special content
regexes = [
    r"\|.*",
    r"\s+",
    "!.*",
    r".*JPG\|",
    r".*jpg\|",
    r".*\}\}",
    r".*\{\{",
    ".*<ref",
    ".*png",
]
combined = "(" + ")|(".join(regexes) + ")"

NUM_NODES = 2
NUM_LINKS = 0


def filter_qualifying_wikitext(wiki_dbs: Sequence[str]) -> Transformation:
    def _(mediawiki_wikitext_current_df: DataFrame) -> DataFrame:
        return (
            mediawiki_wikitext_current_df.where(F.col("wiki_db").isin(*wiki_dbs))
            ## main namespace
            .where(F.col("page_namespace") == 0)
            # no redirect-pages
            .where(F.col("page_redirect_title") == "")
            .where(F.col("revision_text").isNotNull())
            .where(F.length(F.col("revision_text")) > 0)
            .where(~F.col("page_title").startswith("Wikipedia:"))
            .select("wiki_db", "page_title", "revision_text")
        )

    return _


@F.udf(returnType=T.StringType())
def extract_linked_sentences(wikitext: str, language_code: str) -> str | None:
    """
    Returns the first sentence which originally contained a link, but with the
    link removed
    """
    plaintext = wikitext_to_plaintext(wikitext, language_code, "skip-lists")
    if not isinstance(plaintext, str):
        return None
    tokenizer = utils.get_tokenizer(language_code)
    # extract sentences and strip whitespace
    sents = [sent.strip() for sent in tokenize_sentence(plaintext, tokenizer)]
    # filters
    sents = [sent for sent in sents if not re.match(combined, sent)]
    for j in sents:
        jmwp = mwph.parse(j)
        check = True
        for i in jmwp.nodes:
            if not (isinstance(i, Text | Wikilink)):
                check = False
        # preserve sentences that have at least a link
        if (
            check
            and len(jmwp.nodes) > NUM_NODES
            and len(jmwp.filter_wikilinks()) > NUM_LINKS
        ):
            # j is the sentence
            return j

    return None


def enrich_with_link_sentences(
    canonical_wikis: DataFrame, max_sentences_per_wiki: int
) -> Transformation:
    def _(mediawiki_wikitext_current_df: DataFrame) -> DataFrame:
        return (
            mediawiki_wikitext_current_df.join(
                F.broadcast(
                    canonical_wikis.select(
                        F.col("database_code").alias("wiki_db"), "language_code"
                    )
                ),
                on="wiki_db",
            )
            .withColumn(
                "sentence", extract_linked_sentences("revision_text", "language_code")
            )
            .where(F.col("sentence").isNotNull())
            .drop("revision_text", "language_code")
            .transform(
                stratified_sample(
                    cols=["wiki_db"],
                    strata_size=max_sentences_per_wiki,
                    seed=42,
                )
            )
        )

    return _


def run(
    spark: SparkSession,
    snapshot: str,
    wiki_dbs: Sequence[str],
    directory: pathlib.Path,
    max_sentences_per_wiki: int = 200000,
) -> None:
    """
    Generate a back testing dataset.
    """

    linkless_sentences_df = (
        spark.read.table("wmf.mediawiki_wikitext_current")
        .where(F.col("snapshot") == snapshot)
        .transform(filter_qualifying_wikitext(wiki_dbs=wiki_dbs))
        .transform(
            enrich_with_link_sentences(
                spark.table("canonical_data.wikis"), max_sentences_per_wiki
            )
        )
    )

    # store two files
    # training: to be used by the generate_training_data.py, because in the future
    # we might want to use the whole sentence.
    # test: this is effectively the data used by the backtesting protocol.
    wiki_links_train, wiki_links_test = linkless_sentences_df.randomSplit(
        [0.5, 0.5], seed=42
    )

    wiki_links_train.write.mode("overwrite").parquet(
        str(directory / "training/sentences_train.parquet")
    )
    wiki_links_test.write.mode("overwrite").parquet(
        str(directory / "testing/sentences_test.parquet")
    )
