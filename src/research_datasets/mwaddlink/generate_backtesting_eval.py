import multiprocessing
import pathlib
from collections.abc import Sequence
from math import nan
from typing import Any, cast

import fsspec  # type: ignore[import-untyped]
import joblib  # type: ignore[import-untyped]
import mwparserfromhell as mwph  # type: ignore[import-untyped]
import pandas as pd
from mwtokenizer import Tokenizer  # type: ignore[import-untyped]
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F

from research_datasets.mwaddlink.utils import (
    get_language_code,
    get_wiki_url,
    getLinks,
    hdfs_path,
    process_page,
)


def to_dict(df: DataFrame, key_col: str, value_col: str) -> dict[str, Any]:
    any_dict = (
        df.select(key_col, value_col).toPandas().set_index(key_col)[value_col].to_dict()
    )
    return cast(dict[str, Any], any_dict)


def run(  # noqa: PLR0915
    spark: SparkSession,
    model_id: str,
    wiki_dbs: Sequence[str],
    directory: pathlib.Path,
    thresholds: Sequence[float] = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9],
    n_max: int | None = None,
) -> None:
    """
    Performs backtesting evlauation of trained model on held-out testset.

    :param wiki_dbs: Wiki dbs for which to get recommendations
    :param model_id: Model name with which to run evaluataion. Can be the
    WIKI ID for language dependent evaluation
    :param thresholds: Thresholds value for links to be recommended (set if you want to
    evaluate for specific threshold values, if not specified, will evaluate for a range
    0.0, 0.1, ..., 0.9
    :param directory: Path to output directory
    :param n_max: Maximum number of sentences to evaluate
    """

    anchors_df = (
        spark.read.parquet(str(directory / "anchors_filtered"))
        .where(F.col("wiki_db").isin(*wiki_dbs))
        .cache()
    )
    pageids_df = (
        spark.read.parquet(str(directory / "pageids"))
        .withColumnRenamed("pid", "page_id")
        .drop("wikitext")
        .where(F.col("wiki_db").isin(*wiki_dbs))
        .cache()
    )
    redirects_df = (
        spark.read.parquet(str(directory / "redirects"))
        .where(F.col("wiki_db").isin(*wiki_dbs))
        .cache()
    )
    embeddings_df = (
        spark.read.parquet(str(directory / "embeddings"))
        .where(F.col("wiki_db").isin(*wiki_dbs))
        .cache()
    )

    sentences_df = (
        spark.read.parquet(str(directory / "testing/sentences_test.parquet"))
        .where(F.col("wiki_db").isin(*wiki_dbs))
        .cache()
    )

    ## load the trained model
    ## use a fourth of the cpus, at most 8
    n_cpus_max = min([int(multiprocessing.cpu_count() / 4), 8])

    with fsspec.open(
        hdfs_path(directory / model_id / f"{model_id}.linkmodel.joblib"), "rb"
    ) as file:
        model = joblib.load(file)
        model.n_jobs = n_cpus_max

    with fsspec.open(
        hdfs_path(directory / model_id / f"{model_id}.encoder.joblib"), "rb"
    ) as file:
        wiki_db_encoder = joblib.load(file)

    summary = df = pd.DataFrame()
    for wiki_db in wiki_dbs:
        anchors = to_dict(
            anchors_df.where(F.col("wiki_db") == wiki_db), "anchor", "link_occurrence"
        )

        pageids = to_dict(
            pageids_df.where(F.col("wiki_db") == wiki_db), "title", "page_id"
        )

        redirects = to_dict(
            redirects_df.where(F.col("wiki_db") == wiki_db), "title_from", "title_to"
        )

        word2vec = to_dict(
            embeddings_df.where(F.col("wiki_db") == wiki_db), "page_title", "embedding"
        )

        ## load the test-set
        test_set = (
            sentences_df.where(F.col("wiki_db") == wiki_db)
            .select("page_title", "sentence")
            .toPandas()
            .to_numpy()
            .tolist()
        )

        wiki_url = get_wiki_url(wiki_db)
        language_code = get_language_code(wiki_url)
        tokenizer = Tokenizer(language_code=language_code)

        list_result = []
        for threshold in thresholds:
            dict_eval = {}
            print("threshold: ", threshold)
            tot_TP = 0.0
            tot_rel = 0.0
            tot_ret = 0.0
            count_doc = 0
            for page, page_wikicode in test_set:
                try:
                    input_code = page_wikicode
                    ## get links from original wikitext (and resolve redirects)
                    inp_pairs = getLinks(
                        input_code, redirects=redirects, pageids=pageids
                    )

                    ## if no links in main namespace, go to next item
                    if len(inp_pairs) == 0:
                        continue

                    input_code_nolinks = mwph.parse(page_wikicode).strip_code()
                    output_code = process_page(
                        input_code_nolinks,
                        page,
                        anchors,
                        pageids,
                        redirects,
                        word2vec,
                        model,
                        wiki_db_encoder,
                        wiki_db,
                        language_code,
                        tokenizer,
                        threshold=threshold,
                        pr=False,
                    )

                    # output_code is wikicode but getLinks requires str
                    output_code = str(output_code)

                    ## get links from predicted wikitext
                    out_pairs = getLinks(
                        output_code, redirects=redirects, pageids=pageids
                    )

                    TP = dict(set(inp_pairs.items()).intersection(out_pairs.items()))
                    tot_TP += len(TP)
                    tot_ret += len(out_pairs)
                    tot_rel += len(inp_pairs)
                    count_doc += 1
                except Exception as e:
                    print("ERROR: ", type(e).__name__, e)
                    pass
                if count_doc == n_max:
                    break

            if tot_ret > 0:
                micro_precision = tot_TP / tot_ret
            else:
                micro_precision = nan
            if tot_rel > 0:
                micro_recall = tot_TP / tot_rel
            else:
                micro_recall = nan
            print(f"finished: {count_doc} sentences")
            print("micro_precision:\t", micro_precision)
            print("micro_recall:\t", micro_recall)
            print("----------------------")
            dict_eval["threshold"] = threshold
            dict_eval["N"] = count_doc
            dict_eval["micro_precision"] = micro_precision
            dict_eval["micro_recall"] = micro_recall
            list_result.append(dict_eval)

        df = pd.DataFrame.from_records(list_result).sort_values(by="threshold")
        df["wiki_db"] = wiki_db
        summary = pd.concat([summary, df])

    with fsspec.open(
        hdfs_path(directory / model_id / f"{model_id}.backtest.eval.csv"), "w"
    ) as file:
        summary.to_csv(file, index=False)
