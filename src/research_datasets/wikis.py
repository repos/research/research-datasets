from pyspark.sql import DataFrame, SparkSession


def get_wikis(
    spark: SparkSession,
    is_canonical: bool = False,
    table_name: str = "canonical_data.wikis",
) -> DataFrame:
    """
    Return a DataFrame of wikis based on category
    """
    table = spark.table(table_name)

    if is_canonical:
        return (
            table.withColumnRenamed("database_code", "wiki_db")
            .select("wiki_db")
            .where("database_group='wikipedia'")
        )
    else:
        return table.withColumnRenamed("database_code", "wiki_db").select("wiki_db")
