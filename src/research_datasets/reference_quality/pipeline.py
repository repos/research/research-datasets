import logging
import pathlib
import sqlite3
from collections.abc import Mapping, Sequence

import fsspec  # type: ignore[import-untyped]
import pandas as pd
import pyspark.sql.types as T
from pyspark.sql import Column, DataFrame, SparkSession, Window
from pyspark.sql import functions as F

from research_datasets.reference_quality.domain import extract_cited_domains
from research_datasets.reference_quality.perennial_sources import (
    Lang,
    parse_perennial_sources,
)
from research_transformations.transformation import ColName, Transformation


@F.udf(returnType=T.ArrayType(T.StringType()))
def _extract_cited_domains_udf(wikitext: str) -> list[str]:
    return list(set(extract_cited_domains(wikitext)))


def _enrich_with_cited_domains(wikitext_df: DataFrame) -> DataFrame:
    domain_df = wikitext_df.withColumn(
        colName="domain",
        # Use `explode_outer` instead of explode to avoid the udf being
        # run twice due to the inferFiltersFromGenerate optimizer rule.
        col=F.explode_outer(_extract_cited_domains_udf("revision_text")),
    ).dropna(subset="domain")

    return domain_df


def _filter_non_unique_ids(id: Sequence[ColName]) -> Transformation:
    def _(df: DataFrame) -> DataFrame:
        duplicate_df = df.groupBy(*id).count().where(F.col("count") > 1)
        return (
            df.alias("original")
            .join(duplicate_df.alias("duplicate"), on=list(id), how="left_anti")
            .select("original.*")
        )

    return _


def _enrich_with_revision_lifespan(revision_df: DataFrame) -> Transformation:
    def _(df: DataFrame) -> DataFrame:
        unique_revision_df = revision_df.transform(
            _filter_non_unique_ids(id=("wiki_db", "revision_parent_id"))
        )

        wiki_db_matches = F.col("revision.wiki_db") == F.col("successor.wiki_db")
        rev_id_matches = F.col("revision.revision_id") == F.col(
            "successor.revision_parent_id"
        )
        lifespan_df = df.alias("revision").join(
            unique_revision_df.alias("successor"),
            on=((wiki_db_matches) & (rev_id_matches)),
        )

        begin_time = F.col("revision.revision_timestamp").cast("long")
        end_time = F.col("successor.revision_timestamp").cast("long")
        return lifespan_df.select(
            "revision.*",
            F.col("successor.revision_id").alias("successor_revision_id"),
            F.col("successor.revision_timestamp").alias("end_timestamp"),
            (end_time - begin_time).alias("revision_lifespan_secs"),
        )

    return _


def _aggregate_revision_lifespan(by: Sequence[ColName]) -> Transformation:
    def _(lifespan_df: DataFrame) -> DataFrame:
        secs_in_day = 24 * 60 * 60
        agg_df = lifespan_df.groupBy(*by).agg(
            F.min("revision_id").alias("revision_added"),
            F.max("successor_revision_id").alias("revision_removed"),
            F.min("revision_timestamp").alias("first_appeared"),
            F.max("end_timestamp").alias("last_seen"),
            F.count("revision_id").alias("num_edits"),
            F.round(F.sum("revision_lifespan_secs") / secs_in_day).alias(
                "total_lifespan_days"
            ),
        )
        agg_df = agg_df.withColumn(
            colName="max_lifespan_days",
            col=F.round(
                (F.col("last_seen").cast("long") - F.col("first_appeared").cast("long"))
                / secs_in_day
            ),
        )
        return agg_df

    return _


def _with_revisions_since(revision_df: DataFrame) -> DataFrame:
    window = Window.partitionBy("wiki_db", "page_id").orderBy(
        F.desc("revision_timestamp")
    )
    return revision_df.withColumn(
        "revisions_since",
        F.row_number().over(window),
    )


def _with_edit_survival_ratio(revision_df: DataFrame) -> DataFrame:
    return revision_df.withColumn(
        colName="sur_edit_ratio",
        col=F.least((F.col("num_edits") / F.col("revisions_since")), F.lit(1)),
    )


def _extract_statistical_summary(percentiles: Mapping[str, float]) -> Transformation:
    def summary(col: ColName) -> list[Column]:
        percentile_cols = (
            F.percentile_approx(col, percentage).alias(f"{col}_{label}")
            for label, percentage in percentiles.items()
        )
        return [F.mean(col).alias(f"{col}_mean"), *percentile_cols]

    def _(df: DataFrame) -> DataFrame:
        cols = (
            "ref_max_lifespan",
            "ref_real_lifespan",
            "num_edits",
            "sur_edit_ratio",
        )
        return df.groupBy("wiki_db", "domain").agg(
            F.countDistinct("page_id").alias("page_distinct_cnt"),
            F.countDistinct("user_text").alias("add_user_distinct_cnt"),
            *(summary_col for col in cols for summary_col in summary(col)),
        )

    return _


def enrich_with_domain_statistics(
    wiki_dbs: Sequence[str], min_revisions_since: int = 10
) -> Transformation:
    def _(wikitext_df: DataFrame) -> DataFrame:
        summary_percentiles = dict(p25=0.25, median=0.5, p75=0.75)
        domain_key = ("wiki_db", "page_id", "domain")

        filtered_wikitext_df = (
            wikitext_df.where(F.col("wiki_db").isin(*wiki_dbs))
            .where(F.col("page_namespace") == 0)
            .where(F.col("page_redirect_title") == "")
            .withColumn("revision_timestamp", F.to_timestamp("revision_timestamp"))
        )
        revision_df = filtered_wikitext_df.drop("revision_text")
        revisions_since_df = revision_df.transform(_with_revisions_since)

        domain_stats_df = (
            filtered_wikitext_df.transform(_enrich_with_cited_domains)
            .drop("revision_text")
            .transform(_enrich_with_revision_lifespan(revision_df))
            .transform(_aggregate_revision_lifespan(by=domain_key))
            .withColumnRenamed("revision_added", "revision_id")
            .join(revisions_since_df, on=["wiki_db", "page_id", "revision_id"])
            .where(F.col("revisions_since") >= min_revisions_since)
            .transform(_with_edit_survival_ratio)
            .withColumnRenamed("total_lifespan_days", "ref_real_lifespan")
            .withColumnRenamed("max_lifespan_days", "ref_max_lifespan")
            .transform(_extract_statistical_summary(summary_percentiles))
        )
        return domain_stats_df

    return _


@F.udf(returnType=T.MapType(T.StringType(), T.StringType()))
def _parse_perennial_sources_udf(wiki_db: str, wikitext: str) -> dict[str, str]:
    return parse_perennial_sources(
        lang=Lang(wiki_db.replace("wiki", "")),
        wikitext=wikitext,
    )


def enrich_with_perennial_source_labels(langs: Sequence[Lang]) -> Transformation:
    def _(wikitext_current_df: DataFrame) -> DataFrame:
        return (
            wikitext_current_df.where(
                F.col("wiki_db").isin(*(f"{lang}wiki" for lang in langs))
            )
            .where(
                F.col("page_title").isin(*(lang.page_title for lang in langs)),
            )
            .select(
                F.explode(
                    _parse_perennial_sources_udf(
                        F.col("wiki_db"), F.col("revision_text")
                    )
                ).alias("domain", "label"),
                "wiki_db",
            )
        )

    return _


def enrich_with_reference_risk_features(
    wiki_dbs: Sequence[str],
    wikitext_current_df: DataFrame,
    min_revisions_since: int = 10,
) -> Transformation:
    langs: list[Lang] = []
    for wiki_db in wiki_dbs:
        try:
            langs.append(Lang(wiki_db.replace("wiki", "")))
        except ValueError:
            logging.info("No perennial sources found for %s.", wiki_db)

    def _(mediawiki_wikitext_history_df: DataFrame) -> DataFrame:
        perennial_sources_df = wikitext_current_df.transform(
            enrich_with_perennial_source_labels((*langs, Lang("en")))
        ).cache()

        features_df = (
            mediawiki_wikitext_history_df.transform(
                enrich_with_domain_statistics(wiki_dbs, min_revisions_since)
            )
            .alias("domain")
            .join(
                F.broadcast(perennial_sources_df.alias("ps_local")),
                on=["wiki_db", "domain"],
                how="left",
            )
            .join(
                F.broadcast(
                    perennial_sources_df.where("wiki_db='enwiki'").alias("ps_en")
                ),
                on="domain",
                how="left",
            )
        )
        return features_df.select(
            "domain.*",
            F.col("ps_local.label").alias("psl_local"),
            F.col("ps_en.label").alias("psl_enwiki"),
        )

    return _


def to_sqlite(
    dataset: pathlib.Path,
    database: pathlib.Path,
    table: str,
    partition: Sequence[tuple[str, str, str]] | None = None,
) -> None:
    dataset_df = pd.read_parquet(f"hdfs://{dataset}", filters=partition).set_index(
        ["wiki_db", "domain"]
    )

    cache_dir = pathlib.Path("/tmp/reference_risk")
    with fsspec.open(
        f"simplecache::hdfs://{database}",
        simplecache={"cache_storage": str(cache_dir), "same_names": True},
        mode="wb",
    ):
        # `connect` only accepts a local file path, so we use fsspec
        # simplecache to write a local copy of the database that gets copied
        # to hdfs when the cache file is closed.
        with sqlite3.connect(cache_dir / database.name) as conn:
            dataset_df.to_sql(
                name=table,
                con=conn,
                if_exists="replace",
                index=True,
                chunksize=100_000,
            )


def run(
    spark: SparkSession,
    mediawiki_snapshot: str,
    output: pathlib.Path,
    partitions: int = 4,
    sqlite_db: pathlib.Path | None = None,
    wikis: Sequence[str] | None = None,
) -> None:
    wikitext_history_df = spark.table("wmf.mediawiki_wikitext_history").where(
        F.col("snapshot") == mediawiki_snapshot
    )
    wikitext_current_df = spark.table("wmf.mediawiki_wikitext_current").where(
        F.col("snapshot") == mediawiki_snapshot
    )
    features_df = wikitext_history_df.transform(
        enrich_with_reference_risk_features(
            wikitext_current_df=wikitext_current_df,
            wiki_dbs=wikis or [f"{lang.value}wiki" for lang in Lang],
        )
    )
    (
        features_df.repartition(partitions)
        .withColumn("snapshot", F.lit(mediawiki_snapshot))
        .write.partitionBy("snapshot")
        .mode("overwrite")
        .format("parquet")
        .save(str(output))
    )

    if sqlite_db:
        to_sqlite(
            dataset=output,
            partition=(("snapshot", "==", mediawiki_snapshot),),
            database=sqlite_db,
            table="domains",
        )
