import re
import urllib.parse

import regex  # type: ignore

SCHEME_RE = r"(?:ftp|http)s?://"
URL_ALLOWED_CHARS_RE = r"[^\s|{}\[\].<>]"


def is_archive_domain(domain: str) -> bool:
    archive_domains = (
        "archive-it.org",
        "archive.is",
        "archive.org",
        "archive.today",
        "web.archive.org",
    )
    return any(archive_domain in domain for archive_domain in archive_domains)


def parse_url_domain(url: str) -> str | None:
    """Extract the domain from a URL.

    If the domain is an archive domain (e.g. web.archive.org), it tries to
    recursively parse the original URL from the path, given that it contains
    a scheme. Returns `None` if the domain cannot be parsed.
    """
    try:
        parsed_url = urllib.parse.urlparse(urllib.parse.unquote(url))
        domain = parsed_url.netloc
    except ValueError:
        return None

    if is_archive_domain(domain):
        match = re.search(rf"{SCHEME_RE}.+", parsed_url.path, flags=re.VERBOSE)
        if match:
            original_url = match.group()
            # Occasionally, an archived link can point to another
            # archived link. Recursive parsing handles that.
            original_domain = parse_url_domain(original_url)
            domain = original_domain if original_domain else domain

    return domain if domain else None


def normalize_domain(domain: str) -> str | None:
    subdomain_re = r"""
    ^www  # www in the beginning
    \d*   # optional number
    \.    # period
    """
    sld_with_tld_re = r"""
    (?:[\p{Dash}\w]{1,64}\.)+  # second level domain i.e. 'example.' or 'example.co.'
    [\p{Dash}\w]{1,64}\.?      # top level domain i.e. 'org' or 'uk'
    """
    stripped_domain = re.sub(subdomain_re, "", domain.lower(), flags=re.VERBOSE)
    # Also strip the port and any other invalid chars captured during parsing.
    # NOTE: This uses `regex` instead of `re` because the latter seems to miss valid
    # characters sometimes even though they're listed as alphanumeric in the unicode db.
    domain_match = regex.match(sld_with_tld_re, stripped_domain, flags=regex.VERBOSE)
    return domain_match.group() if domain_match else None


def parse_references(wikitext: str) -> list[str]:
    reference_re = r"""
    <ref[^/]*?>  # opening tag with optional attr(s)
    (.*?)        # enclosed contents
    </ref>       # closing tag
    """
    references = re.findall(reference_re, wikitext, flags=re.DOTALL | re.VERBOSE)
    return references


def extract_cited_domains(wikitext: str) -> list[str]:
    """Extract domains of URLs used as citations in the provided wikitext.

    Parses <ref> tags out of the wikitext. The domain is then parsed from
    the first non-archive (e.g. wayback archive) link within the reference.
    If no non-archive links are found, the first one is returned.
    """
    url_re = rf"""
    \b{SCHEME_RE}
    {URL_ALLOWED_CHARS_RE}+         # sub-domain
    (?:\.{URL_ALLOWED_CHARS_RE}+)*  # SLD and TLD
    """

    def extract_domain(link: str) -> str | None:
        domain = parse_url_domain(link)
        return normalize_domain(domain) if domain else None

    domains: list[str] = []
    for reference in parse_references(wikitext):
        urls = re.findall(url_re, reference, flags=re.VERBOSE)
        if not urls:
            continue

        domain = next(
            (
                extracted_domain
                for url in urls
                if (extracted_domain := extract_domain(url))
            ),
            None,
        )
        if domain:
            domains.append(domain)

    return domains
