import itertools
import re
from collections import defaultdict
from collections.abc import Callable, Mapping, Sequence
from enum import Enum
from typing import TypeAlias

import mwparserfromhell as mwp  # type: ignore[import-untyped]
from mwedittypes.utils import wikitext_to_plaintext  # type: ignore[import-untyped]

from research_datasets.reference_quality.domain import normalize_domain


class Lang(str, Enum):
    EN = "en"
    FA = "fa"
    FR = "fr"
    LT = "lt"
    NE = "ne"
    PT = "pt"
    RU = "ru"
    SV = "sv"
    TR = "tr"
    UK = "uk"
    VI = "vi"
    ZH = "zh"

    @property
    def page_title(self) -> str:
        match self:
            case self.EN:
                page_title = "Wikipedia:Reliable sources/Perennial sources"
            case self.FA:
                page_title = "ویکی‌پدیا:منابع معتبر/منابع مکرر"
            case self.FR:
                page_title = "Wikipédia:Observatoire des sources"
            case self.LT:
                page_title = "Vikipedija:Nepatikimi šaltiniai"
            case self.NE:
                page_title = "विकिपिडिया:विश्वसनीय स्रोतहरू/स्थायी स्रोतहरू"
            case self.PT:
                page_title = "Wikipédia:Fontes não confiáveis/Lista"
            case self.RU:
                page_title = "Википедия:Часто используемые источники"
            case self.SV:
                page_title = "Wikipedia:Trovärdiga källor/Bedömningar"
            case self.TR:
                page_title = "Vikipedi:Güvenilir kaynaklar/Mütemadi kaynaklar"
            case self.UK:
                page_title = "Вікіпедія:Список оцінених джерел"
            case self.VI:
                page_title = "Wikipedia:Danh sách nguồn đáng tin cậy/Tiếng Việt"
            case self.ZH:
                page_title = "Wikipedia:可靠来源/常见有争议来源列表"

        return page_title


TEMPLATES = {
    "en": {"status": "WP:RSPSTATUS", "domain": "WP:RSPUSES"},
    "fa": {"status": "/وضعیت", "domain": "/استفاده"},
    "uk": {"status": "/Статус", "domain": "/Використання"},
    "vi": {"status": "WP:NDTCTT", "domain": "WP:NDTCSD"},
    "ru": {"status": "/Статус", "domain": "/Uses"},
}

STATUS_TO_LABEL = {
    "en": {
        "gr": "Generally reliable",
        "sr": "Semi reliable",
        "nc": "No consensus",
        "gu": "Generally unreliable",
        "d": "Deprecated",
        "b": "Blacklisted",
    },
    "ru": {
        "а": "Generally reliable",  # noqa: RUF001 (allow ambiguous unicode char)
        "нк": "No consensus",
        "на": "Generally unreliable",
        "нр": "нр",
        "b": "Blacklisted",
    },
    "sv": {
        "noun_project_-_supprimer_round.svg": "Blacklisted",
        "green_checkmark_circle.svg": "Generally reliable",
        "exclamation.svg": "No consensus",
        "forbidden_symbol.svg": "Generally unreliable",
        "israel_road_sign_304.svg": "Deprecated",
    },
    "tr": {
        "yes_check_circle.svg": "Generally reliable",
        "achtung-orange.svg": "No consensus",
        "pictogram_voting_wait.svg": "No consensus",
        "argentina_-_no_symbol.svg": "Generally unreliable",
        "stop_hand.svg": "Deprecated",
    },
}

# Listed in order of decreasing priority i.e.
# which one to pick when a domain has more than one label.
LABELS = (
    "Blacklisted",
    "Deprecated",
    "Generally unreliable",
    "No consensus",
    "Semi reliable",
    "Generally reliable",
    "нр",
)


def _parse_domains_in_text(text: str) -> list[str]:
    domain_re = r"""
    \b                                 # not preceded by a word character
    (?:
      (?!-)                            # not preceded by a '-'
      [a-zA-Z0-9-\u00A0-\uFFFF]{1,63}  # unicode characters and numbers
      (?<!-)                           # not followed by a '-'
      \.                               # ends with a period
    )+                                 # one or more groups i.e. subdomain, SLD etc.
    [a-zA-Z\u00A0-\uFFFF]{2,63}        # unicode characters for TLD
    \b                                 # not followed by a word character
    """
    normalized_domains = [
        normalized_domain
        for domain in re.findall(domain_re, text, flags=re.VERBOSE)
        if (normalized_domain := normalize_domain(domain))
    ]
    return normalized_domains


def _normalize_commons_filename(filename: str) -> str:
    colon_location = filename.find(":")
    normalized_filename = filename[colon_location + 1 :].replace(" ", "_").lower()
    return normalized_filename


def _parse_status_in_template(
    node: mwp.wikicode.Wikicode,
    template_name: str | None,
    default_template_names: Sequence[str] = ("/Status",),
) -> str | None:
    template_names = (
        (*default_template_names, template_name)
        if template_name
        else default_template_names
    )

    def is_status_template(template: mwp.nodes.Template) -> bool:
        return any(template.name.matches(name) for name in template_names)

    status_template = node.contents.filter_templates(matches=is_status_template)
    if not status_template:
        return None

    [status_template] = status_template
    status, *_other_params = status_template.params
    blacklist_params = (
        "b",
        "blacklisted",
        "с",  # noqa: RUF001 (allow ambiguous unicode char)
    )
    if any(status_template.has(param) for param in blacklist_params):
        status = "b"

    return str(status)


def _parse_status_in_icons(node: mwp.wikicode.Wikicode) -> list[str]:
    return [
        _normalize_commons_filename(filename=str(status_icon.title))
        for status_icon in node.contents.filter_wikilinks()
    ]


def _parse_domains_in_template(
    node: mwp.wikicode.Wikicode,
    template_name: str | None,
    default_template_names: Sequence[str] = ("/Uses",),
) -> list[str] | None:
    template_names = (
        (*default_template_names, template_name)
        if template_name
        else default_template_names
    )

    def is_domain_template(template: mwp.nodes.Template) -> bool:
        return any(template.name.matches(name) for name in template_names)

    domain_templates = node.contents.filter_templates(matches=is_domain_template)
    if not domain_templates:
        return None

    domains = [
        normalized_domain
        for domain_template in domain_templates
        for domain in domain_template.params
        if (normalized_domain := normalize_domain(str(domain).lower()))
    ]
    return domains


def _parse_domains_in_linked_sections(
    node: mwp.wikicode.Wikicode, parsed_article: mwp.wikicode.Wikicode
) -> list[str] | None:
    section_links = node.contents.filter_wikilinks()
    sections = (
        str(section)
        for link in section_links
        for section in parsed_article.get_sections(
            matches=str(link.title.lstrip("#")), include_headings=False
        )
    )
    domains = [
        domain for section in sections for domain in _parse_domains_in_text(section)
    ]
    return domains


def _map_domain_to_label(
    domain_to_status: Mapping[str, list[str]], lang: Lang
) -> dict[str, str]:
    status_label = STATUS_TO_LABEL[lang]
    domain_to_label: dict[str, str] = {}
    for domain, statuses in domain_to_status.items():
        labels = (status_label[status] for status in statuses)
        worst_label = min(labels, key=LABELS.index)
        domain_to_label[domain] = worst_label

    return domain_to_label


Row: TypeAlias = mwp.wikicode.Wikicode


def parse_perennial_sources_table(
    lang: Lang,
    wikitext: mwp.wikicode.Wikicode,
    parse_domain_fn: Callable[[Row], str | list[str] | None],
    parse_status_fn: Callable[[Row], str | list[str] | None],
) -> dict[str, str]:
    [table] = wikitext.filter_tags(
        recursive=False,
        matches="wikitable sortable",
    )

    domain_to_status: dict[str, list[str]] = defaultdict(list[str])
    for row in table.contents.filter_tags(recursive=False):
        status = parse_status_fn(row)
        domains = parse_domain_fn(row) if status else None
        if not (domains and status):
            continue

        all_statuses = [status] if isinstance(status, str) else status
        domain_alternatives = [domains] if isinstance(domains, str) else domains
        for domain, status in itertools.product(domain_alternatives, all_statuses):
            domain_to_status[domain].append(status)

    return _map_domain_to_label(domain_to_status, lang)


def parse_perennial_sources(lang: Lang, wikitext: str) -> dict[str, str]:
    for tag in (r"</?onlyinclude>", r"</?small>"):
        wikitext = re.sub(tag, "", wikitext)

    parsed_article = mwp.parse(wikitext, skip_style_tags=True)
    status_template = TEMPLATES.get(lang, {}).get("status")
    domain_template = TEMPLATES.get(lang, {}).get("domain")

    perennial_sources: dict[str, str]
    match lang:
        case Lang.EN | Lang.FA | Lang.NE | Lang.UK | Lang.VI | Lang.ZH:
            perennial_sources = parse_perennial_sources_table(
                lang=Lang.EN,
                wikitext=parsed_article,
                parse_domain_fn=lambda row: (
                    _parse_domains_in_template(row, template_name=domain_template)
                    or _parse_domains_in_linked_sections(
                        node=row.contents.nodes[-1], parsed_article=parsed_article
                    )
                ),
                parse_status_fn=lambda row: _parse_status_in_template(
                    row, template_name=status_template
                ),
            )

        case Lang.FR:
            perennial_sources = {}
            for ods_template in parsed_article.filter_templates(
                matches=lambda template: template.name.matches("Source ODS")
            ):
                if not ods_template.has("url"):
                    continue
                urls = str(ods_template.get("url").value)
                domains = _parse_domains_in_text(urls)
                description = wikitext_to_plaintext(
                    wt=ods_template.get("résumé").value, lang=lang
                )
                perennial_sources |= itertools.product(domains, (description.strip(),))

        case Lang.LT:
            domains = _parse_domains_in_text(wikitext)
            perennial_sources = dict(
                itertools.product(domains, ("Generally unreliable",))
            )

        case Lang.PT:
            perennial_sources = parse_perennial_sources_table(
                lang=Lang.EN,
                wikitext=parsed_article,
                parse_domain_fn=lambda row: _parse_domains_in_text(
                    text=str(row.contents.nodes[-1])
                ),
                parse_status_fn=lambda _: "gu",
            )

        case Lang.RU:
            perennial_sources = parse_perennial_sources_table(
                lang=lang,
                wikitext=parsed_article,
                parse_domain_fn=lambda row: _parse_domains_in_text(str(row)),
                parse_status_fn=lambda row: _parse_status_in_template(
                    row, template_name=status_template
                ),
            )

        case Lang.SV:
            col_count = 7
            perennial_sources = parse_perennial_sources_table(
                lang=lang,
                wikitext=parsed_article,
                parse_domain_fn=lambda row: _parse_domains_in_text(
                    text=str(row.contents.nodes[-2])
                ),
                parse_status_fn=lambda row: (
                    _parse_status_in_icons(node=row.contents.nodes[1])
                    if len(row.contents.nodes) == col_count
                    else None
                ),
            )

        case Lang.TR:
            col_count = 5
            perennial_sources = parse_perennial_sources_table(
                lang=lang,
                wikitext=parsed_article,
                parse_domain_fn=lambda row: _parse_domains_in_text(
                    text=str(row.contents.nodes[0])
                ),
                parse_status_fn=lambda row: (
                    _parse_status_in_icons(node=row.contents.nodes[2])
                    if len(row.contents.nodes) == col_count
                    else None
                ),
            )

    return perennial_sources
