import json
import pathlib
import subprocess
import sys
from collections.abc import Iterable
from typing import Any

import datamodel_code_generator as dcg
import datamodel_code_generator.model as dcg_model
import datamodel_code_generator.parser.jsonschema as dcg_jsonschema

from research_datasets.cli.core import Command, PydanticModel


def get_model_definition(
    model_name: str, model: type[PydanticModel], docstring: str | None = None
) -> str:
    model_schema = model.schema()
    if docstring:
        model_schema["description"] = docstring

    for key in ("args", "kwargs", "v__duplicate_kwargs"):
        model_schema["properties"].pop(key)

    model_types = dcg_model.get_data_model_types(
        data_model_type=dcg.DataModelType.PydanticBaseModel,
        target_python_version=dcg.PythonVersion.PY_310,
    )
    parser = dcg_jsonschema.JsonSchemaParser(
        json.dumps(model_schema),
        class_name=model_name,
        data_model_type=model_types.data_model,
        data_model_root_type=model_types.root_model,
        data_model_field_type=model_types.field_model,
        data_type_manager_type=model_types.data_type_manager,
        dump_resolve_reference_action=model_types.dump_resolve_reference_action,
        set_default_enum_member=True,
        strict_nullable=True,
        use_default_kwarg=True,
        use_schema_description=True,
    )
    model_definition = parser.parse()
    assert isinstance(model_definition, str)
    return model_definition


def generate_args_definitions(
    commands: Iterable[Command[Any]], output_path: pathlib.Path, instructions: str = ""
) -> pathlib.Path:
    """Get Pydantic model definitions for command `args`.

    The generated file also includes all relevant imports and defintions
    for commands args field types.
    """

    model_definitions: list[str] = []
    seen_models: set[str] = set()

    for cmd in commands:
        model_definition = get_model_definition(
            model_name="".join(part.capitalize() for part in cmd.name.split(".")),
            model=cmd.args,
            docstring=f"Arguments for the command `{cmd.name}`",
        )

        # Don't add definitions for models common across args
        # such as Period again if it has been added before.
        for block in model_definition.split("\n\n\n"):
            if block.startswith("class"):
                model_name, *_ = block.partition("\n")
                if model_name in seen_models:
                    continue
                seen_models.add(model_name)
            model_definitions.append(block)

    output = instructions + "\n".join(model_definitions)
    output_path.write_text(output)
    output = str(output_path)

    # Since each model definition comes with its own imports,
    # combine and deduplicate them using isort.
    subprocess.run(
        [sys.executable, "-m", "isort", output, "--float-to-top", "--quiet"],
        check=False,
    )

    subprocess.run(
        [sys.executable, "-m", "ruff", "format", output, "--quiet"],
        check=False,
    )
    subprocess.run(
        [
            sys.executable,
            "-m",
            "ruff",
            "check",
            output,
            "--fix",
            "--unsafe-fixes",
            "--quiet",
        ],
        check=False,
    )
    return output_path
