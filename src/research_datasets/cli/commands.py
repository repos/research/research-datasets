from collections.abc import Generator

from pyspark.sql import SparkSession

import research_datasets.article_embeddings.pipeline
import research_datasets.article_quality
import research_datasets.article_quality.pipeline
import research_datasets.article_topics.pipeline
import research_datasets.mwaddlink
import research_datasets.mwaddlink.filter_dict_anchor
import research_datasets.mwaddlink.generate_addlink_model
import research_datasets.mwaddlink.generate_anchor_dictionary
import research_datasets.mwaddlink.generate_backtesting_data
import research_datasets.mwaddlink.generate_backtesting_eval
import research_datasets.mwaddlink.generate_training_data
import research_datasets.mwaddlink.generate_wdproperties
import research_datasets.reference_quality
import research_datasets.reference_quality.pipeline
import research_datasets.revert_risk
import research_datasets.revert_risk.base_features
import research_datasets.revert_risk.evaluate
import research_datasets.revert_risk.features
import research_datasets.revert_risk.predict
import research_datasets.revert_risk.train
import research_datasets.risk_observatory
import research_datasets.risk_observatory.pipeline
import research_datasets.top_pages
import research_datasets.top_pages.pipeline
import research_datasets.wikidiff
import research_datasets.wikidiff.pipeline
from research_datasets.cli.core import Command


def _get_spark_session() -> Generator[SparkSession, None, None]:
    """Get an existing spark session or create and return a new one
    if it does not exist.

    This function handles clean up before exiting by stopping the underlying
    spark context and is meant to be used with a context manager.
    """
    spark = SparkSession.builder.getOrCreate()
    yield spark
    spark.stop()


article_embeddings_commands = (
    Command(
        research_datasets.article_embeddings.pipeline.run,
        _get_spark_session,
    ),
)
article_topics_commands = (
    Command(
        research_datasets.article_topics.pipeline.run,
        _get_spark_session,
    ),
)
reference_quality_commands = (
    Command(
        research_datasets.reference_quality.pipeline.run,
        _get_spark_session,
    ),
)
revert_risk_commands = (
    Command(
        research_datasets.revert_risk.base_features.run,
        _get_spark_session,
    ),
    Command(
        research_datasets.revert_risk.features.run,
        _get_spark_session,
    ),
    Command(
        research_datasets.revert_risk.train.run,
        _get_spark_session,
    ),
    Command(
        research_datasets.revert_risk.evaluate.run_model_evaluation,
        _get_spark_session,
    ),
    Command(
        research_datasets.revert_risk.evaluate.run_model_comparison,
        _get_spark_session,
    ),
    Command(
        research_datasets.revert_risk.predict.run,
        _get_spark_session,
    ),
)
risk_observatory_commands = (
    Command(
        research_datasets.risk_observatory.pipeline.run_revert_risk_prediction,
        _get_spark_session,
    ),
    Command(
        research_datasets.risk_observatory.pipeline.run_high_risk_threshold_computation,
        _get_spark_session,
    ),
    Command(
        research_datasets.risk_observatory.pipeline.run_risk_statistics_computation,
        _get_spark_session,
    ),
)
top_pages_commands = (
    Command(
        research_datasets.top_pages.pipeline.run,
        _get_spark_session,
    ),
)
wikidiff_commands = (
    Command(
        research_datasets.wikidiff.pipeline.run_batch,
        _get_spark_session,
    ),
    Command(
        research_datasets.wikidiff.pipeline.run_wikis,
        _get_spark_session,
    ),
)
mwaddlink = (
    Command(
        research_datasets.mwaddlink.generate_anchor_dictionary.run, _get_spark_session
    ),
    Command(research_datasets.mwaddlink.generate_wdproperties.run, _get_spark_session),
    Command(research_datasets.mwaddlink.filter_dict_anchor.run, _get_spark_session),
    Command(
        research_datasets.mwaddlink.generate_backtesting_data.run, _get_spark_session
    ),
    Command(research_datasets.mwaddlink.generate_training_data.run, _get_spark_session),
    Command(research_datasets.mwaddlink.generate_addlink_model.run),
    Command(
        research_datasets.mwaddlink.generate_backtesting_eval.run, _get_spark_session
    ),
)
article_quality_commands = (
    Command(
        research_datasets.article_quality.pipeline.run_article_features,
        _get_spark_session,
    ),
    Command(
        research_datasets.article_quality.pipeline.run_article_scores,
        _get_spark_session,
    ),
)

COMMANDS: tuple[Command[None], ...] = (
    *article_embeddings_commands,
    *article_topics_commands,
    *reference_quality_commands,
    *revert_risk_commands,
    *risk_observatory_commands,
    *top_pages_commands,
    *wikidiff_commands,
    *mwaddlink,
    *article_quality_commands,
)
