from collections.abc import Sequence
from typing import TypeAlias

from pyspark.ml.feature import VectorAssembler
from pyspark.sql import DataFrame
from pyspark.sql import functions as F
from pyspark.sql import types as T

from research_transformations.transformation import Transformation

# A model is specified by a list of feature names
# TODO, type alias for now. a serialiable features definition used by training and
# inference When model features are defined as pydantic classes, it is more
# convenient convert between different usages of futures.
# E.g. derive a pyspark features schema for use with distributed xgboost
# Get json schema / directionary for use knowledge_integrity/liftwing
FeatureNames: TypeAlias = Sequence[str]


def assemble_features_vector(feature_names: FeatureNames) -> Transformation:
    """
    Convert the features into a spark ML vector expected
    by pyspark xgboost on CPU.
    """

    def _(df: DataFrame) -> DataFrame:
        # convert the json features into a typed pyspark features
        schema = T.StructType(
            [T.StructField(name, T.FloatType(), True) for name in feature_names]
        )
        df = df.withColumn("features", F.from_json("features", schema))

        feature_cols = [
            F.col(f"features.{name}").alias(f"feat-{name}") for name in feature_names
        ]
        feature_col_names = [f"feat-{name}" for name in feature_names]
        non_feature_cols = df.columns
        non_feature_cols.remove("features")

        # prepare columns for VectorAssembler
        df = df.select(
            *non_feature_cols,
            *feature_cols,
        )
        # fill na with 0
        df = df.na.fill(0, subset=feature_col_names)
        vec_assembler = VectorAssembler(
            inputCols=feature_col_names,
            outputCol="features",
        )
        return vec_assembler.transform(df).drop(*feature_col_names)

    return _
