from collections.abc import Iterable

import pandas as pd
from pyspark.sql import Column as SparkColumn
from pyspark.sql import DataFrame
from pyspark.sql import functions as F
from pyspark.sql import types as T
from sklearn.metrics import auc  # type: ignore

from research_transformations.transformation import Transformation


def binary_classifier_metrics(
    pred_col: SparkColumn,
    label_col: SparkColumn,
    grouping_cols: Iterable[SparkColumn] = (),
    thresholds: Iterable[int] = range(100),
) -> Transformation:
    """For a predictions from a binary classifier,
    compute precision, recall, accuracy, fpr, F1, auc
    for a series of threshold candidates. The auc
    column is the same for all thresholds of a
    group.

    pred_col should be a float column
    label_col should be a bool column
    """

    metrics = ["precision", "recall", "accuracy", "fpr", "F1"]

    def conditional_count(condition: SparkColumn) -> SparkColumn:
        return F.sum(
            F.when(
                condition,
                1,
            ).otherwise(0)
        )

    # define the necessary aggregations in a list that
    # will be computed in a single reduce step
    agg_cols = []
    for pth in thresholds:
        th = pth / 100
        agg_cols.extend(
            [
                conditional_count(label_col & (pred_col >= th)).alias(f"tp_{pth}"),
                conditional_count(~label_col & (pred_col >= th)).alias(f"fp_{pth}"),
                conditional_count(~label_col & (pred_col < th)).alias(f"tn_{pth}"),
                conditional_count(label_col & (pred_col < th)).alias(f"fn_{pth}"),
            ]
        )

    # calculate the statitics
    stats_cols = []
    for th in thresholds:
        stats_cols.extend(
            [
                # precision = tp / (tp + fp)
                (F.col(f"tp_{th}") / (F.col(f"tp_{th}") + F.col(f"fp_{th}"))).alias(
                    f"precision_{th}"
                ),
                # recall = tp / (tp + fn)
                (F.col(f"tp_{th}") / (F.col(f"tp_{th}") + F.col(f"fn_{th}"))).alias(
                    f"recall_{th}"
                ),
                # accuracy = (tp + tn) / total
                ((F.col(f"tp_{th}") + F.col(f"tn_{th}")) / F.col("total")).alias(
                    f"accuracy_{th}"
                ),
                # fpr = fp / (fp + tn)
                (F.col(f"fp_{th}") / (F.col(f"fp_{th}") + F.col(f"tn_{th}"))).alias(
                    f"fpr_{th}"
                ),
            ]
        )

    # the F1 score is calculated based on other statistics, so has to be
    # done in a second step
    f1_cols = []
    for th in thresholds:
        f1_cols.extend(
            [
                # F1 = 2 * precision * recall / (precision + recall)
                (
                    (2 * F.col(f"precision_{th}") * F.col(f"recall_{th}"))
                    / (F.col(f"precision_{th}") + F.col(f"recall_{th}"))
                ).alias(f"F1_{th}")
            ]
        )

    def transform(predictions_df: DataFrame) -> DataFrame:
        # aggregate all the needed counts
        counts = predictions_df.groupBy(*grouping_cols).agg(
            *agg_cols, F.count("*").alias("total")
        )
        stats_df = counts.select(*grouping_cols, *stats_cols)
        stats_df = stats_df.select(*stats_df.columns, *f1_cols)

        # stats contains all the statistics for a given grouping in a single row
        # e.g. there are 502 columns - 100 thresholds * 5 statistics + grouping_cols

        # to create the shape of the dataframe as currently specified, we explode the
        # thresholds so that they are in different rows

        thresholds_structs_df = stats_df.select(
            *grouping_cols,
            *[
                F.struct(
                    F.lit(th).alias("threshold"),
                    *[F.col(f"{m}_{th}").alias(m) for m in metrics],
                ).alias(f"threshold_{th}")
                for th in thresholds
            ],
        )

        metrics_df = thresholds_structs_df.select(
            *grouping_cols,
            F.explode(F.array(*[F.col(f"threshold_{th}") for th in thresholds])).alias(
                "thresholds_stats"
            ),
        ).select(*grouping_cols, "thresholds_stats.*")

        auc_df = metrics_df.transform(
            binary_classifier_auc(grouping_cols=grouping_cols)
        )

        if grouping_cols:
            return metrics_df.join(
                auc_df,
                on=[c._jc.toString() for c in grouping_cols],  # type: ignore
                how="left",
            )
        else:
            return metrics_df.crossJoin(auc_df)

    return transform


def binary_classifier_auc(
    grouping_cols: Iterable[SparkColumn] = (),
) -> Transformation:
    """Computes the AUC from the model metrics
    for a binary classifier
    """

    def auc_func(group_cols, df):  # type: ignore
        return pd.DataFrame([(*group_cols, auc(df.fpr, df.recall))])

    def transform(metrics_df: DataFrame) -> DataFrame:
        schema = metrics_df.select(*grouping_cols).schema.add("auc", T.FloatType())
        return metrics_df.groupby(*grouping_cols).applyInPandas(auc_func, schema=schema)

    return transform


def compare_auc(
    candidate_auc_df: DataFrame,
    reference_auc_df: DataFrame,
    compare_cols: Iterable[SparkColumn] = (),
) -> DataFrame:
    """Compares the auc values between a candidate model and a reference model.
    The input dataframes require an `auc` column"""
    if not compare_cols:
        # if there are no compare columns (e.g. there should be a single row)
        # we use a dummy unit column
        candidate_auc_df = candidate_auc_df.withColumn("unit", F.lit(""))
        reference_auc_df = reference_auc_df.withColumn("unit", F.lit(""))
        compare_cols = (F.col("unit"),)

    aucs_df = candidate_auc_df.withColumnRenamed("auc", "candidate_auc").join(
        reference_auc_df.withColumnRenamed("auc", "reference_auc"),
        on=[c._jc.toString() for c in compare_cols],  # type: ignore
        how="inner",
    )

    return (
        aucs_df.groupby(
            F.round(F.col("candidate_auc") - F.col("reference_auc"), 2).alias(
                "auc_diff"
            )
        )
        .agg(
            F.count("*").alias("count"),
            F.collect_list(F.concat(*compare_cols)).alias("group"),
        )
        .sort("auc_diff")
    )
