import logging
from collections.abc import Callable
from typing import TypeAlias

from pyspark.sql import DataFrame, SparkSession, Window
from pyspark.sql import functions as F

Transformation: TypeAlias = Callable[[DataFrame], DataFrame]

ColName: TypeAlias = str


def stratified_sample(
    cols: list[str],
    strata_size: int,
    seed: int | None,
    repartition: bool = False,
) -> Transformation:
    """Returns a same-size stratified sample based on where the strata are
    all the unique values in `cols`. The maximum size of each stratum depends
    equals `strata_size`.
    """

    def _(df: DataFrame) -> DataFrame:
        # Partitioning by just *cols when taking a stratified sample might lead
        # to large partitions that are also skewed. Instead we first give each row
        # a "partition" number based on the hash code of its page_id (MurmurHash).
        # page_id works well to distribute revisions across partitions because their
        # distribution across page_ids is fairly uniform with relatively few outliers.
        if repartition:
            nonlocal strata_size
            spark = SparkSession.builder.getOrCreate()
            shuffle_partitions = int(
                spark.conf.get("spark.sql.shuffle.partitions"), base=10
            )
            if strata_size > 10 * shuffle_partitions:
                df = df.withColumn(
                    "partition", (F.rand() * shuffle_partitions).cast("int")
                )
                cols.append("partition")
                strata_size = strata_size // shuffle_partitions + 1
            else:
                logging.warning("no partitioning applied, strata_size too small")

        window = Window.partitionBy(*cols).orderBy(F.rand(seed))
        df = (
            df.withColumn("row_num", F.row_number().over(window))
            .filter(f"row_num <= {strata_size}")
            .drop("row_num")
        )
        return df.drop("partition")

    return _


def with_parent_revision_history(
    parent_df: DataFrame,
    rev_id: ColName = "revision_id",
    parent_rev_id: ColName = "revision_parent_id",
    wiki_db: ColName = "wiki_db",
    keep_nulls: bool = False,
) -> Transformation:
    """Aligns the revisions in the dataframe with their parent revisions
    from `parent_df` using `rev_id` and `parent_rev_id`. The resulting dataframe
    will have all existing columns both for the child and parent revisions, nested
    in two struct columns ('revision' & 'parent') with identical fields.
    """

    def _(df: DataFrame) -> DataFrame:
        rev_df = df.alias("rev")
        rev_parent_df = parent_df.alias("parent")

        # join conditions
        rev_id_matches = F.col(f"rev.{parent_rev_id}") == F.col(f"parent.{rev_id}")
        wiki_db_matches = F.col(f"rev.{wiki_db}") == F.col(f"parent.{wiki_db}")

        rev_df = rev_df.join(
            rev_parent_df,
            on=((rev_id_matches) & (wiki_db_matches)),
            how="left" if keep_nulls else "inner",
        )

        return rev_df.select(
            F.struct("rev.*").alias("revision"), F.struct("parent.*").alias("parent")
        )

    return _


def filter_disputed_reverts(
    reverting_rev_df: DataFrame,
    rev_id: ColName = "revision_id",
    reverting_rev_id: ColName = "revision_first_identity_reverting_revision_id",
    is_reverted: ColName = "revision_is_identity_reverted",
    page_id: ColName = "page_id",
    wiki_db: ColName = "wiki_db",
) -> Transformation:
    """Filters all disputed reverts. A disputed revert is any revert
    whose reverting revision has also been reverted. This phenomenon
    is also known as edit wars.
    """

    def _(df: DataFrame) -> DataFrame:
        rev_df = df.alias("rev")
        reverting_df = reverting_rev_df.alias("reverter")

        # join conditions
        rev_id_matches = F.col(f"rev.{reverting_rev_id}") == F.col(f"reverter.{rev_id}")
        page_id_matches = F.col(f"rev.{page_id}") == F.col(f"reverter.{page_id}")
        wiki_db_matches = F.col(f"rev.{wiki_db}") == F.col(f"reverter.{wiki_db}")

        rev_df = rev_df.join(
            reverting_df,
            on=((rev_id_matches) & (page_id_matches) & (wiki_db_matches)),
            how="left",
        )
        # filter out any reverts where the reverting revision
        # is also reverted i.e. edit wars
        rev_df = rev_df.filter(
            F.col(f"reverter.{rev_id}").isNull() | ~F.col(f"reverter.{is_reverted}")
        )
        return rev_df.select("rev.*")

    return _


def with_wikitext(
    wikitext_df: DataFrame,
    rev_id: ColName = "revision_id",
    wikitext_rev_id: ColName = "revision_id",
    rev_text: ColName = "revision_text",
    wiki_db: ColName = "wiki_db",
) -> Transformation:
    """Returns a new dataframe by adding a column that contains the revision
    wikitext, joining with `wikitext_df` on `rev_id` and `wikitext_rev_id` columns.
    """

    def _(df: DataFrame) -> DataFrame:
        df = df.alias("rev")
        wt_df = wikitext_df.alias("wt")

        # join conditions
        rev_id_matches = F.col(f"rev.{rev_id}") == F.col(f"wt.{wikitext_rev_id}")
        wiki_db_matches = F.col(f"rev.{wiki_db}") == F.col(f"wt.{wiki_db}")

        df = df.join(
            wt_df,
            on=((rev_id_matches) & (wiki_db_matches)),
            how="inner",
        )
        return df.select("rev.*", f"wt.{rev_text}")

    return _
