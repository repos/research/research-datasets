# Style Guide

This document contains guidelines that we follow within the `research-datasets` codebase to make sure our code stays consistent, readable and maintainable. That said, this document is still evolving and decisions here may change in the face of new language features, dependency upgrades and as new patterns emerge.

## Table of contents

- [Dataframe transformations](#dataframe-transformations)
  - [Naming transformations](#naming-transformations)
  - [Chaining transformations](#chaining-transformations)
  - [Defining transformations with additional arguments](#defining-transformations-with-additional-arguments)
  - [Making schema expectations implicit or explicit](#making-schema-expectations-implicit-or-explicit)
- [Pipelines](#pipelines)
  - [Recommendations for pipelines](#recommendations-for-pipelines)
    - [Be data lake centric](#be-data-lake-centric)
    - [Be pure](#be-pure)
    - [Be idempotent](#be-idempotent)

## Dataframe transformations

`research-datasets` mostly consists of data pipelines written using pyspark for various research projects and transformations are at the heart of all those pipelines. Specifically, a dataframe transformation is a function that takes a spark dataframe and returns a new spark dataframe i.e. `pyspark.sql.DataFrame -> pyspark.sql.DataFrame` (`research-datasets` has [defined](https://gitlab.wikimedia.org/repos/research/research-datasets/-/blob/main/src/research_transformations/transformation.py#L8) a [type alias](https://docs.python.org/3.10/library/typing.html#type-aliases) for such functions). For example, the following is a transformation:

```python
def with_foo(df: DataFrame) -> DataFrame:
    return df.withColumn("foo", foo_udf("bar"))
```

Organizing spark pipelines as a set of transformations allows us to:

- Test each transformation in isolation, making debugging easier when things go wrong.
- Reuse transformations across pipelines and in notebooks, decreasing the amount of code and tests needed.

### Naming transformations

The convention used for naming transformations is:

- `project_*` for transformations that select columns.
- `filter_*` for transformations that remove rows.
- `with_*` for transformations that add columns.
- `enrich_with_*` for transformations that add new columns while also removing one or more existing columns or rows.

The goal of this naming scheme is to convey _what_ a transformation does and _how_ it does it without necessarily having to look at the definition. Since transformations are _actions_ performed on a dataframe, transformation names usually shouldn't be nouns.

```python
# Good
def project_user_info(mediawiki_history_df: DataFrame) -> DataFrame:
    ...

def filter_disputed_reverts(mediawiki_history_df: DataFrame) -> DataFrame:
    ...

def with_wikitext_diff(wikitext_df: DataFrame) -> DataFrame:
    ...

def enrich_with_revert_risk(mediawiki_history_df: DataFrame) -> DataFrame:
    ...

# Bad: Doesn't convey how the transformation affects existing rows and columns
def predictions(df: DataFrame) -> DataFrame:
    ...
```

### Chaining transformations

To chain transformations and build up more complex behaviour, use the `pyspark.sql.DataFrame.transform` method:

```python
# Good
revert_risk_df = (
    mediawiki_history_df.transform(filter_disputed_reverts)
    .transform(with_revision_content)
    .transform(enrich_with_revert_risk)
)

# Bad: Intermediate variables that don't improve readability but add noise
filtered_mediawiki_history_df = filter_disputed_reverts(mediawiki_history_df)
revision_content_df = with_revision_content(filtered_mediawiki_history_df)
revert_risk_df = enrich_with_revert_risk(revision_content_df)
```

One caveat here is that very long chains of transformations can also sometimes be hard to read and reason about in which case it is okay to break those chains into logical groups and assign intermediate results to well named variables.

### Defining transformations with additional arguments

`transform` expects a function of the form `pyspark.sql.DataFrame -> pyspark.sql.DataFrame`. However, there are cases where you may need to pass in additional arguments alongside a dataframe -- for example another dataframe to join with or flags to configure the behavior of the transformation. The recommended way to do that is to make your transformation an inner function and return a closure over all arguments other than the dataframe:

```python
# Pass in an additional dataframe to join with.
def with_revision_content(wikitext_df: DataFrame) -> Transformation:
    # This inner function is named '_' to be easily recognizable
    # and consistent across transformations.
    def _(mediawiki_history_df: DataFrame) -> DataFrame:  # The actual transformation
        return mediawiki_history_df.join(wikitext_df, on=["wiki_db", "page_id"])

    return _

# Pass in an additional threshold value to the transformation.
def filter_pageviews_under_threshold(threshold: int) -> Transformation:
    def _(pageviews_df: DataFrame) -> DataFrame:  # The actual transformation
        return pageviews_df.filter(F.col("pageviews") > threshold)

    return _

mediawiki_history_df.transform(with_revision_content(wikitext_df=wikitext_df))
pageviews_df.transform(filter_pageviews_under_threshold(threshold=5))
```

### Making schema expectations implicit or explicit

Every transformation expects a certain schema and you can make this implicit or explicit:

```python
# Implicit: Expects a dataframe and assumes it has certain columns
def filter_disputed_reverts(reverting_rev_df: DataFrame) -> Transformation:
    def _(df: Dataframe) -> Dataframe:
        # The names of the columns to join are hard-coded, i.e.
        # the dataframes are implicitly expected to contain the required columns.
        return df.join(reverting_rev_df, on=["rev_id" == "reverting_rev_id"])

    return _

# Explicit: Expects a dataframe along with column names for all required columns
def filter_disputed_reverts(
    reverting_rev_df: DataFrame,
    rev_id: ColName = "rev_id",
    reverting_rev_id: ColName = "reverting_rev_id",
) -> Transformation:
    def _(df: Dataframe) -> Dataframe:
        # Column name arguments are used instead of
        # hard-coded names when referring to columns.
        return df.join(reverting_rev_df, on=[rev_id == reverting_rev_id])

    return _
```

Explicit is better because:

- It makes the transformation reusable across pipelines.
- It makes it easier to test the transformation as you know exactly what columns the test input should have by just looking at the signature.

However, for transformations that manipulate a large number of columns or that are not meant to be reusable, it is okay to implicitly assume a certain schema.

## Pipelines

Pipelines in `research-datasets` are transformations that are themselves composed of built-in and custom dataframe transformations. Pipelines are meant to be _applied_ to datasets in the [data lake](https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake) (or to dataframes that have equivalent schemas) to derive new datasets like the article topics dataset or revert risk predictions datasets. Apart from the benefits discussed above, writing pipelines as chained transformations gives a high level overview of its structure in one place which makes it easy to grok what a pipeline actually does. For example:

```python
# The top pages dataset pipeline defined as a transformation
def enrich_with_top_pages(
    hour_to_process: datetime,
    pageviews_threshold: int,
    fingerprint_threshold: int,
    top_n: int,
) -> Transformation:
    def _(pageview_actor_df: DataFrame) -> DataFrame:
        top_pages_df = (
            pageview_actor_df.transform(_filter_pageviews_outside_hour(hour_to_process))
            .transform(_filter_non_qualifying_pageviews)
            .transform(_aggregate_pageviews_by_country)
            .where(F.col("pageviews") > pageviews_threshold)
            .where(F.col("fingerprints") > fingerprint_threshold)
            .transform(_take_top_pageviews_by_country(top_n))
        )
        return top_pages_df

    return _

```

### Recommendations for pipelines

While there might be exceptional cases, most pipelines should:

#### Be data lake centric

The input to every pipeline should be one or more dataframes that represent datasets from the data lake. Any transformations like projection or filtering to get the dataset in the right shape should be part of the pipeline. Filtering for partitions is an exception to this rule, however, and should be done outside the transformation.

**Why**: This approach helps with clarity as it lets the reader know what schema a pipeline expects. More importantly, it makes the data dependencies of a pipeline explicit. It also helps with testing as you can define fixtures for datasets in the data lake once and use them across pipelines.

#### Be pure

Like all transformations, pipelines should (within reasonable bounds) be free from side effects. This means operations like reading data from a table in HDFS, loading a model from a file and writing results out to a parquet file should not be done within the pipeline transformation. Instead, expect the caller to perform these side effects and pass resources as inputs to the pipeline.

```python
# Good
def pipeline(df: DataFrame) -> DataFrame:
    ...

# Bad: Reads from the table within the transformation
def pipeline() -> DataFrame:
    df = spark.table(...)
```

**Why**: Pushing side effects outside of pipelines and to the boundaries of the program makes it trivial to test pipelines, start to finish.

```python
def test_enrich_with_top_pages() -> None:
    # Set up a dataframe with schema equivalent to `wmf.pageview_actor_df`
    pageview_actor_df = ...

    # Transform it to get the top pages dataset
    actual_df = pageview_actor_df.transform(enrich_with_top_pages)

    assert actual_df == expected_df
```

#### Be idempotent

When given the same inputs, produce the same output. An example of this is transformations that utilize pseudo random number generators (PRNG) to generate random numbers. To make sure that such transformations are idempotent, you'd usually set a seed (unless the randomness is desired):

```python
# Good
def pipeline(df: DataFrame) -> DataFrame:
    sample = df.sample(fraction=0.5, seed=42)

# Bad: returns a different output every time
def pipeline(df: DataFrame) -> DataFrame:
    sample = df.sample(fraction=0.5)
```

**Why**: To be able to fully reproduce a dataset as long as the input sources have not changed plus write assertions against the output when testing and have the tests pass reliably.
